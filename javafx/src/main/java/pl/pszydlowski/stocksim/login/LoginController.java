package pl.pszydlowski.stocksim.login;

import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import lombok.Setter;
import pl.pszydlowski.stocksim.shareholder.value.ShareholderId;

import java.util.function.Consumer;

public final class LoginController {

    public static final String INVALID_NAME_MESSAGE = "Only letters and digits are allowed as your name (5-20 characters)!";

    public TextField shareholderCreationTextField;
    public Label hintLabel;

    @Setter
    private Consumer<ShareholderId> loginConsumer;

    public void tryLogin(KeyEvent event) {
        if (isEnterKey(event)) {
            login();
        }
    }

    public void reloadView() {
        shareholderCreationTextField.clear();
        hintLabel.setText("");
    }

    public void login() {
        String shareholderName = shareholderCreationTextField.getText();
        if (isValidShareholderName(shareholderName)) {
            loginConsumer.accept(new ShareholderId(shareholderName));
        } else {
            hintLabel.setText(INVALID_NAME_MESSAGE);
        }
    }

    private boolean isEnterKey(KeyEvent event) {
        return event.getCode().equals(KeyCode.ENTER);
    }

    private boolean isValidShareholderName(String name) {
        return name != null && !name.isEmpty() && name.matches("([a-zA-Z0-9] ?)+") && name.length() >= 5 && name.length() <= 20;
    }
}
