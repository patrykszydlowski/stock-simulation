package pl.pszydlowski.stocksim.common.component;

import javafx.scene.control.Label;

import java.math.BigDecimal;

public class DoubleLabel extends Label {
    public BigDecimal getValue() {
        return new BigDecimal(getText());
    }

    public void setValue(double value) {
        setText(String.format("%.2f", value));
    }
}
