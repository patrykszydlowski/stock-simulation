package pl.pszydlowski.stocksim.common;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.ButtonBase;
import javafx.scene.control.ComboBoxBase;
import javafx.scene.control.TextInputControl;
import javafx.scene.layout.Pane;


public final class CssUtils {
    public static <T extends Node> T attachCssClass(T node, String... cssClass) {
        node.getStyleClass().addAll(cssClass);
        return node;
    }

    public static <T extends Pane> Parent<T> parent(T pane) {
        return new Parent<>(pane);
    }

    public static <T extends ComboBoxBase> T attachPrompt(T node, String promptText) {
        node.setPromptText(promptText);
        return node;
    }

    public static <T extends TextInputControl> T attachPrompt(T node, String promptText) {
        node.setPromptText(promptText);
        return node;
    }

    public static <T extends ButtonBase> T attachAction(T button, EventHandler<ActionEvent> handler) {
        button.setOnAction(handler);
        return button;
    }

    public static final class Parent<T extends Pane> {

        private final T instance;

        Parent(T instance) {
            this.instance = instance;
        }

        public T children(Node... children) {
            instance.getChildren().addAll(children);
            return instance;
        }
    }
}
