package pl.pszydlowski.stocksim.common.component;

import javafx.scene.control.Label;

import java.math.BigInteger;

public class IntegerLabel extends Label {
    public BigInteger getValue() {
        return new BigInteger(getText());
    }

    public void setValue(int value) {
        setText(String.valueOf(value));
    }
}
