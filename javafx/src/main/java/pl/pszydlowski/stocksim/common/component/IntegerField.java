package pl.pszydlowski.stocksim.common.component;

import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.util.converter.IntegerStringConverter;

import java.math.BigInteger;

public final class IntegerField extends TextField {

    public IntegerField() {
        setTextFormatter(new TextFormatter<>(new IntegerStringConverter()));
    }

    public BigInteger getValue() {
        try {
            return new BigInteger(getText());
        } catch (Exception ignored) {
            return null;
        }
    }

    public void setValue(int value) {
        setText(String.valueOf(value));
    }
}
