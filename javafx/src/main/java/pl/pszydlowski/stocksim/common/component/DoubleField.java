package pl.pszydlowski.stocksim.common.component;

import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.util.converter.DoubleStringConverter;

import java.math.BigDecimal;

public final class DoubleField extends TextField {

    public DoubleField() {
        setTextFormatter(new TextFormatter<>(new DoubleStringConverter()));
    }

    public BigDecimal getValue() {
        try {
            return new BigDecimal(getText());
        } catch (Exception ignored) {
            return null;
        }
    }

    public void setValue(double value) {
        setText(String.valueOf(value));
    }
}
