package pl.pszydlowski.stocksim.bootstrap;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import pl.pszydlowski.stocksim.admin.AdminController;
import pl.pszydlowski.stocksim.dashboard.DashboardController;
import pl.pszydlowski.stocksim.environment.EnvironmentLogger;
import pl.pszydlowski.stocksim.environment.InteractiveEnvironment;
import pl.pszydlowski.stocksim.environment.InteractiveEnvironmentBuilder;
import pl.pszydlowski.stocksim.login.LoginController;
import pl.pszydlowski.stocksim.share.repository.json.JsonShareRepository;
import pl.pszydlowski.stocksim.share.value.ShareId;
import pl.pszydlowski.stocksim.share.value.SharePrice;
import pl.pszydlowski.stocksim.share.value.ShareVolume;
import pl.pszydlowski.stocksim.shareholder.StockLogger;
import pl.pszydlowski.stocksim.shareholder.StrategyBasedShareholder;
import pl.pszydlowski.stocksim.shareholder.ThreadedStrategyBasedShareholder;
import pl.pszydlowski.stocksim.shareholder.repository.json.JsonShareholderDataRepository;
import pl.pszydlowski.stocksim.shareholder.strategy.BuyAllAvailableVolumeStrategy;
import pl.pszydlowski.stocksim.shareholder.strategy.FlipAllAvailableVolumeStrategy;
import pl.pszydlowski.stocksim.shareholder.value.ShareholderId;
import pl.pszydlowski.stocksim.stock.StockObserver;

import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

import static java.util.Arrays.asList;
import static pl.pszydlowski.stocksim.bootstrap.ApplicationProperties.ADMIN_PANEL_TITLE;
import static pl.pszydlowski.stocksim.bootstrap.ApplicationProperties.AVAILABLE_BOT_STRATEGIES;
import static pl.pszydlowski.stocksim.bootstrap.ApplicationProperties.AVAILABLE_STOCK_UPDATE_STRATEGIES;
import static pl.pszydlowski.stocksim.bootstrap.ApplicationProperties.DASHBOARD_PANEL_TITLE;
import static pl.pszydlowski.stocksim.bootstrap.ApplicationProperties.DATA_REPO_SIGNATURE;
import static pl.pszydlowski.stocksim.bootstrap.ApplicationProperties.SHAREHOLDER_UPDATE_INTERVAL;
import static pl.pszydlowski.stocksim.bootstrap.ApplicationProperties.STOCK_UPDATE_INTERVAL;
import static pl.pszydlowski.stocksim.bootstrap.ApplicationProperties.LOGIN_PANEL_TITLE;
import static pl.pszydlowski.stocksim.bootstrap.ApplicationProperties.SHARE_REPO_SIGNATURE;
import static pl.pszydlowski.stocksim.bootstrap.ApplicationProperties.STARTING_WALLET;

@SuppressWarnings("Duplicates")
public final class StockApplication extends Application {

    private final ScheduledExecutorService shareholderExecutorService = Executors.newScheduledThreadPool(4);

    private final URL commonCss = getClass().getResource("/styles/common.css");
    private final URL loginCss = getClass().getResource("/styles/login.css");
    private final URL dashboardCss = getClass().getResource("/styles/dashboard.css");
    private final URL adminCss = getClass().getResource("/styles/admin.css");

    private final URL loginView = getClass().getResource("/views/loginView.fxml");
    private final URL dashboardView = getClass().getResource("/views/dashboardView.fxml");
    private final URL adminView = getClass().getResource("/views/adminView.fxml");

    private InteractiveEnvironment environment;

    private Stage window;

    private Scene loginScene;
    private Scene dashboardScene;
    private Scene adminScene;

    private LoginController loginController;
    private DashboardController dashboardController;
    private AdminController adminController;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void init() {
        initEnvironment();
    }

    @Override
    public void start(Stage window) throws Exception {
        this.window = window;
        initComponents();
        wireControllers();
        initStartingView();
    }

    @Override
    public void stop() {
        stopEnvironment();
        shareholderExecutorService.shutdown();
    }

    private void stopEnvironment() {
        environment.stop();
    }

    private void startEnvironment() {
        environment.start();
    }

    private void initStartingView() {
        window.setTitle(LOGIN_PANEL_TITLE);
        redirect(loginScene);
        window.show();
    }

    private void wireControllers() {
        loginController.setLoginConsumer(id -> {
            createInteractiveShareholderForCurrentSession(id);
            redirectToDashboardView();
            createInitialEnvironment();
        });

        dashboardController.setRedirectToLoginAction(this::redirectToLoginView);
        dashboardController.setRedirectToAdminAction(this::redirectToAdminView);

        adminController.setRedirectToLoginAction(this::redirectToLoginView);
        adminController.setShareRemoveConsumer(dashboardController::removeShare);
    }

    private void createInitialEnvironment() {
        createInitialShares();
        createSessionBots();
    }

    private void createInitialShares() {
        environment.addShare(new ShareId("bit-coin"), SharePrice.of(10.0), ShareVolume.of(10000));
        environment.addShare(new ShareId("doge-coin"), SharePrice.of(50.0), ShareVolume.of(2500));
        environment.addShare(new ShareId("luxury-coin"), SharePrice.of(1000.0), ShareVolume.of(100));
    }

    private void createSessionBots() {
        final ShareholderId lazyGeorge = new ShareholderId("Lazy George");
        final ShareholderId greedyMike = new ShareholderId("Greedy Mike");
        final ShareholderId threadedJohn = new ShareholderId("Threaded John");
        final ShareholderId flipperJack = new ShareholderId("Flipper Jack");

        environment.addShareholder(lazyGeorge, new StrategyBasedShareholder(lazyGeorge));
        environment.addShareholder(
                greedyMike,
                new StrategyBasedShareholder(greedyMike, BuyAllAvailableVolumeStrategy.getInstance())
        );
        environment.addShareholder(
                threadedJohn,
                new ThreadedStrategyBasedShareholder(
                        threadedJohn,
                        FlipAllAvailableVolumeStrategy.getInstance(),
                        shareholderExecutorService,
                        SHAREHOLDER_UPDATE_INTERVAL
                )
        );
        environment.addShareholder(
                flipperJack,
                new StrategyBasedShareholder(flipperJack, FlipAllAvailableVolumeStrategy.getInstance())
        );

        adminController.setSessionBots(asList(lazyGeorge, greedyMike, flipperJack));
    }

    private void createInteractiveShareholderForCurrentSession(ShareholderId id) {
        final StockObserver sessionShareholder = dashboardController.createShareholder(id);
        environment.addShareholder(id, sessionShareholder);
    }

    private void redirectToDashboardView() {
        window.setTitle(DASHBOARD_PANEL_TITLE);
        startEnvironment();
        redirect(dashboardScene);
    }

    private void redirectToLoginView() {
        window.setTitle(LOGIN_PANEL_TITLE);
        stopEnvironment();
        loginController.reloadView();
        redirect(loginScene);
    }

    private void redirectToAdminView() {
        window.setTitle(ADMIN_PANEL_TITLE);
        stopEnvironment();
        adminController.reloadView();
        redirect(adminScene);
    }

    private void redirect(Scene scene) {
        window.setScene(scene);
    }

    private void initComponents() throws IOException {
        initLoginComponents();
        initDashboardComponents();
        initAdminComponents();
    }

    private void initAdminComponents() throws IOException {
        final FXMLLoader adminViewLoader = new FXMLLoader(adminView);
        final Parent adminView = adminViewLoader.load();

        adminScene = new Scene(adminView);

        loadCss(adminScene, commonCss, adminCss);

        adminController = adminViewLoader.getController();

        adminController.setEnvironment(environment);
        adminController.setStockUpdateStrategies(AVAILABLE_STOCK_UPDATE_STRATEGIES);
        adminController.setShareholderActionStrategies(AVAILABLE_BOT_STRATEGIES);
    }

    private void initDashboardComponents() throws IOException {
        final FXMLLoader dashboardViewLoader = new FXMLLoader(dashboardView);
        final Parent dashboardView = dashboardViewLoader.load();

        dashboardScene = new Scene(dashboardView);

        loadCss(dashboardScene, commonCss, dashboardCss);

        dashboardController = dashboardViewLoader.getController();
    }

    private void initLoginComponents() throws IOException {
        final FXMLLoader loginViewLoader = new FXMLLoader(loginView);
        final Parent loginView = loginViewLoader.load();

        loginScene = new Scene(loginView);

        loadCss(loginScene, commonCss, loginCss);

        loginController = loginViewLoader.getController();
    }

    private void initEnvironment() {
        environment = new InteractiveEnvironmentBuilder()
                .withShareRepository(new JsonShareRepository(SHARE_REPO_SIGNATURE))
                .withDataRepository(new JsonShareholderDataRepository(DATA_REPO_SIGNATURE))
                .withServiceSupplier(Executors::newSingleThreadScheduledExecutor)
                .withUpdateInterval(STOCK_UPDATE_INTERVAL)
                .withDefaultWallet(STARTING_WALLET)
                .withAdditionalStockObserver(new StockLogger())
                .build(new EnvironmentLogger());
    }

    private void loadCss(Scene scene, URL... css) {
        scene.getStylesheets().addAll(Arrays.stream(css).map(URL::toExternalForm).toArray(String[]::new));
    }
}
