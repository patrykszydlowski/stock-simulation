package pl.pszydlowski.stocksim.bootstrap;

import javafx.geometry.Orientation;
import javafx.scene.control.Separator;

import static pl.pszydlowski.stocksim.common.CssUtils.attachCssClass;

public final class CssProperties {

    // common css classes
    public static final String SCROLLABLE_CONTAINER = "scrollable-container";
    public static final String SCROLLBAR = "scrollbar";
    public static final String SMALL_MARGIN = "small-margin";
    public static final String BOX = "box";
    public static final String BOX_HEADER = "box-header";
    public static final String NAME_LABEL = "name-label";

    // dashboard panel specific classes
    public static final String WALLET_BOX = "wallet-box";
    public static final String RIGHT_BOX = "right-box";
    public static final String ACTIVITY_BUTTON = "activity-button";

    // admin panel specific classes
    public static final String CONTAINER_HEADER = "container-header";
    public static final String BOTTOM_BOX = "bottom-box";
    public static final String HINT = "hint";
    public static final String STRATEGY_BOX = "strategy-box";
    public static final String NAME_PROMPT = "name-prompt";
    public static final String DATA_PROMPT = "data-prompt";
    public static final String DATA_BUTTON = "data-button";

    public static Separator horizontalSeparator() {
        return attachCssClass(new Separator(), SMALL_MARGIN);
    }

    public static Separator verticalSeparator() {
        final Separator separator = horizontalSeparator();
        separator.setOrientation(Orientation.VERTICAL);
        return separator;
    }
}
