package pl.pszydlowski.stocksim.bootstrap;


import pl.pszydlowski.stocksim.common.money.Wallet;
import pl.pszydlowski.stocksim.shareholder.strategy.BuyAllAvailableVolumeStrategy;
import pl.pszydlowski.stocksim.shareholder.strategy.FlipAllAvailableVolumeStrategy;
import pl.pszydlowski.stocksim.shareholder.strategy.NullShareholderActionStrategy;
import pl.pszydlowski.stocksim.shareholder.strategy.ShareholderActionStrategy;
import pl.pszydlowski.stocksim.stock.strategy.ConstantValueChangeStrategy;
import pl.pszydlowski.stocksim.stock.strategy.NullStockUpdateStrategy;
import pl.pszydlowski.stocksim.stock.strategy.RandomizingStrategy;
import pl.pszydlowski.stocksim.stock.strategy.StockUpdateStrategy;
import pl.pszydlowski.stocksim.stock.value.UpdateInterval;

import java.util.Map;
import java.util.function.Supplier;

import static java.util.concurrent.TimeUnit.SECONDS;
import static pl.pszydlowski.stocksim.common.data.Maps.entry;
import static pl.pszydlowski.stocksim.common.data.Maps.map;

public final class ApplicationProperties {

    static final String LOGIN_PANEL_TITLE = "Stock Simulator - Login Panel";
    static final String ADMIN_PANEL_TITLE = "Stock Simulator - Admin Panel";
    static final String DASHBOARD_PANEL_TITLE = "Stock Simulator - Dashboard";

    static final String SHARE_REPO_SIGNATURE = "shares";
    static final String DATA_REPO_SIGNATURE = "shareholders";

    static final UpdateInterval STOCK_UPDATE_INTERVAL = new UpdateInterval(1, SECONDS);
    static final UpdateInterval SHAREHOLDER_UPDATE_INTERVAL = new UpdateInterval(10, SECONDS);
    public static final Wallet STARTING_WALLET = Wallet.of(1000);

    static Map<String, StockUpdateStrategy> AVAILABLE_STOCK_UPDATE_STRATEGIES = map(
            entry("don't change price of shares", NullStockUpdateStrategy.getInstance()),
            entry("make shares constantly more expensive", new ConstantValueChangeStrategy(1.1)),
            entry("slightly randomize prices after transactions", new RandomizingStrategy(0.5))
    );

    static Map<String, Supplier<ShareholderActionStrategy>> AVAILABLE_BOT_STRATEGIES = map(
            entry("don't buy or sell any share", NullShareholderActionStrategy::getInstance),
            entry("buy all possible shares", BuyAllAvailableVolumeStrategy::getInstance),
            entry("flip all possible shares", FlipAllAvailableVolumeStrategy::getInstance)
    );

}
