package pl.pszydlowski.stocksim.admin;

import javafx.scene.control.ComboBox;
import lombok.Setter;
import pl.pszydlowski.stocksim.common.data.Tuple;
import pl.pszydlowski.stocksim.common.money.Wallet;
import pl.pszydlowski.stocksim.common.repository.Repository;
import pl.pszydlowski.stocksim.admin.component.BotBoxContainer;
import pl.pszydlowski.stocksim.admin.component.ShareBoxContainer;
import pl.pszydlowski.stocksim.admin.component.ShareholderBoxContainer;
import pl.pszydlowski.stocksim.admin.component.WalletBoxContainer;
import pl.pszydlowski.stocksim.environment.InteractiveEnvironment;
import pl.pszydlowski.stocksim.share.Share;
import pl.pszydlowski.stocksim.share.value.ShareChange;
import pl.pszydlowski.stocksim.share.value.ShareData;
import pl.pszydlowski.stocksim.share.value.ShareId;
import pl.pszydlowski.stocksim.share.value.SharePrice;
import pl.pszydlowski.stocksim.share.value.ShareVolume;
import pl.pszydlowski.stocksim.shareholder.StrategyBasedShareholder;
import pl.pszydlowski.stocksim.shareholder.strategy.ShareholderActionStrategy;
import pl.pszydlowski.stocksim.shareholder.value.ShareholderData;
import pl.pszydlowski.stocksim.shareholder.value.ShareholderId;
import pl.pszydlowski.stocksim.stock.strategy.StockUpdateStrategy;

import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import static java.util.Optional.ofNullable;
import static pl.pszydlowski.stocksim.bootstrap.ApplicationProperties.STARTING_WALLET;

public final class AdminController {

    public ComboBox updateStrategyBox;
    public ShareBoxContainer shareContainer;
    public WalletBoxContainer walletContainer;
    public ShareholderBoxContainer shareholderContainer;
    public BotBoxContainer botContainer;

    private Map<String, StockUpdateStrategy> stockStrategiesByDescription;
    private InteractiveEnvironment environment;

    @Setter
    private Runnable redirectToLoginAction;

    @Setter
    private Consumer<ShareId> shareRemoveConsumer;

    public void setEnvironment(InteractiveEnvironment environment) {
        this.environment = environment;

        botContainer.setBotAddConsumer((id, strategy) -> {
            shareholderContainer.addShareholderBox(id);
            walletContainer.addWallet(id, STARTING_WALLET);
            environment.addShareholder(id, new StrategyBasedShareholder(id, strategy));
        });

        botContainer.setBotRemoveConsumer(environment::removeShareholder);
        shareContainer.setShareRemoveConsumer(this::removeShare);
        shareContainer.setShareChangeConsumer(this::postShareChangeToEnvironment);
        shareContainer.setShareAddedConsumer(id -> environment.addShare(id, SharePrice.ZERO, ShareVolume.ZERO));

        walletContainer.setWalletChangeConsumer(this::postWalletChangeToEnvironment);

        shareholderContainer.setShareholderRemoveConsumer(id -> {
            botContainer.removeBot(id);
            walletContainer.removeWallet(id);
            environment.removeShareholder(id);
        });
    }

    public void setSessionBots(List<ShareholderId> sessionBots) {
        botContainer.setBots(sessionBots);
    }

    public void redirectToLoginPanel() {
        redirectToLoginAction.run();
    }

    public void reloadView() {
        final Repository<Share, ShareId> shareRepository = environment.getShareRepository();
        final Repository<ShareholderData, ShareholderId> dataRepository = environment.getDataRepository();

        final List<Tuple<ShareId, ShareData>> shareData = getShares(shareRepository);
        final List<ShareholderData> shareholderData = getShareholdersData(dataRepository);
        final List<ShareholderId> shareholders = getShareholders(dataRepository);

        shareContainer.setShares(shareData);
        shareholderContainer.setShareholders(shareholders);
        walletContainer.setWallets(shareholderData);
    }

    public void setStockUpdateStrategies(Map<String, StockUpdateStrategy> strategies) {
        stockStrategiesByDescription = strategies;
        updateStrategyBox.getItems().setAll(strategies.keySet());
    }

    public void changeStockUpdateStrategy() {
        ofNullable(stockStrategiesByDescription.get(updateStrategyBox.getValue())).ifPresent(environment::changeStockUpdateStrategy);
    }

    public void setShareholderActionStrategies(Map<String, Supplier<ShareholderActionStrategy>> strategies) {
        botContainer.setAvailableStrategies(strategies);
    }

    private void postShareChangeToEnvironment(ShareChange change) {
        if (change.priceChanged()) {
            environment.changeSharePrice(change.getId(), change.getCurrentData().getPrice());
        }
        if (change.volumeChanged()) {
            environment.changeShareVolume(change.getId(), change.getCurrentData().getVolume());
        }
    }

    private void postWalletChangeToEnvironment(ShareholderId shareholderId, Wallet wallet) {
        environment.changeShareholderWallet(shareholderId, wallet);
    }

    private void removeShare(ShareId shareId) {
        environment.removeShare(shareId);
        shareRemoveConsumer.accept(shareId);
    }

    private List<Tuple<ShareId, ShareData>> getShares(Repository<Share, ShareId> shareRepository) {
        return shareRepository
                .findAll()
                .stream()
                .map(share -> new Tuple<>(share.getId(), share.getData()))
                .collect(Collectors.toList());
    }

    private List<ShareholderData> getShareholdersData(Repository<ShareholderData, ShareholderId> dataRepository) {
        return dataRepository.findAll();
    }

    private List<ShareholderId> getShareholders(Repository<ShareholderData, ShareholderId> dataRepository) {
        return dataRepository
                .findAll()
                .stream()
                .map(ShareholderData::getId)
                .collect(Collectors.toList());
    }
}
