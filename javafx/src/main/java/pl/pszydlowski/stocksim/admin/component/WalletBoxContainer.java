package pl.pszydlowski.stocksim.admin.component;

import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.VBox;
import lombok.Setter;
import pl.pszydlowski.stocksim.common.money.Wallet;
import pl.pszydlowski.stocksim.shareholder.value.ShareholderData;
import pl.pszydlowski.stocksim.shareholder.value.ShareholderId;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;

import static javafx.application.Platform.runLater;
import static pl.pszydlowski.stocksim.bootstrap.CssProperties.CONTAINER_HEADER;
import static pl.pszydlowski.stocksim.bootstrap.CssProperties.SCROLLABLE_CONTAINER;
import static pl.pszydlowski.stocksim.bootstrap.CssProperties.horizontalSeparator;
import static pl.pszydlowski.stocksim.common.CssUtils.attachCssClass;
import static pl.pszydlowski.stocksim.common.CssUtils.parent;

public final class WalletBoxContainer extends VBox {

    private final Map<ShareholderId, WalletBox> boxesById;

    private final VBox list;
    private final ScrollPane listContainer;

    @Setter
    private BiConsumer<ShareholderId, Wallet> walletChangeConsumer;

    public WalletBoxContainer() {
        boxesById = new HashMap<>();
        final Label header = attachCssClass(new Label("Shareholder Wallets"), CONTAINER_HEADER);

        listContainer = attachCssClass(new ScrollPane(), SCROLLABLE_CONTAINER);
        list = new VBox();
        listContainer.setContent(list);

        parent(this).children(header, horizontalSeparator(), listContainer);
    }

    public void setWallets(List<ShareholderData> shareholderData) {
        boxesById.clear();
        list.getChildren().clear();
        shareholderData.forEach(data -> addWallet(data.getId(), data.getWallet()));
    }

    public void removeWallet(ShareholderId shareholderId) {
        WalletBox box = boxesById.remove(shareholderId);
        list.getChildren().remove(box);
    }

    public void addWallet(ShareholderId shareholderId, Wallet wallet) {
        if (!boxesById.containsKey(shareholderId)) {
            WalletBox box = new WalletBox();
            box.setShareholderName(shareholderId);
            box.setWallet(wallet);
            box.setWalletChangeAction(updatedWallet -> walletChangeConsumer.accept(shareholderId, updatedWallet));
            list.getChildren().add(box);
            boxesById.put(shareholderId, box);
            runLater(() -> listContainer.setVvalue(1));
        }
    }
}

