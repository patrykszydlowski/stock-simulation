package pl.pszydlowski.stocksim.admin.component;

import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.VBox;
import lombok.Setter;
import pl.pszydlowski.stocksim.shareholder.value.ShareholderId;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

import static javafx.application.Platform.runLater;
import static pl.pszydlowski.stocksim.bootstrap.CssProperties.CONTAINER_HEADER;
import static pl.pszydlowski.stocksim.bootstrap.CssProperties.SCROLLABLE_CONTAINER;
import static pl.pszydlowski.stocksim.bootstrap.CssProperties.horizontalSeparator;
import static pl.pszydlowski.stocksim.common.CssUtils.attachCssClass;
import static pl.pszydlowski.stocksim.common.CssUtils.parent;

public final class ShareholderBoxContainer extends VBox {

    private final Map<ShareholderId, ShareholderBox> boxesById;

    private final VBox list;
    private final ScrollPane listContainer;

    @Setter
    private Consumer<ShareholderId> shareholderRemoveConsumer;

    public ShareholderBoxContainer() {
        boxesById = new HashMap<>();
        final Label header = attachCssClass(new Label("Shareholders"), CONTAINER_HEADER);

        listContainer = attachCssClass(new ScrollPane(), SCROLLABLE_CONTAINER);
        list = new VBox();
        listContainer.setContent(list);

        parent(this).children(header, horizontalSeparator(), listContainer);
    }

    public void setShareholders(List<ShareholderId> shareholders) {
        shareholders.forEach(this::addShareholderBox);
    }

    public void addShareholderBox(ShareholderId shareholderId) {
        if (!boxesById.containsKey(shareholderId)) {
            ShareholderBox box = new ShareholderBox();
            box.setShareholderName(shareholderId);
            box.setRemoveAction(() -> removeShareholder(shareholderId));
            boxesById.put(shareholderId, box);
            list.getChildren().add(box);
            runLater(() -> listContainer.setVvalue(1));
        }
    }

    private void removeShareholder(ShareholderId shareholderId) {
        ShareholderBox box = boxesById.remove(shareholderId);
        list.getChildren().remove(box);
        shareholderRemoveConsumer.accept(shareholderId);
    }
}
