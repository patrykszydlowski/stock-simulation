package pl.pszydlowski.stocksim.admin.component;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import pl.pszydlowski.stocksim.shareholder.value.ShareholderId;

import static pl.pszydlowski.stocksim.common.CssUtils.attachCssClass;
import static pl.pszydlowski.stocksim.common.CssUtils.parent;
import static pl.pszydlowski.stocksim.bootstrap.CssProperties.BOX;
import static pl.pszydlowski.stocksim.bootstrap.CssProperties.BOX_HEADER;

final class ShareholderBox extends HBox {

    private final Label name;

    private final Button removeButton;

    ShareholderBox() {
        attachCssClass(this, BOX);

        name = attachCssClass(new Label(), BOX_HEADER);
        removeButton = new Button("remove");

        parent(this).children(name, removeButton);
    }

    void setShareholderName(ShareholderId shareholderId) {
        name.setText(shareholderId.getValue());
    }

    void setRemoveAction(Runnable removeAction) {
        removeButton.setOnAction(e -> removeAction.run());
    }
}
