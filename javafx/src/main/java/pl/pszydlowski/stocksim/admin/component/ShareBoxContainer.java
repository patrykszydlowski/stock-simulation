package pl.pszydlowski.stocksim.admin.component;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import lombok.Setter;
import pl.pszydlowski.stocksim.common.data.Tuple;
import pl.pszydlowski.stocksim.share.value.ShareChange;
import pl.pszydlowski.stocksim.share.value.ShareData;
import pl.pszydlowski.stocksim.share.value.ShareId;
import pl.pszydlowski.stocksim.share.value.SharePrice;
import pl.pszydlowski.stocksim.share.value.ShareVolume;

import java.time.Instant;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

import static javafx.application.Platform.runLater;
import static pl.pszydlowski.stocksim.bootstrap.CssProperties.BOTTOM_BOX;
import static pl.pszydlowski.stocksim.bootstrap.CssProperties.CONTAINER_HEADER;
import static pl.pszydlowski.stocksim.bootstrap.CssProperties.HINT;
import static pl.pszydlowski.stocksim.bootstrap.CssProperties.NAME_PROMPT;
import static pl.pszydlowski.stocksim.bootstrap.CssProperties.SCROLLABLE_CONTAINER;
import static pl.pszydlowski.stocksim.bootstrap.CssProperties.horizontalSeparator;
import static pl.pszydlowski.stocksim.common.CssUtils.attachAction;
import static pl.pszydlowski.stocksim.common.CssUtils.attachCssClass;
import static pl.pszydlowski.stocksim.common.CssUtils.attachPrompt;
import static pl.pszydlowski.stocksim.common.CssUtils.parent;

public final class ShareBoxContainer extends VBox {

    private static final String INVALID_NAME_MESSAGE = "Only letters, digits and dashes are allowed as share name (3-20 characters)!";
    private static final String NAME_ALREADY_EXISTS = "This share name already exists!";

    private final Map<ShareId, ShareBox> boxesById;

    private final VBox list;
    private final TextField addShareNameField;
    private final Label hintLabel;
    private final ScrollPane listContainer;

    @Setter
    private Consumer<ShareChange> shareChangeConsumer;

    @Setter
    private Consumer<ShareId> shareRemoveConsumer;

    @Setter
    private Consumer<ShareId> shareAddedConsumer;

    public ShareBoxContainer() {
        boxesById = new HashMap<>();
        final Label header = attachCssClass(new Label("Stock Shares"), CONTAINER_HEADER);

        listContainer = attachCssClass(new ScrollPane(), SCROLLABLE_CONTAINER);
        list = new VBox();
        listContainer.setContent(list);

        final Button addShareButton = attachAction(new Button("add"), e -> tryAddShareBox());
        addShareNameField = attachPrompt(attachCssClass(new TextField(), NAME_PROMPT), "Enter share name...");
        final HBox addShareBox = parent(new HBox()).children(addShareNameField, addShareButton);

        hintLabel = attachCssClass(new Label(), HINT);
        final VBox bottomBox = parent(attachCssClass(new VBox(), BOTTOM_BOX)).children(addShareBox, hintLabel);

        parent(this).children(header, horizontalSeparator(), listContainer, horizontalSeparator(), bottomBox);
    }

    public void setShares(List<Tuple<ShareId, ShareData>> shares) {
        boxesById.clear();
        list.getChildren().clear();
        shares.forEach(share -> addShareBox(share.getLeft(), share.getRight().getPrice(), share.getRight().getVolume()));
    }

    private void tryAddShareBox() {
        String shareName = addShareNameField.getText();
        if (!isValidShareName(shareName)) {
            hintLabel.setText(INVALID_NAME_MESSAGE);
        } else if (!isAvailableShareName(shareName)) {
            hintLabel.setText(NAME_ALREADY_EXISTS);
        } else {
            addShareBox(shareName);
        }
    }

    private void addShareBox(String shareName) {
        ShareId shareId = new ShareId(shareName);
        addShareBox(shareId, SharePrice.ZERO, ShareVolume.ZERO);
        shareAddedConsumer.accept(shareId);
    }

    private void addShareBox(ShareId shareId, SharePrice price, ShareVolume volume) {
        ShareBox box = new ShareBox();
        box.setShareName(shareId);
        box.setPrice(price);
        box.setVolume(volume);
        box.setRemoveAction(() -> removeShare(shareId));
        box.setShareDataChangeAction((previousData, updatedData) -> shareChangeConsumer.accept(new ShareChange(shareId, previousData, updatedData, Instant.now())));
        boxesById.put(shareId, box);
        list.getChildren().add(box);
        runLater(() -> listContainer.setVvalue(1));
    }

    private void removeShare(ShareId shareId) {
        ShareBox box = boxesById.remove(shareId);
        list.getChildren().remove(box);
        shareRemoveConsumer.accept(shareId);
    }

    private boolean isValidShareName(String shareName) {
        return shareName != null && !shareName.isEmpty() && shareName.matches("[a-zA-Z0-9\\-]{3,20}");
    }

    private boolean isAvailableShareName(String shareName) {
        return boxesById.keySet().stream().noneMatch(id -> id.getValue().equals(shareName));
    }
}
