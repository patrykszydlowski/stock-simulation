package pl.pszydlowski.stocksim.admin.component;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import pl.pszydlowski.stocksim.shareholder.value.ShareholderId;

import static pl.pszydlowski.stocksim.common.CssUtils.attachCssClass;
import static pl.pszydlowski.stocksim.common.CssUtils.parent;
import static pl.pszydlowski.stocksim.bootstrap.CssProperties.BOX;
import static pl.pszydlowski.stocksim.bootstrap.CssProperties.BOX_HEADER;

final class BotBox extends HBox {

    private final Label botName;

    private final Button removeButton;

    BotBox() {
        attachCssClass(this, BOX);

        botName = attachCssClass(new Label(), BOX_HEADER);
        removeButton = new Button("remove");

        parent(this).children(botName, removeButton);
    }

    void setShareholderName(ShareholderId shareholderId) {
        botName.setText(shareholderId.getValue());
    }

    void setRemoveAction(Runnable removeAction) {
        removeButton.setOnAction(e -> removeAction.run());
    }
}
