package pl.pszydlowski.stocksim.admin.component;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import pl.pszydlowski.stocksim.common.money.Wallet;
import pl.pszydlowski.stocksim.common.component.DoubleField;
import pl.pszydlowski.stocksim.common.component.DoubleLabel;
import pl.pszydlowski.stocksim.shareholder.value.ShareholderId;

import java.util.function.Consumer;

import static pl.pszydlowski.stocksim.common.CssUtils.attachAction;
import static pl.pszydlowski.stocksim.common.CssUtils.attachCssClass;
import static pl.pszydlowski.stocksim.common.CssUtils.parent;
import static pl.pszydlowski.stocksim.bootstrap.CssProperties.BOX;
import static pl.pszydlowski.stocksim.bootstrap.CssProperties.BOX_HEADER;
import static pl.pszydlowski.stocksim.bootstrap.CssProperties.DATA_BUTTON;
import static pl.pszydlowski.stocksim.bootstrap.CssProperties.DATA_PROMPT;
import static pl.pszydlowski.stocksim.bootstrap.CssProperties.NAME_LABEL;

final class WalletBox extends VBox {

    private final Label shareholderName;
    private final DoubleLabel value;

    private final DoubleField valueEditField;

    private final Button editButton;
    private final Button confirmButton;
    private final Button cancelButton;

    private final VBox buttonBox;
    private final Pane valuePane;


    WalletBox() {
        attachCssClass(this, BOX);

        value = new DoubleLabel();
        valuePane = attachCssClass(new Pane(), DATA_PROMPT);
        valueEditField = attachCssClass(new DoubleField(), DATA_PROMPT);
        buttonBox = new VBox();

        editButton = attachAction(attachCssClass(new Button("edit"), DATA_BUTTON), e -> enterEditMode());
        cancelButton = attachAction(attachCssClass(new Button("cancel"), DATA_BUTTON), e -> enterVisualMode());
        confirmButton = attachCssClass(new Button("confirm"), DATA_BUTTON);

        final Label valueLabel = attachCssClass(new Label("money: "), NAME_LABEL);
        final HBox dataBox = parent(new HBox()).children(valueLabel, valuePane);
        final HBox mainBox = parent(new HBox()).children(dataBox, buttonBox);

        shareholderName = attachCssClass(new Label(), BOX_HEADER);
        parent(this).children(shareholderName, mainBox);
        enterVisualMode();
    }

    void setShareholderName(ShareholderId shareholderId) {
        shareholderName.setText(shareholderId.getValue());
    }

    void setWallet(Wallet wallet) {
        value.setValue(wallet.getValue().doubleValue());
    }

    void setWalletChangeAction(Consumer<Wallet> walletChangeAction) {
        confirmButton.setOnAction(e -> {
            enterVisualMode();
            Wallet wallet = new Wallet(valueEditField.getValue());
            walletChangeAction.accept(wallet);
            setWallet(wallet);
        });
    }

    private void enterEditMode() {
        cleanContainers();
        valueEditField.setValue(value.getValue().doubleValue());
        buttonBox.getChildren().addAll(confirmButton, cancelButton);
        valuePane.getChildren().add(valueEditField);
    }

    private void enterVisualMode() {
        cleanContainers();
        buttonBox.getChildren().add(editButton);
        valuePane.getChildren().add(value);
    }

    private void cleanContainers() {
        buttonBox.getChildren().clear();
        valuePane.getChildren().clear();
    }
}
