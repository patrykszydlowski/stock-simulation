package pl.pszydlowski.stocksim.admin.component;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import pl.pszydlowski.stocksim.common.component.DoubleField;
import pl.pszydlowski.stocksim.common.component.DoubleLabel;
import pl.pszydlowski.stocksim.common.component.IntegerField;
import pl.pszydlowski.stocksim.common.component.IntegerLabel;
import pl.pszydlowski.stocksim.share.value.ShareData;
import pl.pszydlowski.stocksim.share.value.ShareId;
import pl.pszydlowski.stocksim.share.value.SharePrice;
import pl.pszydlowski.stocksim.share.value.ShareVolume;

import java.util.function.BiConsumer;

import static pl.pszydlowski.stocksim.common.CssUtils.attachAction;
import static pl.pszydlowski.stocksim.common.CssUtils.attachCssClass;
import static pl.pszydlowski.stocksim.common.CssUtils.parent;
import static pl.pszydlowski.stocksim.bootstrap.CssProperties.BOX;
import static pl.pszydlowski.stocksim.bootstrap.CssProperties.BOX_HEADER;
import static pl.pszydlowski.stocksim.bootstrap.CssProperties.DATA_BUTTON;
import static pl.pszydlowski.stocksim.bootstrap.CssProperties.DATA_PROMPT;
import static pl.pszydlowski.stocksim.bootstrap.CssProperties.NAME_LABEL;

final class ShareBox extends VBox {

    private final Label shareName;
    private final DoubleLabel price;
    private final IntegerLabel volume;

    private final DoubleField priceEditField;
    private final IntegerField volumeEditField;

    private final Button editButton;
    private final Button removeButton;
    private final Button confirmButton;
    private final Button cancelButton;

    private final VBox buttonBox;
    private final Pane pricePane;
    private final Pane volumePane;

    ShareBox() {
        attachCssClass(this, BOX);

        price = new DoubleLabel();
        volume = new IntegerLabel();

        pricePane = attachCssClass(new Pane(), DATA_PROMPT);
        volumePane = attachCssClass(new Pane(), DATA_PROMPT);

        priceEditField = attachCssClass(new DoubleField(), DATA_PROMPT);
        volumeEditField = attachCssClass(new IntegerField(), DATA_PROMPT);

        editButton = attachAction(attachCssClass(new Button("edit"), DATA_BUTTON), e -> enterEditMode());
        cancelButton = attachAction(attachCssClass(new Button("cancel"), DATA_BUTTON), e -> enterVisualMode());
        confirmButton = attachCssClass(new Button("confirm"), DATA_BUTTON);
        removeButton = attachCssClass(new Button("remove"), DATA_BUTTON);


        final Label priceLabel = attachCssClass(new Label("price: "), NAME_LABEL);
        final HBox priceBox = parent(new HBox()).children(priceLabel, pricePane);

        final Label volumeLabel = attachCssClass(new Label("volume: "), NAME_LABEL);
        final HBox volumeBox = parent(new HBox()).children(volumeLabel, volumePane);

        final VBox dataBox = parent(new VBox()).children(priceBox, volumeBox);

        buttonBox = new VBox();
        final HBox mainBox = parent(new HBox()).children(dataBox, buttonBox);

        shareName = attachCssClass(new Label(), BOX_HEADER);
        parent(this).children(shareName, mainBox);

        enterVisualMode();
    }

    void setShareName(ShareId shareId) {
        shareName.setText(shareId.getValue());
    }

    void setPrice(SharePrice sharePrice) {
        price.setValue(sharePrice.getValue().doubleValue());
    }

    void setVolume(ShareVolume shareVolume) {
        volume.setValue(shareVolume.getValue().intValue());
    }

    void setRemoveAction(Runnable removeAction) {
        removeButton.setOnAction(e -> removeAction.run());
    }

    void setShareDataChangeAction(BiConsumer<ShareData, ShareData> shareDataChangeAction) {
        confirmButton.setOnAction(e -> {
            enterVisualMode();
            ShareData updatedData = getUpdatedData();
            shareDataChangeAction.accept(getPreviousData(), updatedData);
            setPrice(updatedData.getPrice());
            setVolume(updatedData.getVolume());
        });
    }

    private ShareData getPreviousData() {
        return new ShareData(new SharePrice(price.getValue()), new ShareVolume(volume.getValue()));
    }

    private ShareData getUpdatedData() {
        return new ShareData(new SharePrice(priceEditField.getValue()), new ShareVolume(volumeEditField.getValue()));
    }

    private void enterEditMode() {
        cleanContainers();
        priceEditField.setValue(price.getValue().doubleValue());
        volumeEditField.setValue(volume.getValue().intValue());
        buttonBox.getChildren().addAll(confirmButton, cancelButton);
        pricePane.getChildren().add(priceEditField);
        volumePane.getChildren().add(volumeEditField);
    }

    private void enterVisualMode() {
        cleanContainers();
        buttonBox.getChildren().addAll(editButton, removeButton);
        pricePane.getChildren().add(price);
        volumePane.getChildren().add(volume);
    }

    private void cleanContainers() {
        buttonBox.getChildren().clear();
        pricePane.getChildren().clear();
        volumePane.getChildren().clear();
    }
}
