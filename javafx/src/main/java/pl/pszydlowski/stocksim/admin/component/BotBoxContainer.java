package pl.pszydlowski.stocksim.admin.component;

import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import lombok.Setter;
import pl.pszydlowski.stocksim.shareholder.strategy.ShareholderActionStrategy;
import pl.pszydlowski.stocksim.shareholder.value.ShareholderId;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Supplier;

import static javafx.application.Platform.runLater;
import static pl.pszydlowski.stocksim.bootstrap.CssProperties.BOTTOM_BOX;
import static pl.pszydlowski.stocksim.bootstrap.CssProperties.CONTAINER_HEADER;
import static pl.pszydlowski.stocksim.bootstrap.CssProperties.HINT;
import static pl.pszydlowski.stocksim.bootstrap.CssProperties.NAME_PROMPT;
import static pl.pszydlowski.stocksim.bootstrap.CssProperties.SCROLLABLE_CONTAINER;
import static pl.pszydlowski.stocksim.bootstrap.CssProperties.STRATEGY_BOX;
import static pl.pszydlowski.stocksim.bootstrap.CssProperties.horizontalSeparator;
import static pl.pszydlowski.stocksim.common.CssUtils.attachAction;
import static pl.pszydlowski.stocksim.common.CssUtils.attachCssClass;
import static pl.pszydlowski.stocksim.common.CssUtils.attachPrompt;
import static pl.pszydlowski.stocksim.common.CssUtils.parent;
import static pl.pszydlowski.stocksim.login.LoginController.INVALID_NAME_MESSAGE;

public final class BotBoxContainer extends VBox {

    private static final String NO_STRATEGY_ASSIGNED = "No shareholder strategy has been chosen!";

    private final Map<ShareholderId, BotBox> boxesById;

    private final VBox list;
    private final TextField nameField;
    private final ComboBox<String> strategyBox;
    private final Label hintLabel;
    private final ScrollPane listContainer;

    private Map<String, Supplier<ShareholderActionStrategy>> strategiesByDescription;

    @Setter
    private Consumer<ShareholderId> botRemoveConsumer;

    @Setter
    private BiConsumer<ShareholderId, ShareholderActionStrategy> botAddConsumer;

    public BotBoxContainer() {
        boxesById = new HashMap<>();
        final Label header = attachCssClass(new Label("Session Bots"), CONTAINER_HEADER);

        listContainer = attachCssClass(new ScrollPane(), SCROLLABLE_CONTAINER);
        list = new VBox();
        listContainer.setContent(list);

        final Button addButton = attachAction(new Button("add"), e -> tryAddBotBox());
        nameField = attachPrompt(attachCssClass(new TextField(), NAME_PROMPT), "Enter bot name...");

        final HBox promptBox = parent(new HBox()).children(nameField, addButton);

        strategyBox = attachPrompt(attachCssClass(new ComboBox<>(), STRATEGY_BOX), "Select strategy");
        final VBox addShareBox = parent(new VBox()).children(strategyBox, promptBox);

        hintLabel = attachCssClass(new Label(), HINT);
        final VBox bottomBox = parent(attachCssClass(new VBox(), BOTTOM_BOX)).children(addShareBox, hintLabel);


        parent(this).children(header, horizontalSeparator(), listContainer, horizontalSeparator(), bottomBox);
    }

    public void setBots(List<ShareholderId> shareholders) {
        shareholders.forEach(this::addBotBox);
    }

    public void setAvailableStrategies(Map<String, Supplier<ShareholderActionStrategy>> strategiesByDescription) {
        this.strategiesByDescription = strategiesByDescription;
        strategyBox.getItems().clear();
        strategyBox.getItems().addAll(strategiesByDescription.keySet());
    }


    private void tryAddBotBox() {
        String shareholderName = nameField.getText();
        ShareholderActionStrategy strategy = strategiesByDescription.get(strategyBox.getValue()).get();
        if (!isValidShareholderName(shareholderName)) {
            hintLabel.setText(INVALID_NAME_MESSAGE);
        } else if (strategy == null) {
            hintLabel.setText(NO_STRATEGY_ASSIGNED);
        } else {
            ShareholderId shareholderId = new ShareholderId(shareholderName);
            addBotBox(shareholderId);
            botAddConsumer.accept(shareholderId, strategy);
        }
    }

    private void addBotBox(ShareholderId shareholderId) {
        if (!boxesById.containsKey(shareholderId)) {
            BotBox box = new BotBox();
            box.setShareholderName(shareholderId);
            box.setRemoveAction(() -> removeBot(shareholderId));
            boxesById.put(shareholderId, box);
            list.getChildren().add(box);
            runLater(() -> listContainer.setVvalue(1));
        }
    }

    public void removeBot(ShareholderId shareholderId) {
        BotBox box = boxesById.remove(shareholderId);
        if (box != null) {
            list.getChildren().remove(box);
            botRemoveConsumer.accept(shareholderId);
        }
    }

    private boolean isValidShareholderName(String name) {
        return name != null && !name.isEmpty() && name.matches("[a-zA-Z0-9]{5,20}");
    }
}