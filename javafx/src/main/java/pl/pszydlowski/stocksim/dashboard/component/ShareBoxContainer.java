package pl.pszydlowski.stocksim.dashboard.component;

import javafx.scene.control.ScrollPane;
import javafx.scene.layout.VBox;
import lombok.Setter;
import pl.pszydlowski.stocksim.share.value.ShareChange;
import pl.pszydlowski.stocksim.share.value.ShareId;
import pl.pszydlowski.stocksim.share.value.ShareVolume;
import pl.pszydlowski.stocksim.stock.value.StockResponse;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BiFunction;

import static java.util.Optional.ofNullable;
import static pl.pszydlowski.stocksim.bootstrap.CssProperties.SCROLLABLE_CONTAINER;
import static pl.pszydlowski.stocksim.bootstrap.CssProperties.SCROLLBAR;
import static pl.pszydlowski.stocksim.common.CssUtils.attachCssClass;
import static pl.pszydlowski.stocksim.common.CssUtils.parent;

public final class ShareBoxContainer extends VBox {

    private final Map<ShareId, ShareBox> boxesById;
    private final Map<ShareId, ShareVolume> ownedVolumes;
    private final VBox list;

    @Setter
    private BiFunction<ShareId, ShareVolume, StockResponse> buyFunction;
    @Setter
    private BiFunction<ShareId, ShareVolume, StockResponse> sellFunction;

    public ShareBoxContainer() {
        boxesById = new HashMap<>();
        ownedVolumes = new HashMap<>();

        final ScrollPane listContainer = attachCssClass(new ScrollPane(), SCROLLABLE_CONTAINER, SCROLLBAR);
        list = new VBox();
        listContainer.setContent(list);

        parent(this).children(listContainer);
    }

    public void onShareChange(ShareChange shareChange) {
        final ShareId shareId = shareChange.getId();
        final ShareBox shareBox = boxesById.get(shareId);
        if (shareBox != null) {
            shareBox.onShareChange(shareChange);
        } else {
            addShareBox(shareChange);
        }
    }

    public void onVolumeAssigned(ShareId shareId, ShareVolume volume) {
        final ShareBox shareBox = boxesById.get(shareId);
        if (shareBox != null) {
            shareBox.onVolumeAssigned(volume);
        } else {
            ownedVolumes.put(shareId, volume);
        }
    }

    public void removeShare(ShareId shareId) {
        ShareBox box = boxesById.remove(shareId);
        list.getChildren().remove(box);
    }

    private void addShareBox(ShareChange shareCreation) {
        final ShareId shareId = shareCreation.getId();
        final ShareBox shareBox = new ShareBox();

        shareBox.setAssociatedId(shareId);
        shareBox.onShareChange(shareCreation);
        shareBox.setBuyFunction(volume -> buyFunction.apply(shareId, volume));
        shareBox.setSellFunction(volume -> sellFunction.apply(shareId, volume));

        ofNullable(ownedVolumes.remove(shareId)).ifPresent(shareBox::onVolumeAssigned);

        list.getChildren().add(shareBox);
        boxesById.put(shareId, shareBox);
    }
}
