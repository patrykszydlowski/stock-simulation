package pl.pszydlowski.stocksim.dashboard.component;

import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import pl.pszydlowski.stocksim.common.component.DoubleLabel;
import pl.pszydlowski.stocksim.common.money.Wallet;

import static pl.pszydlowski.stocksim.bootstrap.CssProperties.WALLET_BOX;
import static pl.pszydlowski.stocksim.common.CssUtils.attachCssClass;
import static pl.pszydlowski.stocksim.common.CssUtils.parent;

public final class WalletBox extends HBox {

    private final DoubleLabel walletLabel;

    public WalletBox() {
        attachCssClass(this, WALLET_BOX);

        walletLabel = new DoubleLabel();
        walletLabel.setValue(0);


        final Label nameLabel = new Label("Current wallet: ");

        parent(this).children(nameLabel, walletLabel);
    }

    public void setWallet(Wallet wallet) {
        walletLabel.setValue(wallet.getValue().doubleValue());
    }
}
