package pl.pszydlowski.stocksim.dashboard.component;

import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import pl.pszydlowski.stocksim.bootstrap.CssProperties;
import pl.pszydlowski.stocksim.share.value.ShareData;

import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

import static pl.pszydlowski.stocksim.bootstrap.CssProperties.SMALL_MARGIN;
import static pl.pszydlowski.stocksim.bootstrap.CssProperties.horizontalSeparator;
import static pl.pszydlowski.stocksim.bootstrap.CssProperties.verticalSeparator;
import static pl.pszydlowski.stocksim.common.CssUtils.attachCssClass;
import static pl.pszydlowski.stocksim.common.CssUtils.parent;

final class ShareChartContainer extends HBox {

    private static final DateTimeFormatter FORMATTER = DateTimeFormatter
            .ofPattern("HH:mm:ss")
            .withLocale(Locale.US)
            .withZone(ZoneId.systemDefault());

    private final XYChart.Series<String, Number> priceSeries;
    private final XYChart.Series<String, Number> volumeSeries;

    ShareChartContainer() {
        attachCssClass(this, SMALL_MARGIN);

        priceSeries = new XYChart.Series<>();
        volumeSeries = new XYChart.Series<>();


        final CategoryAxis priceTimeAxis = new CategoryAxis();
        final CategoryAxis volumeTimeAxis = new CategoryAxis();
        final NumberAxis priceAxis = new NumberAxis();
        final NumberAxis volumeAxis = new NumberAxis();

        volumeTimeAxis.setLabel("Time");
        priceTimeAxis.setLabel("Time");
        priceAxis.setLabel("Price");
        volumeAxis.setLabel("Volume");

        final LineChart<String, Number> priceChart = new LineChart<>(priceTimeAxis, priceAxis);
        final LineChart<String, Number> volumeChart = new LineChart<>(volumeTimeAxis, volumeAxis);

        priceChart.setLegendVisible(false);
        volumeChart.setLegendVisible(false);

        priceChart.getData().add(priceSeries);
        volumeChart.getData().add(volumeSeries);

        parent(this).children(priceChart, verticalSeparator(), volumeChart);
    }

    void addEntry(Instant timestamp, ShareData data) {
        priceSeries.getData().add(new XYChart.Data<>(convertToReadableFormat(timestamp), data.getPrice().getValue()));
        volumeSeries.getData().add(new XYChart.Data<>(convertToReadableFormat(timestamp), data.getVolume().getValue()));
    }

    private static String convertToReadableFormat(Instant timestamp) {
        return FORMATTER.format(timestamp);
    }
}
