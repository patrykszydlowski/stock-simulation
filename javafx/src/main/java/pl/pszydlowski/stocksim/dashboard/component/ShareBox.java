package pl.pszydlowski.stocksim.dashboard.component;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import lombok.Setter;
import pl.pszydlowski.stocksim.common.component.DoubleLabel;
import pl.pszydlowski.stocksim.common.component.IntegerField;
import pl.pszydlowski.stocksim.common.component.IntegerLabel;
import pl.pszydlowski.stocksim.share.value.ShareChange;
import pl.pszydlowski.stocksim.share.value.ShareData;
import pl.pszydlowski.stocksim.share.value.ShareId;
import pl.pszydlowski.stocksim.share.value.ShareVolume;
import pl.pszydlowski.stocksim.stock.value.StockResponse;

import java.math.BigInteger;
import java.util.function.Function;

import static pl.pszydlowski.stocksim.bootstrap.CssProperties.ACTIVITY_BUTTON;
import static pl.pszydlowski.stocksim.bootstrap.CssProperties.BOX;
import static pl.pszydlowski.stocksim.bootstrap.CssProperties.BOX_HEADER;
import static pl.pszydlowski.stocksim.bootstrap.CssProperties.NAME_LABEL;
import static pl.pszydlowski.stocksim.bootstrap.CssProperties.RIGHT_BOX;
import static pl.pszydlowski.stocksim.bootstrap.CssProperties.horizontalSeparator;
import static pl.pszydlowski.stocksim.bootstrap.CssProperties.verticalSeparator;
import static pl.pszydlowski.stocksim.common.CssUtils.attachAction;
import static pl.pszydlowski.stocksim.common.CssUtils.attachCssClass;
import static pl.pszydlowski.stocksim.common.CssUtils.parent;

final class ShareBox extends VBox {

    private final Label shareName;
    private final ShareChartContainer chartContainer;

    private final DoubleLabel currentPriceLabel;
    private final IntegerLabel currentVolumeLabel;
    private final IntegerLabel ownedVolumeLabel;

    private final IntegerField volumeField;

    private ShareVolume ownedVolume;

    private final Label hintLabel;

    @Setter
    private Function<ShareVolume, StockResponse> buyFunction;
    @Setter
    private Function<ShareVolume, StockResponse> sellFunction;

    ShareBox() {
        attachCssClass(this, BOX);

        shareName = attachCssClass(new Label(), BOX_HEADER);
        chartContainer = new ShareChartContainer();

        currentPriceLabel = new DoubleLabel();
        currentVolumeLabel = new IntegerLabel();
        ownedVolumeLabel = new IntegerLabel();
        ownedVolumeLabel.setValue(0);
        ownedVolume = ShareVolume.ZERO;

        hintLabel = new Label();
        hintLabel.setWrapText(true);

        final Label currentPriceNameLabel = attachCssClass(new Label("current price: "), NAME_LABEL);
        final HBox currentPriceBox = parent(new HBox()).children(currentPriceNameLabel, currentPriceLabel);

        final Label currentVolumeNameLabel = attachCssClass(new Label("current volume: "), NAME_LABEL);
        final HBox currentVolumeBox = parent(new HBox()).children(currentVolumeNameLabel, currentVolumeLabel);

        final Label ownedVolumeNameLabel = attachCssClass(new Label("owned volume: "), NAME_LABEL);
        final HBox ownedVolumeBox = parent(new HBox()).children(ownedVolumeNameLabel, ownedVolumeLabel);
        final VBox labelBox = parent(new VBox()).children(currentPriceBox, currentVolumeBox, ownedVolumeBox);

        volumeField = new IntegerField();

        final Button buyButton = attachAction(attachCssClass(new Button("buy"), ACTIVITY_BUTTON), e -> tryBuy());
        final Button sellButton = attachAction(attachCssClass(new Button("sell"), ACTIVITY_BUTTON), e -> trySell());

        final HBox buttonBox = parent(new HBox()).children(buyButton, sellButton);
        final VBox activityBox = parent(new VBox()).children(volumeField, buttonBox, hintLabel);

        final VBox rightBox = parent(attachCssClass(new VBox(), RIGHT_BOX)).children(labelBox, horizontalSeparator(), activityBox);

        final HBox dataBox = parent(new HBox()).children(chartContainer, verticalSeparator(), rightBox);

        parent(this).children(shareName, horizontalSeparator(), dataBox);
    }

    void setAssociatedId(ShareId id) {
        shareName.setText(id.getValue());
    }

    void onShareChange(ShareChange change) {
        final ShareData currentData = change.getCurrentData();
        currentPriceLabel.setValue(currentData.getPrice().getValue().doubleValue());
        currentVolumeLabel.setValue(currentData.getVolume().getValue().intValue());
        chartContainer.addEntry(change.getTimestamp(), currentData);
    }

    void onVolumeAssigned(ShareVolume volume) {
        ownedVolume = volume;
        ownedVolumeLabel.setValue(ownedVolume.getValue().intValue());
    }

    private void tryBuy() {
        final BigInteger value = volumeField.getValue();
        if (value != null) {
            buy(new ShareVolume(value));
        }
    }

    private void trySell() {
        final BigInteger value = volumeField.getValue();
        if (value != null) {
            sell(new ShareVolume(value));
        }
    }

    private void buy(ShareVolume buyVolume) {
        StockResponse response = buyFunction.apply(buyVolume);
        showHints(response);
    }

    private void sell(ShareVolume sellVolume) {
        StockResponse response = sellFunction.apply(sellVolume);
        showHints(response);
    }

    private void showHints(StockResponse response) {
        switch (response.getCode()) {
            case SUCCESS:
                hintLabel.setText("");
                break;
            case INSUFFICIENT_OWNED_MONEY:
                hintLabel.setText("You don't have enough money to buy this amount of current share");
                break;
            case INSUFFICIENT_AVAILABLE_VOLUME:
                hintLabel.setText("There is not enough amount of current share for You to buy");
                break;
            case INSUFFICIENT_OWNED_VOLUME:
                hintLabel.setText("You don't own enough volume to sell this amount of current share");
                break;
            case INVALID_SHARE_ID:
                hintLabel.setText("There is no share of given id [DEV BUG]");
                break;
        }
    }
}
