package pl.pszydlowski.stocksim.dashboard;

import lombok.Setter;
import pl.pszydlowski.stocksim.common.money.Wallet;
import pl.pszydlowski.stocksim.dashboard.component.ShareBoxContainer;
import pl.pszydlowski.stocksim.dashboard.component.WalletBox;
import pl.pszydlowski.stocksim.share.value.ShareChange;
import pl.pszydlowski.stocksim.share.value.ShareId;
import pl.pszydlowski.stocksim.share.value.ShareVolume;
import pl.pszydlowski.stocksim.shareholder.InteractiveShareholder;
import pl.pszydlowski.stocksim.shareholder.value.ShareholderId;
import pl.pszydlowski.stocksim.stock.StockObserver;
import pl.pszydlowski.stocksim.stock.value.StockResponse;

import static javafx.application.Platform.runLater;

public final class DashboardController {

    public WalletBox walletBox;
    public ShareBoxContainer shareContainer;

    private InteractiveShareholder shareholder;

    @Setter
    private Runnable redirectToLoginAction;
    @Setter
    private Runnable redirectToAdminAction;


    public void initialize() {
        shareContainer.setBuyFunction(this::buy);
        shareContainer.setSellFunction(this::sell);
    }

    public void redirectToLoginPanel() {
        redirectToLoginAction.run();
    }

    public void redirectToAdminPanel() {
        redirectToAdminAction.run();
    }

    public void removeShare(ShareId shareId) {
        shareContainer.removeShare(shareId);
    }

    public StockObserver createShareholder(ShareholderId shareholderId) {
        shareholder = new InteractiveShareholder(shareholderId, this::onShareChange, this::onWalletAssigned, this::onVolumeAssigned);
        return shareholder;
    }

    private void onShareChange(ShareChange shareChange) {
        runLater(() -> shareContainer.onShareChange(shareChange));
    }

    private void onWalletAssigned(Wallet wallet) {
        walletBox.setWallet(wallet);
    }

    private void onVolumeAssigned(ShareId shareId, ShareVolume volume) {
        shareContainer.onVolumeAssigned(shareId, volume);
    }

    private StockResponse buy(ShareId id, ShareVolume buyVolume) {
        return shareholder.buy(id, buyVolume);
    }

    private StockResponse sell(ShareId id, ShareVolume sellVolume) {
        return shareholder.sell(id, sellVolume);
    }
}
