package pl.pszydlowski.stocksim.share

import pl.pszydlowski.stocksim.share.value.ShareId
import pl.pszydlowski.stocksim.share.value.SharePrice
import pl.pszydlowski.stocksim.share.value.ShareVolume
import pl.pszydlowski.stocksim.shareholder.value.ShareholderId
import spock.lang.Specification


class ShareSpec extends Specification {

    def "should correctly calculate total price of volume"() {
        expect:
            Share.getTotalPrice(SharePrice.of(pricePerShare), ShareVolume.of(volume)) == totalPrice
        where:
            pricePerShare | volume | totalPrice
            10            | 10     | 100
            1.5           | 15     | 22.5
            0             | 100    | 0
            3219.321      | 0      | 0
    }

    def "should not modify share owned volumes map when modifying queried volumes"() {
        given:
            def shareholder1 = new ShareholderId("test-shareholder-1")
            def ownedVolumes = [shareholder1: ShareVolume.of(100), shareholder2: ShareVolume.of(50)]
            def share = new Share(new ShareId("test-share"), ownedVolumes, SharePrice.ZERO, ShareVolume.ZERO)
        when:
            def queriedOwnedVolumes = new HashMap(share.getOwnedVolumes())
            queriedOwnedVolumes[shareholder1] = ShareVolume.ZERO
        then:
            queriedOwnedVolumes != ownedVolumes
    }

    def "should correctly compute current share data"() {
        given:
            def price = SharePrice.of(14)
            def volume = ShareVolume.of(100)
            def share = new Share(new ShareId("test"), price, volume)
        when:
            def data = share.getData()
        then:
            data.price == price
            data.volume == volume
    }

    def "should return volume of zero when shareholder does not own any volume"() {
        given:
            def shareholder = new ShareholderId("test-shareholder")
            def share = new Share(new ShareId("test-share"), SharePrice.ZERO, ShareVolume.ZERO)
        when:
            def shareholderOwnedVolume = share.getShareholderOwnedVolume(shareholder)
        then:
            shareholderOwnedVolume == ShareVolume.ZERO
    }

    def "should move volume from available volume to owned when setting new shareholder volume"() {
        given:
            def shareholder = new ShareholderId("test-shareholder")
            def availableVolume = ShareVolume.of(100)
            def shareholderVolume = ShareVolume.of(10)
            def share = new Share(new ShareId("test-share"), SharePrice.ZERO, availableVolume)
        when:
            share.setShareholderOwnedVolume(shareholder, shareholderVolume)
            def ownedVolume = share.getShareholderOwnedVolume(shareholder)
        then:
            share.availableVolume == availableVolume.subtract(shareholderVolume)
            ownedVolume == shareholderVolume
    }

    def "should indicate that share has enough available volume for purchase"() {
        given:
            def availableVolume = ShareVolume.of(100)
            def requestedVolume = ShareVolume.of(10)
            def share = new Share(new ShareId("test-share"), SharePrice.ZERO, availableVolume)
        expect:
            share.hasEnoughAvailableVolume(requestedVolume)
    }

    def "should indicate that share does not have enough available volume for purchase"() {
        given:
            def availableVolume = ShareVolume.of(10)
            def requestedVolume = ShareVolume.of(100)
            def share = new Share(new ShareId("test-share"), SharePrice.ZERO, availableVolume)
        expect:
            !share.hasEnoughAvailableVolume(requestedVolume)
    }

    def "should indicate that shareholder has enough owned volume for sale"() {
        given:
            def shareholder = new ShareholderId("test-shareholder")
            def availableVolume = ShareVolume.of(100)
            def ownedVolume = ShareVolume.of(50)
            def requestedVolume = ShareVolume.of(10)
            def share = new Share(new ShareId("test-share"), SharePrice.ZERO, availableVolume)
            share.setShareholderOwnedVolume(shareholder, ownedVolume)
        expect:
            share.hasEnoughOwnedVolume(shareholder, requestedVolume)
    }

    def "should indicate that shareholder does not have enough owned volume for sale"() {
        given:
            def shareholder = new ShareholderId("test-shareholder")
            def availableVolume = ShareVolume.of(100)
            def ownedVolume = ShareVolume.of(50)
            def requestedVolume = ShareVolume.of(55)
            def share = new Share(new ShareId("test-share"), SharePrice.ZERO, availableVolume)
            share.setShareholderOwnedVolume(shareholder, ownedVolume)
        expect:
            !share.hasEnoughOwnedVolume(shareholder, requestedVolume)
    }
}