package pl.pszydlowski.stocksim.share.value


import spock.lang.Specification

class ShareVolumeSpec extends Specification {

    def "should create a new volume when adding volumes"() {
        given:
            def firstValue = 149
            def secondValue = 76
            def firstVolume = ShareVolume.of(firstValue)
            def secondVolume = ShareVolume.of(secondValue)
        when:
            def volumeSum = firstVolume.add(secondVolume)
        then:
            volumeSum.value == firstValue + secondValue
            firstVolume.value == firstValue
            secondVolume.value == secondValue
    }

    def "should create a new volume when subtracting volumes"() {
        given:
            def firstValue = 149
            def secondValue = 76
            def firstVolume = ShareVolume.of(firstValue)
            def secondVolume = ShareVolume.of(secondValue)
        when:
            def volumeDifference = firstVolume.subtract(secondVolume)
        then:
            volumeDifference.value == firstValue - secondValue
            firstVolume.value == firstValue
            secondVolume.value == secondValue
    }

    def "should correctly compare volumes"() {
        expect:
            biggerVolume > smallerVolume
        where:
            biggerVolume        | smallerVolume
            ShareVolume.of(100) | ShareVolume.of(50)
            ShareVolume.of(13)  | ShareVolume.of(12)
            ShareVolume.of(1)   | ShareVolume.of(0)
            ShareVolume.of(1)   | ShareVolume.ZERO
    }
}
