package pl.pszydlowski.stocksim.share.value

import spock.lang.Specification

class SharePriceSpec extends Specification {

    def "should create a new price when adding prices"() {
        given:
            def firstValue = 149
            def secondValue = 76
            def firstPrice = SharePrice.of(firstValue)
            def secondPrice = SharePrice.of(secondValue)
        when:
            def priceSum = firstPrice.add(secondPrice)
        then:
            priceSum.value == firstValue + secondValue
            firstPrice.value == firstValue
            secondPrice.value == secondValue
    }

    def "should create a new price when subtracting prices"() {
        given:
            def firstValue = 149
            def secondValue = 76
            def firstPrice = SharePrice.of(firstValue)
            def secondPrice = SharePrice.of(secondValue)
        when:
            def priceDifference = firstPrice.subtract(secondPrice)
        then:
            priceDifference.value == firstValue - secondValue
            firstPrice.value == firstValue
            secondPrice.value == secondValue
    }

    def "should correctly compare prices"() {
        expect:
            biggerPrice > smallerPrice
        where:
            biggerPrice        | smallerPrice
            SharePrice.of(100) | SharePrice.of(50)
            SharePrice.of(13)  | SharePrice.of(12)
            SharePrice.of(1)   | SharePrice.of(0)
            SharePrice.of(1)   | SharePrice.ZERO
    }
}
