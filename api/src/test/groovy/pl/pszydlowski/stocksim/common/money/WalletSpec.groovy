package pl.pszydlowski.stocksim.common.money

import spock.lang.Specification


class WalletSpec extends Specification {

    def "should indicate wallets with same money value as equal"() {
        expect:
            firstWallet == secondWallet
        where:
            firstWallet                         | secondWallet
            Wallet.EMPTY                        | Wallet.EMPTY
            Wallet.of(10)                       | Wallet.of(10)
            new Wallet(BigDecimal.valueOf(110)) | new Wallet(BigDecimal.valueOf(110))
    }

    def "should create a new wallet when adding wallets"() {
        given:
            def firstValue = 14
            def secondValue = 76
            def firstWallet = Wallet.of(firstValue)
            def secondWallet = Wallet.of(secondValue)
        when:
            def walletSum = firstWallet.add(secondWallet)
        then:
            walletSum.value == firstValue + secondValue
            firstWallet.value == firstValue
            secondWallet.value == secondValue
    }

    def "should create a new wallet when subtracting wallets"() {
        given:
            def firstValue = 149
            def secondValue = 76
            def firstWallet = Wallet.of(firstValue)
            def secondWallet = Wallet.of(secondValue)
        when:
            def walletDifference = firstWallet.subtract(secondWallet)
        then:
            walletDifference.value == firstValue - secondValue
            firstWallet.value == firstValue
            secondWallet.value == secondValue
    }

    def "should indicate that wallet can afford an expense"() {
        given:
            def walletMoney = 1000
            def expense = walletMoney - 70
            def wallet = Wallet.of(walletMoney)
        expect:
            wallet.canAfford(BigDecimal.valueOf(expense))
    }

    def "should indicate that wallet cannot afford an expense"() {
        given:
            def walletMoney = 1000
            def expense = walletMoney + 70
            def wallet = Wallet.of(walletMoney)
        expect:
            !wallet.canAfford(BigDecimal.valueOf(expense))
    }
}