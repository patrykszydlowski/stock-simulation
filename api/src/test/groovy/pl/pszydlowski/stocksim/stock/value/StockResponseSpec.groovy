package pl.pszydlowski.stocksim.stock.value

import pl.pszydlowski.stocksim.common.money.Wallet
import pl.pszydlowski.stocksim.share.value.ShareVolume
import spock.lang.Specification

class StockResponseSpec extends Specification {

    def "should correctly create success response"() {
        given:
            def wallet = Wallet.EMPTY
            def volume = ShareVolume.ZERO
            def action = StockResponse.success(wallet, volume)
        expect:
            action.code == StockResponseCode.SUCCESS
            action.shareholderWallet == wallet
            action.ownedVolume == volume
    }

    def "should correctly create failure response"() {
        given:
            def wallet = Wallet.EMPTY
            def action = StockResponse.failure(StockResponseCode.INVALID_SHARE_ID, wallet)
        expect:
            action.code == StockResponseCode.INVALID_SHARE_ID
            action.shareholderWallet == wallet
    }
}
