package pl.pszydlowski.stocksim.shareholder.strategy

import pl.pszydlowski.stocksim.share.value.ShareId
import pl.pszydlowski.stocksim.share.value.ShareVolume
import spock.lang.Specification

class ShareholderActionSpec extends Specification {

    def "should correctly create pass action"() {
        given:
            def shareId = new ShareId("test-share")
            def action = ShareholderAction.pass(shareId)
        expect:
            action.shareId == shareId
            action.code == ShareholderActionCode.PASS
            action.volume == ShareVolume.ZERO
    }

    def "should correctly create buy action"() {
        given:
            def shareId = new ShareId("test-share")
            def volume = ShareVolume.of(100)
            def action = ShareholderAction.buy(shareId, volume)
        expect:
            action.shareId == shareId
            action.code == ShareholderActionCode.BUY
            action.volume == volume
    }

    def "should correctly create sell action"() {
        given:
            def shareId = new ShareId("test-share")
            def volume = ShareVolume.of(100)
            def action = ShareholderAction.sell(shareId, volume)
        expect:
            action.shareId == shareId
            action.code == ShareholderActionCode.SELL
            action.volume == volume
    }
}
