package pl.pszydlowski.stocksim.stock;

import pl.pszydlowski.stocksim.common.money.Wallet;
import pl.pszydlowski.stocksim.common.observer.Observer;
import pl.pszydlowski.stocksim.share.value.ShareChange;
import pl.pszydlowski.stocksim.share.value.ShareId;
import pl.pszydlowski.stocksim.share.value.SharePrice;
import pl.pszydlowski.stocksim.share.value.ShareVolume;

public interface StockObserver extends Observer {
    void onShareChange(ShareChange shareChange);

    void onShareRemoved(SharePrice price, ShareVolume ownedVolume);

    void onWalletAssigned(Wallet wallet);

    void onVolumeAssigned(ShareId shareId, ShareVolume ownedVolume);

    void onStockServiceAssigned(StockService stockService);
}
