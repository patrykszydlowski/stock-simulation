package pl.pszydlowski.stocksim.stock.value;

import lombok.NonNull;
import lombok.Value;
import pl.pszydlowski.stocksim.common.money.Wallet;
import pl.pszydlowski.stocksim.share.value.ShareVolume;

@Value
public final class StockResponse {
    @NonNull
    StockResponseCode code;
    @NonNull
    Wallet shareholderWallet;
    @NonNull
    ShareVolume ownedVolume;

    public static StockResponse success(Wallet wallet, ShareVolume ownedVolume) {
        return new StockResponse(StockResponseCode.SUCCESS, wallet, ownedVolume);
    }

    public static StockResponse failure(StockResponseCode failureCode, Wallet originalWaller) {
        return new StockResponse(failureCode, originalWaller, ShareVolume.ZERO);
    }
}
