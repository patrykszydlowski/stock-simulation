package pl.pszydlowski.stocksim.stock;

import pl.pszydlowski.stocksim.share.value.ShareId;
import pl.pszydlowski.stocksim.share.value.ShareVolume;
import pl.pszydlowski.stocksim.shareholder.value.ShareholderId;
import pl.pszydlowski.stocksim.stock.value.StockResponse;

public interface StockService {
    StockResponse requestBuy(ShareholderId shareholderId, ShareId shareId, ShareVolume buyVolume);

    StockResponse requestSell(ShareholderId shareholderId, ShareId shareId, ShareVolume sellVolume);
}
