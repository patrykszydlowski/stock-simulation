package pl.pszydlowski.stocksim.stock.value;

import lombok.NonNull;
import lombok.Value;

import java.util.concurrent.TimeUnit;

@Value
public final class UpdateInterval {
    long interval;
    @NonNull
    TimeUnit timeUnit;
}
