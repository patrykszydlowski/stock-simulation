package pl.pszydlowski.stocksim.stock;

import pl.pszydlowski.stocksim.share.value.ShareData;
import pl.pszydlowski.stocksim.share.value.ShareId;

public interface StockUpdateDispatcher {
    void start();

    void addShareTransaction(ShareId shareId, ShareData shareData);

    void stop();
}
