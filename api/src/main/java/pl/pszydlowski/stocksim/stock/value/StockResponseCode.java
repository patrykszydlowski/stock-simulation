package pl.pszydlowski.stocksim.stock.value;

public enum StockResponseCode {
    SUCCESS,
    INVALID_SHARE_ID,
    INSUFFICIENT_OWNED_MONEY,
    INSUFFICIENT_AVAILABLE_VOLUME,
    INSUFFICIENT_OWNED_VOLUME
}
