package pl.pszydlowski.stocksim.stock;

import pl.pszydlowski.stocksim.common.observer.Observable;

public interface StockObservable extends Observable<StockObserver> {
}
