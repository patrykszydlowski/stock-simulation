package pl.pszydlowski.stocksim.stock;

import pl.pszydlowski.stocksim.share.value.ShareChange;
import pl.pszydlowski.stocksim.share.value.ShareData;
import pl.pszydlowski.stocksim.share.value.ShareId;
import pl.pszydlowski.stocksim.stock.strategy.StockUpdateStrategy;

import java.util.List;
import java.util.Optional;

public interface StockUpdateApplier {
    Optional<ShareChange> updateShare(ShareId shareId, List<ShareData> shareHistory);

    StockUpdateStrategy getStockUpdateStrategy();

    void setStockUpdateStrategy(StockUpdateStrategy strategy);
}
