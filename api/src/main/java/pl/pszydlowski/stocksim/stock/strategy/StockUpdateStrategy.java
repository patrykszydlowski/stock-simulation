package pl.pszydlowski.stocksim.stock.strategy;

import pl.pszydlowski.stocksim.share.value.ShareData;
import pl.pszydlowski.stocksim.share.value.ShareId;
import pl.pszydlowski.stocksim.share.value.SharePrice;

public interface StockUpdateStrategy {
    SharePrice computeNewPrice(ShareId shareId, ShareData currentData);
}
