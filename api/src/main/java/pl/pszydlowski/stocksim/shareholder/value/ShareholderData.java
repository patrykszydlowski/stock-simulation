package pl.pszydlowski.stocksim.shareholder.value;

import lombok.NonNull;
import lombok.Value;
import pl.pszydlowski.stocksim.common.money.Wallet;

@Value
public final class ShareholderData {
    @NonNull
    ShareholderId id;
    @NonNull
    Wallet wallet;
}
