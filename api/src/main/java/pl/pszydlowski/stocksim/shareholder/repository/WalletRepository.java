package pl.pszydlowski.stocksim.shareholder.repository;

import pl.pszydlowski.stocksim.common.money.Wallet;
import pl.pszydlowski.stocksim.common.repository.Repository;
import pl.pszydlowski.stocksim.shareholder.value.ShareholderId;

public interface WalletRepository extends Repository<Wallet, ShareholderId> {
}
