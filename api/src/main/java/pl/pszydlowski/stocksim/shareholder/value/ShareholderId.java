package pl.pszydlowski.stocksim.shareholder.value;

import lombok.NonNull;
import lombok.Value;

@Value
public final class ShareholderId {
    @NonNull
    String value;

    @Override
    public String toString() {
        return value;
    }
}
