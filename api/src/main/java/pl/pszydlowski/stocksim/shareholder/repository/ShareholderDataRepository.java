package pl.pszydlowski.stocksim.shareholder.repository;

import pl.pszydlowski.stocksim.common.repository.Repository;
import pl.pszydlowski.stocksim.shareholder.value.ShareholderData;
import pl.pszydlowski.stocksim.shareholder.value.ShareholderId;

public interface ShareholderDataRepository extends Repository<ShareholderData, ShareholderId> {
}
