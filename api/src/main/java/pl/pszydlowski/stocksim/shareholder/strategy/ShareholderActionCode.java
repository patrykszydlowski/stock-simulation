package pl.pszydlowski.stocksim.shareholder.strategy;

public enum ShareholderActionCode {
    BUY, SELL, PASS
}
