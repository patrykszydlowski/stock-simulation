package pl.pszydlowski.stocksim.shareholder.strategy;

import lombok.NonNull;
import lombok.Value;
import pl.pszydlowski.stocksim.share.value.ShareId;
import pl.pszydlowski.stocksim.share.value.ShareVolume;

@Value
public final class ShareholderAction {
    @NonNull
    ShareholderActionCode code;
    @NonNull
    ShareId shareId;
    @NonNull
    ShareVolume volume;

    public static ShareholderAction pass(ShareId id) {
        return new ShareholderAction(ShareholderActionCode.PASS, id, ShareVolume.ZERO);
    }

    public static ShareholderAction buy(ShareId id, ShareVolume volume) {
        return new ShareholderAction(ShareholderActionCode.BUY, id, volume);
    }

    public static ShareholderAction sell(ShareId id, ShareVolume volume) {
        return new ShareholderAction(ShareholderActionCode.SELL, id, volume);
    }
}
