package pl.pszydlowski.stocksim.shareholder.strategy;

import pl.pszydlowski.stocksim.common.money.Wallet;
import pl.pszydlowski.stocksim.share.value.ShareChange;
import pl.pszydlowski.stocksim.share.value.ShareVolume;

public interface ShareholderActionStrategy {
    ShareholderAction computeAction(ShareChange shareChange, Wallet shareholderWallet, ShareVolume ownedVolume);
}
