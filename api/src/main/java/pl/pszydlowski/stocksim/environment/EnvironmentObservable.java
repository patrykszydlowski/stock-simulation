package pl.pszydlowski.stocksim.environment;

import pl.pszydlowski.stocksim.common.observer.Observable;

public interface EnvironmentObservable extends Observable<EnvironmentObserver> {
}
