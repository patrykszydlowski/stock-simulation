package pl.pszydlowski.stocksim.environment;

import pl.pszydlowski.stocksim.common.money.Wallet;
import pl.pszydlowski.stocksim.common.observer.Observer;
import pl.pszydlowski.stocksim.share.value.ShareId;
import pl.pszydlowski.stocksim.share.value.SharePrice;
import pl.pszydlowski.stocksim.share.value.ShareVolume;
import pl.pszydlowski.stocksim.shareholder.value.ShareholderId;
import pl.pszydlowski.stocksim.stock.StockObserver;
import pl.pszydlowski.stocksim.stock.strategy.StockUpdateStrategy;

public interface EnvironmentObserver extends Observer {
    void onEnvironmentStart();

    void onEnvironmentStop();

    void onStockUpdateStrategyChange(StockUpdateStrategy stockUpdateStrategy);

    void onShareholderAdded(ShareholderId shareholderId, StockObserver shareholder);

    void onShareholderWalletChange(ShareholderId shareholderId, Wallet wallet);

    void onShareholderRemoved(ShareholderId shareholderId);

    void onShareAdded(ShareId shareId, SharePrice price, ShareVolume volume);

    void onSharePriceChange(ShareId shareId, SharePrice updatedPrice);

    void onShareVolumeChange(ShareId shareId, ShareVolume updatedVolume);

    void onShareRemoved(ShareId shareId);
}
