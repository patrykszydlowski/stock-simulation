package pl.pszydlowski.stocksim.common.money;

import lombok.NonNull;
import lombok.Value;
import pl.pszydlowski.stocksim.common.math.Operable;

import java.math.BigDecimal;

@Value
public final class Wallet implements Comparable<Wallet>, Operable<Wallet> {

    public static final Wallet EMPTY = of(0);

    @NonNull
    BigDecimal value;

    public static Wallet of(double value) {
        return new Wallet(BigDecimal.valueOf(value));
    }

    @Override
    public Wallet add(Wallet wallet) {
        return new Wallet(value.add(wallet.value));
    }

    @Override
    public Wallet subtract(Wallet wallet) {
        return new Wallet(value.subtract(wallet.value));
    }

    @Override
    public int compareTo(Wallet wallet) {
        return value.compareTo(wallet.value);
    }

    public boolean canAfford(BigDecimal expense) {
        return value.compareTo(expense) >= 0;
    }
}
