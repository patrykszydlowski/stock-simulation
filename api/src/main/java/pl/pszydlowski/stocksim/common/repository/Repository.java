package pl.pszydlowski.stocksim.common.repository;

import java.util.List;
import java.util.Optional;

public interface Repository<T, ID> {
    Optional<T> findById(ID id);

    List<T> findAll();

    void save(ID id, T t);

    Optional<T> remove(ID id);
}
