package pl.pszydlowski.stocksim.common.observer;

public interface Observable<T extends Observer> {
    void register(T observer);

    void unregister(T observer);
}
