package pl.pszydlowski.stocksim.common.math;

public interface Operable<T> {
    T add(T t);

    T subtract(T t);
}
