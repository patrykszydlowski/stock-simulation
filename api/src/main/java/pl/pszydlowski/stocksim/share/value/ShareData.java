package pl.pszydlowski.stocksim.share.value;

import lombok.NonNull;
import lombok.Value;

@Value
public final class ShareData {
    @NonNull
    SharePrice price;
    @NonNull
    ShareVolume volume;
}
