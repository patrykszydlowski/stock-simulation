package pl.pszydlowski.stocksim.share.repository;

import pl.pszydlowski.stocksim.common.repository.Repository;
import pl.pszydlowski.stocksim.share.Share;
import pl.pszydlowski.stocksim.share.value.ShareId;

public interface ShareRepository extends Repository<Share, ShareId> {
}
