package pl.pszydlowski.stocksim.share;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import pl.pszydlowski.stocksim.share.value.ShareData;
import pl.pszydlowski.stocksim.share.value.ShareId;
import pl.pszydlowski.stocksim.share.value.SharePrice;
import pl.pszydlowski.stocksim.share.value.ShareVolume;
import pl.pszydlowski.stocksim.shareholder.value.ShareholderId;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static java.util.Collections.unmodifiableMap;
import static java.util.Optional.ofNullable;

@Data
public final class Share {

    @Getter(AccessLevel.NONE)
    @Setter(AccessLevel.NONE)
    private final Map<ShareholderId, ShareVolume> ownedVolumes;

    private final ShareId id;

    private SharePrice currentPrice;
    private ShareVolume availableVolume;

    public Share(@NonNull ShareId id, @NonNull Map<ShareholderId, ShareVolume> ownedVolumes,
                 @NonNull SharePrice currentPrice, @NonNull ShareVolume availableVolume) {
        this.id = id;
        this.ownedVolumes = ownedVolumes;
        this.currentPrice = currentPrice;
        this.availableVolume = availableVolume;
    }

    public Share(@NonNull ShareId id, @NonNull SharePrice currentPrice, @NonNull ShareVolume availableVolume) {
        this.id = id;
        this.ownedVolumes = new ConcurrentHashMap<>();
        this.currentPrice = currentPrice;
        this.availableVolume = availableVolume;
    }

    public static BigDecimal getTotalPrice(SharePrice pricePerShare, ShareVolume volume) {
        return pricePerShare.getValue().multiply(new BigDecimal(volume.getValue()));
    }

    public Map<ShareholderId, ShareVolume> getOwnedVolumes() {
        return unmodifiableMap(new HashMap<>(ownedVolumes));
    }

    public ShareData getData() {
        return new ShareData(currentPrice, availableVolume);
    }

    public ShareVolume getShareholderOwnedVolume(ShareholderId id) {
        return ownedVolumes.getOrDefault(id, ShareVolume.ZERO);
    }

    public void setShareholderOwnedVolume(ShareholderId id, ShareVolume newVolume) {
        final ShareVolume ownedVolume = getShareholderOwnedVolume(id);
        final ShareVolume volumeDifference = newVolume.subtract(ownedVolume);

        availableVolume = availableVolume.subtract(volumeDifference);
        ownedVolumes.put(id, newVolume);
    }

    public boolean hasEnoughOwnedVolume(ShareholderId id, ShareVolume requestVolume) {
        return getShareholderOwnedVolume(id).compareTo(requestVolume) >= 0;
    }

    public boolean hasEnoughAvailableVolume(ShareVolume requestVolume) {
        return availableVolume.compareTo(requestVolume) >= 0;
    }

    public boolean shareholderOwnsAnyVolume(ShareholderId id) {
        return ofNullable(ownedVolumes.get(id))
                .filter(volume -> volume.compareTo(ShareVolume.ZERO) > 0)
                .isPresent();
    }
}
