package pl.pszydlowski.stocksim.share.value;

import lombok.NonNull;
import lombok.Value;
import pl.pszydlowski.stocksim.common.math.Operable;

import java.math.BigInteger;

@Value
public final class ShareVolume implements Operable<ShareVolume>, Comparable<ShareVolume> {

    public static final ShareVolume ZERO = of(0);

    @NonNull
    BigInteger value;

    public static ShareVolume of(long value) {
        return new ShareVolume(BigInteger.valueOf(value));
    }

    @Override
    public ShareVolume add(ShareVolume shareVolume) {
        return new ShareVolume(value.add(shareVolume.value));
    }

    @Override
    public ShareVolume subtract(ShareVolume shareVolume) {
        return new ShareVolume(value.subtract(shareVolume.value));
    }

    @Override
    public int compareTo(ShareVolume shareVolume) {
        return value.compareTo(shareVolume.value);
    }

    @Override
    public String toString() {
        return value.toString();
    }
}
