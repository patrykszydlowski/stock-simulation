package pl.pszydlowski.stocksim.share.value;

import lombok.NonNull;
import lombok.Value;

@Value
public final class ShareId {
    @NonNull
    String value;

    @Override
    public String toString() {
        return value;
    }
}
