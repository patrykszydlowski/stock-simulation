package pl.pszydlowski.stocksim.share.value;

import lombok.NonNull;
import lombok.Value;

import java.time.Instant;

@Value
public final class ShareChange {
    @NonNull
    ShareId id;
    @NonNull
    ShareData previousData;
    @NonNull
    ShareData currentData;
    @NonNull
    Instant timestamp;

    public boolean priceChanged() {
        return currentData.getPrice().compareTo(previousData.getPrice()) != 0;
    }

    public boolean volumeChanged() {
        return currentData.getVolume().compareTo(previousData.getVolume()) != 0;
    }
}
