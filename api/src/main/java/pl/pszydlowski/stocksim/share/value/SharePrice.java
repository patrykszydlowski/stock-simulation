package pl.pszydlowski.stocksim.share.value;

import lombok.NonNull;
import lombok.Value;
import pl.pszydlowski.stocksim.common.math.Operable;

import java.math.BigDecimal;

@Value
public final class SharePrice implements Operable<SharePrice>, Comparable<SharePrice> {

    public static final SharePrice ZERO = of(0);

    @NonNull
    BigDecimal value;

    public static SharePrice of(double value) {
        return new SharePrice(BigDecimal.valueOf(value));
    }

    @Override
    public SharePrice add(SharePrice sharePrice) {
        return new SharePrice(value.add(sharePrice.value));
    }

    @Override
    public SharePrice subtract(SharePrice sharePrice) {
        return new SharePrice(value.subtract(sharePrice.value));
    }

    @Override
    public int compareTo(SharePrice sharePrice) {
        return value.compareTo(sharePrice.value);
    }

    @Override
    public String toString() {
        return value.toPlainString();
    }
}
