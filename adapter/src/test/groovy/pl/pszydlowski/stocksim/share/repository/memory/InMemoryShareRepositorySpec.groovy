package pl.pszydlowski.stocksim.share.repository.memory


import pl.pszydlowski.stocksim.share.Share
import pl.pszydlowski.stocksim.share.value.ShareId
import pl.pszydlowski.stocksim.share.value.SharePrice
import pl.pszydlowski.stocksim.share.value.ShareVolume
import spock.lang.Specification

class InMemoryShareRepositorySpec extends Specification {

    def "should save shares to delegate"() {
        given:
            def repo = new InMemoryShareRepository()
            def shareId = new ShareId("test-share")
            def share = createShare(shareId)
        when:
            repo.save(shareId, share)
        then:
            repo.delegate.containsKey(shareId)
            repo.delegate.containsValue(share)
            repo.delegate.size() == 1
    }

    def "should get share from delegate"() {
        given:
            def repo = new InMemoryShareRepository()
            def shareId = new ShareId("test-share")
            def share = createShare(shareId)
            repo.save(shareId, share)
        when:
            def foundShare = repo.findById(shareId)
        then:
            foundShare.isPresent()
            foundShare.filter { it == share }.isPresent()
    }

    def "should return empty optional if share not found in delegate"() {
        given:
            def repo = new InMemoryShareRepository()
            def shareId = new ShareId("test-share")
        when:
            def foundShare = repo.findById(shareId)
        then:
            !foundShare.isPresent()
    }

    def "should remove share from delegate"() {
        given:
            def repo = new InMemoryShareRepository()
            def shareId = new ShareId("test-share")
            def share = createShare(shareId)
            repo.save(shareId, share)
        when:
            def foundShare = repo.remove(shareId)
        then:
            foundShare.isPresent()
            foundShare.filter { it == share }.isPresent()
            repo.delegate.isEmpty()
    }

    def "should return all shares from delegate"() {
        given:
            def repo = new InMemoryShareRepository()
            def shareId1 = new ShareId("test-share-1")
            def shareId2 = new ShareId("test-share-2")
            def share1 = createShare(shareId1)
            def share2 = createShare(shareId2)
            repo.save(shareId1, share1)
            repo.save(shareId2, share2)
        when:
            def foundShares = repo.findAll()
        then:
            foundShares.size() == 2
            foundShares.containsAll([share1, share2])
    }

    private static Share createShare(ShareId id) {
        return new Share(id, SharePrice.ZERO, ShareVolume.ZERO)
    }
}
