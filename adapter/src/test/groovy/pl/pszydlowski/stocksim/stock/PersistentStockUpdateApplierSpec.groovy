package pl.pszydlowski.stocksim.stock

import pl.pszydlowski.stocksim.share.Share
import pl.pszydlowski.stocksim.share.repository.ShareRepository
import pl.pszydlowski.stocksim.share.value.ShareId
import pl.pszydlowski.stocksim.share.value.SharePrice
import pl.pszydlowski.stocksim.share.value.ShareVolume
import pl.pszydlowski.stocksim.stock.strategy.StockUpdateStrategy
import spock.lang.Specification

class PersistentStockUpdateApplierSpec extends Specification {

    def "should not update price if share cannot be found in repository"() {
        given:
            def shareId = new ShareId("test-share")
            def data = []

            def repository = Mock(ShareRepository) {
                1 * findById(shareId) >> Optional.empty()
            }

            def strategy = Mock(StockUpdateStrategy) {
                0 * _
            }

            def updateApplier = new PersistentStockUpdateApplier(repository, strategy)
        when:
            def shareChange = updateApplier.updateShare(shareId, data)
        then:
            !shareChange.isPresent()
    }

    def "should apply price change and store new share data in repository"() {
        given:
            def shareId = new ShareId("test-share")
            def share = new Share(shareId, SharePrice.ZERO, ShareVolume.ZERO)
            def data = share.getData()
            def dataList = [data]
            def calculatedPrice = SharePrice.of(100)

            def repository = Mock(ShareRepository) {
                1 * findById(shareId) >> Optional.of(share)
            }

            def strategy = Mock(StockUpdateStrategy) {
                1 * computeNewPrice(shareId, data) >> calculatedPrice
            }

            def updateApplier = new PersistentStockUpdateApplier(repository, strategy)
        when:
            def shareChange = updateApplier.updateShare(shareId, dataList)
        then:
            shareChange.isPresent()
            def change = shareChange.get()
            change.id == shareId
            change.currentData.price == calculatedPrice
    }
}
