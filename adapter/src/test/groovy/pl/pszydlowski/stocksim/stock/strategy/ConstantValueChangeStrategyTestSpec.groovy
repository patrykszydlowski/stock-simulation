package pl.pszydlowski.stocksim.stock.strategy

import pl.pszydlowski.stocksim.share.value.ShareData
import pl.pszydlowski.stocksim.share.value.ShareId
import pl.pszydlowski.stocksim.share.value.SharePrice
import pl.pszydlowski.stocksim.share.value.ShareVolume
import spock.lang.Specification

class ConstantValueChangeStrategyTestSpec extends Specification {

    def "should always calculate new price based on given ratio"() {
        when:
            def strategy = new ConstantValueChangeStrategy(ratio)
            def share = new ShareId("test-share")
            def data = new ShareData(SharePrice.of(price), ShareVolume.ZERO)
            def computedPrice = strategy.computeNewPrice(share, data)
        then:
            computedPrice == SharePrice.of(expectedPrice)
        where:
            price | ratio   | expectedPrice
            100   | 0.01    | 1
            1     | 10      | 10
            1000  | 1.27    | 1270
            0     | 1000000 | 0
            10000 | 0       | 0
    }
}
