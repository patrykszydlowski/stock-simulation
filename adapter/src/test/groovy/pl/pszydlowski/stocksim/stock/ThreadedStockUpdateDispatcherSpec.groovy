package pl.pszydlowski.stocksim.stock

import pl.pszydlowski.stocksim.share.value.*
import pl.pszydlowski.stocksim.stock.value.UpdateInterval
import spock.lang.Specification
import spock.util.concurrent.PollingConditions

import java.time.Instant
import java.util.concurrent.Executors
import java.util.concurrent.ScheduledExecutorService
import java.util.concurrent.ScheduledFuture
import java.util.function.Supplier

import static java.util.concurrent.TimeUnit.SECONDS

class ThreadedStockUpdateDispatcherSpec extends Specification {

    def interval = new UpdateInterval(1, SECONDS)

    def "should start dispatcher"() {
        given:
            def future = Mock(ScheduledFuture)

            def executor = Mock(ScheduledExecutorService) {
                1 * scheduleAtFixedRate(_, 0, 1, SECONDS) >> future
            }

            def executorSupplier = Mock(Supplier) {
                1 * get() >> executor
            }

            def updateApplier = Mock(StockUpdateApplier)

            def updateDispatcher = new ThreadedStockUpdateDispatcher(executorSupplier, [], updateApplier, interval)
        when:
            updateDispatcher.start()
        then:
            updateDispatcher.running
            updateDispatcher.currentService == executor
            updateDispatcher.dispatchingFunction == future
    }

    def "should stop dispatcher once started"() {
        given:
            def future = Mock(ScheduledFuture)

            def executor = Mock(ScheduledExecutorService) {
                1 * scheduleAtFixedRate(_, 0, 1, SECONDS) >> future
            }

            def executorSupplier = Mock(Supplier) {
                1 * get() >> executor
            }

            def updateApplier = Mock(StockUpdateApplier)

            def updateDispatcher = new ThreadedStockUpdateDispatcher(executorSupplier, [], updateApplier, interval)
            updateDispatcher.start()
        when:
            updateDispatcher.stop()
        then:
            !updateDispatcher.running
            updateDispatcher.currentService == executor
            updateDispatcher.dispatchingFunction == future
    }

    def "should add transactions to cached queue"() {
        given:
            def shareId = new ShareId("test-share")
            def shareData1 = new ShareData(SharePrice.ZERO, ShareVolume.ZERO)
            def shareData2 = new ShareData(SharePrice.ZERO, ShareVolume.ZERO)
            def executorSupplier = Mock(Supplier)
            def updateApplier = Mock(StockUpdateApplier)

            def updateDispatcher = new ThreadedStockUpdateDispatcher(executorSupplier, [], updateApplier, interval)
        when:
            updateDispatcher.addShareTransaction(shareId, shareData1)
            updateDispatcher.addShareTransaction(shareId, shareData2)
        then:
            updateDispatcher.cachedShareChanges.size() == 1
            updateDispatcher.cachedShareChanges.containsKey(shareId)
            updateDispatcher.cachedShareChanges.containsValue([shareData1, shareData2])
    }

    def "should publish all leftover changes after stopping dispatcher"() {
        given:
            def shareId = new ShareId("test-share")
            def shareData = new ShareData(SharePrice.ZERO, ShareVolume.ZERO)
            def shareChange = new ShareChange(shareId, shareData, shareData, Instant.now())

            def future = Mock(ScheduledFuture) {
                1 * cancel(false) >> true
            }

            def executor = Mock(ScheduledExecutorService) {
                1 * scheduleAtFixedRate(_, 0, 1, SECONDS) >> future
            }

            def executorSupplier = Mock(Supplier) {
                1 * get() >> executor
            }

            def updateApplier = Mock(StockUpdateApplier) {
                1 * updateShare(shareId, [shareData]) >> Optional.of(shareChange)
            }

            def observer1 = Mock(StockObserver)
            def observer2 = Mock(StockObserver)

            def observers = [observer1, observer2]

            def updateDispatcher = new ThreadedStockUpdateDispatcher(executorSupplier, observers, updateApplier, interval)
            updateDispatcher.addShareTransaction(shareId, shareData)
            updateDispatcher.start()
        when:
            updateDispatcher.stop()
        then:
            !updateDispatcher.running
            updateDispatcher.currentService == executor
            updateDispatcher.dispatchingFunction == future
            updateDispatcher.cachedShareChanges.isEmpty()
            1 * executor.shutdown()
            1 * observer1.onShareChange(shareChange)
            1 * observer2.onShareChange(shareChange)
    }

    def "should notify observers of share changes when dispatcher is running"() {
        given:
            def conditions = new PollingConditions(initialDelay: 0.01, timeout: 5, factor: 1)

            def shareId = new ShareId("test-share")
            def shareData = new ShareData(SharePrice.ZERO, ShareVolume.ZERO)
            def shareChange = new ShareChange(shareId, shareData, shareData, Instant.now())

            def executorSupplier = { Executors.newSingleThreadScheduledExecutor() }

            def updateApplier = Mock(StockUpdateApplier) {
                1 * updateShare(shareId, [shareData]) >> Optional.of(shareChange)
            }

            def observer1 = Mock(StockObserver) {
                1 * onShareChange(shareChange)
            }
            def observer2 = Mock(StockObserver) {
                1 * onShareChange(shareChange)
            }

            def observers = [observer1, observer2]

            def updateDispatcher = new ThreadedStockUpdateDispatcher(executorSupplier, observers, updateApplier, interval)
            updateDispatcher.start()
        when:
            updateDispatcher.addShareTransaction(shareId, shareData)
        then:
            updateDispatcher.running
        cleanup:
            updateDispatcher.stop()
    }
}
