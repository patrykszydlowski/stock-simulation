package pl.pszydlowski.stocksim.stock

import pl.pszydlowski.stocksim.common.money.Wallet
import pl.pszydlowski.stocksim.share.Share
import pl.pszydlowski.stocksim.share.repository.ShareRepository
import pl.pszydlowski.stocksim.share.value.ShareId
import pl.pszydlowski.stocksim.share.value.SharePrice
import pl.pszydlowski.stocksim.share.value.ShareVolume
import pl.pszydlowski.stocksim.shareholder.repository.ShareholderDataRepository
import pl.pszydlowski.stocksim.shareholder.value.ShareholderData
import pl.pszydlowski.stocksim.shareholder.value.ShareholderId
import pl.pszydlowski.stocksim.stock.value.StockResponseCode
import spock.lang.Specification

class PersistentSchedulingStockServiceSpec extends Specification {

    def "should return response with invalid share id and empty wallet on purchase if wallet and share do not exist in repository"() {
        given:
            def shareholderId = new ShareholderId("test-shareholder")
            def shareId = new ShareId("test-share")

            def buyVolume = ShareVolume.of(100)

            def shareRepository = Mock(ShareRepository) {
                1 * findById(shareId) >> Optional.empty()
            }

            def dataRepository = Mock(ShareholderDataRepository) {
                1 * findById(shareholderId) >> Optional.empty()
            }

            def dispatcher = Mock(StockUpdateDispatcher)

            def service = new PersistentSchedulingStockService(shareRepository, dataRepository, dispatcher)
        when:
            def stockResponse = service.requestBuy(shareholderId, shareId, buyVolume)
        then:
            0 * dispatcher.addShareTransaction(shareId, _)
            stockResponse.code == StockResponseCode.INVALID_SHARE_ID
            stockResponse.shareholderWallet == Wallet.EMPTY
    }

    def "should return response with insufficient available volume if share has not enough volume for purchase"() {
        given:
            def shareholderId = new ShareholderId("test-shareholder")
            def shareId = new ShareId("test-share")

            def buyVolume = ShareVolume.of(100)

            def share = new Share(shareId, SharePrice.ZERO, ShareVolume.ZERO)

            def shareRepository = Mock(ShareRepository) {
                1 * findById(shareId) >> Optional.of(share)
            }

            def dataRepository = Mock(ShareholderDataRepository) {
                1 * findById(shareholderId) >> Optional.empty()
            }

            def dispatcher = Mock(StockUpdateDispatcher)

            def service = new PersistentSchedulingStockService(shareRepository, dataRepository, dispatcher)
        when:
            def stockResponse = service.requestBuy(shareholderId, shareId, buyVolume)
        then:
            0 * dispatcher.addShareTransaction(shareId, _)
            stockResponse.code == StockResponseCode.INSUFFICIENT_AVAILABLE_VOLUME
    }

    def "should return response with insufficient owned money and unchanged wallet when buyer has not enough money for purchase"() {
        given:
            def shareholderId = new ShareholderId("test-shareholder")
            def shareId = new ShareId("test-share")

            def buyVolume = ShareVolume.of(2)
            def sharePrice = SharePrice.of(1000)
            def shareVolume = ShareVolume.of(1000)
            def buyerWallet = Wallet.of(1999)

            def share = new Share(shareId, sharePrice, shareVolume)

            def shareRepository = Mock(ShareRepository) {
                1 * findById(shareId) >> Optional.of(share)
            }

            def dataRepository = Mock(ShareholderDataRepository) {
                1 * findById(shareholderId) >> Optional.of(new ShareholderData(shareholderId, buyerWallet))
            }

            def dispatcher = Mock(StockUpdateDispatcher)

            def service = new PersistentSchedulingStockService(shareRepository, dataRepository, dispatcher)
        when:
            def stockResponse = service.requestBuy(shareholderId, shareId, buyVolume)
        then:
            0 * dispatcher.addShareTransaction(shareId, _)
            stockResponse.code == StockResponseCode.INSUFFICIENT_OWNED_MONEY
            stockResponse.shareholderWallet == buyerWallet
    }

    def "should save performed purchase and add transaction to update dispatcher"() {
        given:
            def shareholderId = new ShareholderId("test-shareholder")
            def shareId = new ShareId("test-share")

            def buyVolume = 10
            def totalPrice = 100
            def pricePerShare = 10

            def sharePrice = SharePrice.of(pricePerShare)
            def shareVolume = ShareVolume.of(1000)
            def buyerWallet = Wallet.of(totalPrice)

            def expectedResponseWallet = Wallet.EMPTY

            def share = new Share(shareId, sharePrice, shareVolume)

            def shareRepository = Mock(ShareRepository) {
                1 * findById(shareId) >> Optional.of(share)
            }

            def dataRepository = Mock(ShareholderDataRepository) {
                1 * findById(shareholderId) >> Optional.of(new ShareholderData(shareholderId, buyerWallet))
            }

            def dispatcher = Mock(StockUpdateDispatcher)

            def service = new PersistentSchedulingStockService(shareRepository, dataRepository, dispatcher)
        when:
            def stockResponse = service.requestBuy(shareholderId, shareId, ShareVolume.of(buyVolume))
        then:
            1 * shareRepository.save(shareId, _)
            1 * dataRepository.save(shareholderId, new ShareholderData(shareholderId, expectedResponseWallet))
        then:
            1 * dispatcher.addShareTransaction(shareId, _)
            stockResponse.code == StockResponseCode.SUCCESS
            stockResponse.shareholderWallet == expectedResponseWallet
    }

    def "should return response with invalid share id and empty wallet on sale if wallet and share do not exist in repository"() {
        given:
            def shareholderId = new ShareholderId("test-shareholder")
            def shareId = new ShareId("test-share")

            def buyVolume = ShareVolume.of(100)

            def shareRepository = Mock(ShareRepository) {
                1 * findById(shareId) >> Optional.empty()
            }

            def dataRepository = Mock(ShareholderDataRepository) {
                1 * findById(shareholderId) >> Optional.empty()
            }

            def dispatcher = Mock(StockUpdateDispatcher)

            def service = new PersistentSchedulingStockService(shareRepository, dataRepository, dispatcher)
        when:
            def stockResponse = service.requestSell(shareholderId, shareId, buyVolume)
        then:
            0 * dispatcher.addShareTransaction(shareId, _)
            stockResponse.code == StockResponseCode.INVALID_SHARE_ID
            stockResponse.shareholderWallet == Wallet.EMPTY
    }

    def "should return response with insufficient owned volume if shareholder has not own volume for sale"() {
        given:
            def shareholderId = new ShareholderId("test-shareholder")
            def shareId = new ShareId("test-share")

            def sellVolume = ShareVolume.of(100)

            def share = new Share(shareId, SharePrice.ZERO, ShareVolume.ZERO)

            def shareRepository = Mock(ShareRepository) {
                1 * findById(shareId) >> Optional.of(share)
            }

            def dataRepository = Mock(ShareholderDataRepository) {
                1 * findById(shareholderId) >> Optional.empty()
            }

            def dispatcher = Mock(StockUpdateDispatcher)

            def service = new PersistentSchedulingStockService(shareRepository, dataRepository, dispatcher)
        when:
            def stockResponse = service.requestSell(shareholderId, shareId, sellVolume)
        then:
            0 * dispatcher.addShareTransaction(shareId, _)
            stockResponse.code == StockResponseCode.INSUFFICIENT_OWNED_VOLUME
    }

    def "should save performed sale and add transaction to update dispatcher"() {
        given:
            def shareholderId = new ShareholderId("test-shareholder")
            def shareId = new ShareId("test-share")

            def sellVolume = 10
            def pricePerShare = 10
            def totalPrice = 100

            def sharePrice = SharePrice.of(pricePerShare)
            def shareVolume = ShareVolume.of(1000)
            def sellerWallet = Wallet.of(totalPrice)

            def ownedVolumes = [(shareholderId): ShareVolume.of(100)]

            def expectedResponseWallet = Wallet.of(totalPrice + totalPrice)

            def share = new Share(shareId, ownedVolumes, sharePrice, shareVolume)

            def shareRepository = Mock(ShareRepository) {
                1 * findById(shareId) >> Optional.of(share)
            }

            def dataRepository = Mock(ShareholderDataRepository) {
                1 * findById(shareholderId) >> Optional.of(new ShareholderData(shareholderId, sellerWallet))
            }

            def dispatcher = Mock(StockUpdateDispatcher)

            def service = new PersistentSchedulingStockService(shareRepository, dataRepository, dispatcher)
        when:
            def stockResponse = service.requestSell(shareholderId, shareId, ShareVolume.of(sellVolume))
        then:
            1 * shareRepository.save(shareId, _)
            1 * dataRepository.save(shareholderId, new ShareholderData(shareholderId, expectedResponseWallet))
        then:
            1 * dispatcher.addShareTransaction(shareId, _)
            stockResponse.code == StockResponseCode.SUCCESS
            stockResponse.shareholderWallet == expectedResponseWallet
    }
}
