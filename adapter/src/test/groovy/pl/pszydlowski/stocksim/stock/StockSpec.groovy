package pl.pszydlowski.stocksim.stock

import pl.pszydlowski.stocksim.common.money.Wallet
import pl.pszydlowski.stocksim.share.Share
import pl.pszydlowski.stocksim.share.repository.ShareRepository
import pl.pszydlowski.stocksim.share.repository.memory.InMemoryShareRepository
import pl.pszydlowski.stocksim.share.value.ShareId
import pl.pszydlowski.stocksim.share.value.SharePrice
import pl.pszydlowski.stocksim.share.value.ShareVolume
import pl.pszydlowski.stocksim.shareholder.repository.ShareholderDataRepository
import pl.pszydlowski.stocksim.shareholder.repository.memory.InMemoryShareholderDataRepository
import pl.pszydlowski.stocksim.shareholder.value.ShareholderData
import pl.pszydlowski.stocksim.shareholder.value.ShareholderId
import pl.pszydlowski.stocksim.stock.strategy.StockUpdateStrategy
import pl.pszydlowski.stocksim.stock.value.UpdateInterval
import spock.lang.Specification

import java.util.concurrent.ScheduledExecutorService
import java.util.concurrent.ScheduledFuture
import java.util.concurrent.TimeUnit
import java.util.function.Supplier

class StockSpec extends Specification {

    def interval = new UpdateInterval(1, TimeUnit.SECONDS)
    def startingWallet = Wallet.EMPTY

    def "should queue up all shares in repository to dispatcher"() {
        given:
            def shareId1 = new ShareId("test-share-1")
            def shareId2 = new ShareId("test-share-2")
            def share1 = new Share(shareId1, SharePrice.ZERO, ShareVolume.ZERO)
            def share2 = new Share(shareId2, SharePrice.ZERO, ShareVolume.ZERO)

            def shareRepository = new InMemoryShareRepository();

            shareRepository.save(shareId1, share1)
            shareRepository.save(shareId2, share2)

            def dataRepository = Mock(ShareholderDataRepository)
            def executorSupplier = Mock(Supplier)

            def stock = new Stock(shareRepository, dataRepository, executorSupplier, interval, startingWallet)
        expect:
            def changesCache = stock.updateDispatcher.cachedShareChanges
            changesCache.size() == 2
            changesCache.get(shareId1) == [share1.getData()]
            changesCache.get(shareId2) == [share2.getData()]
    }

    def "should register observer to stock"() {
        given:
            def shareRepository = new InMemoryShareRepository();
            def dataRepository = Mock(ShareholderDataRepository)
            def executorSupplier = Mock(Supplier)
            def observer = Mock(StockObserver)

            def stock = new Stock(shareRepository, dataRepository, executorSupplier, interval, startingWallet)
        when:
            stock.register(observer)
        then:
            stock.observers.size() == 1
            stock.observers.contains(observer)
    }

    def "should unregister observer from stock"() {
        given:
            def shareRepository = new InMemoryShareRepository();
            def dataRepository = Mock(ShareholderDataRepository)
            def executorSupplier = Mock(Supplier)
            def observer = Mock(StockObserver)

            def stock = new Stock(shareRepository, dataRepository, executorSupplier, interval, startingWallet)
            stock.register(observer)
        when:
            stock.unregister(observer)
        then:
            stock.observers.isEmpty()
    }

    def "should start update dispatcher when notifying stock of environment start"() {
        given:
            def shareRepository = new InMemoryShareRepository();
            def dataRepository = Mock(ShareholderDataRepository)

            def future = Mock(ScheduledFuture) {
                1 * cancel(false) >> true
            }

            def executor = Mock(ScheduledExecutorService) {
                1 * scheduleAtFixedRate(_, _, _, _) >> future
            }

            def executorSupplier = Mock(Supplier) {
                1 * get() >> executor
            }

            def stock = new Stock(shareRepository, dataRepository, executorSupplier, interval, startingWallet)
        when:
            stock.onEnvironmentStart()
        then:
            stock.updateDispatcher.running
        cleanup:
            stock.updateDispatcher.stop()
    }

    def "should stop update dispatcher when notifying stock of environment stop"() {
        given:
            def shareRepository = new InMemoryShareRepository();
            def dataRepository = Mock(ShareholderDataRepository)

            def future = Mock(ScheduledFuture) {
                1 * cancel(false) >> true
            }

            def executor = Mock(ScheduledExecutorService) {
                1 * scheduleAtFixedRate(_, _, _, _) >> future
            }

            def executorSupplier = Mock(Supplier) {
                1 * get() >> executor
            }

            def stock = new Stock(shareRepository, dataRepository, executorSupplier, interval, startingWallet)
            stock.onEnvironmentStart()
        when:
            stock.onEnvironmentStop()
        then:
            !stock.updateDispatcher.running
    }

    def "should set new strategy for update applier when notifying stock of environment strategy change"() {
        given:
            def shareRepository = new InMemoryShareRepository();
            def dataRepository = Mock(ShareholderDataRepository)
            def strategy = Mock(StockUpdateStrategy)
            def executorSupplier = Mock(Supplier)
            def stock = new Stock(shareRepository, dataRepository, executorSupplier, interval, startingWallet)
        when:
            stock.onStockUpdateStrategyChange(strategy)
        then:
            stock.updateApplier.stockUpdateStrategy == strategy
    }

    def "should save new shareholder wallet to repository and notify him after notifying stock on shareholder add"() {
        given:
            def shareholderId = new ShareholderId("test-shareholder")
            def shareholder = Mock(StockObserver)

            def shareRepository = new InMemoryShareRepository()
            def dataRepository = Mock(ShareholderDataRepository) {
                1 * findById(shareholderId) >> Optional.empty()
                1 * save(shareholderId, new ShareholderData(shareholderId, startingWallet))
            }
            def stock = new Stock(shareRepository, dataRepository, Mock(Supplier), interval, startingWallet)
        when:
            stock.onShareholderAdded(shareholderId, shareholder)
        then:
            stock.observers.size() == 1
            stock.observers.contains(shareholder)
            1 * shareholder.onWalletAssigned(startingWallet)
            1 * shareholder.onStockServiceAssigned(_)
    }

    def "should save modified shareholder wallet to repository and notify him after notifying stock on shareholder wallet change"() {
        given:
            def shareholderId = new ShareholderId("test-shareholder")
            def shareholder = Mock(StockObserver)
            def updatedWallet = Wallet.of(100)

            def shareRepository = new InMemoryShareRepository()
            def dataRepository = new InMemoryShareholderDataRepository()
            def stock = new Stock(shareRepository, dataRepository, Mock(Supplier), interval, startingWallet)
            stock.onShareholderAdded(shareholderId, shareholder)
        when:
            stock.onShareholderWalletChange(shareholderId, updatedWallet)
        then:
            stock.observers.size() == 1
            stock.observers.contains(shareholder)
            1 * shareholder.onWalletAssigned(updatedWallet)
    }

    def "should unregister shareholder from stock when removing shareholder from environment"() {
        given:
            def shareholderId = new ShareholderId("test-shareholder")
            def shareholder = Mock(StockObserver)

            def shareRepository = new InMemoryShareRepository()
            def dataRepository = new InMemoryShareholderDataRepository()
            def stock = new Stock(shareRepository, dataRepository, Mock(Supplier), interval, startingWallet)
            stock.onShareholderAdded(shareholderId, shareholder)
        when:
            stock.onShareholderRemoved(shareholderId)
        then:
            stock.observers.isEmpty()
    }

    def "should save new share to repository and add transaction to dispatcher queue"() {
        given:
            def shareId = new ShareId("test-share")
            def price = SharePrice.of(100)
            def volume = ShareVolume.of(10)
            def share = new Share(shareId, price, volume)

            def shareRepository = Mock(ShareRepository) {
                1 * findAll() >> []
                1 * findById(shareId) >> Optional.empty()
                1 * save(shareId, share)
            }
            def dataRepository = new InMemoryShareholderDataRepository()
            def stock = new Stock(shareRepository, dataRepository, Mock(Supplier), interval, startingWallet)
        when:
            stock.onShareAdded(shareId, price, volume)
        then:
            def changesCache = stock.updateDispatcher.cachedShareChanges
            changesCache.size() == 1
            changesCache.get(shareId) == [share.getData()]
    }

    def "should change share price in repository and add transaction to dispatcher queue"() {
        given:
            def shareId = new ShareId("test-share")
            def preUpdateShare = new Share(shareId, SharePrice.ZERO, ShareVolume.ZERO)
            def updatedPrice = SharePrice.of(100)
            def updatedShare = new Share(shareId, updatedPrice, ShareVolume.ZERO)

            def shareRepository = new InMemoryShareRepository()
            def dataRepository = new InMemoryShareholderDataRepository()
            def stock = new Stock(shareRepository, dataRepository, Mock(Supplier), interval, startingWallet)
            stock.onShareAdded(shareId, SharePrice.ZERO, ShareVolume.ZERO)
        when:
            stock.onSharePriceChange(shareId, updatedPrice)
        then:
            def changesCache = stock.updateDispatcher.cachedShareChanges
            changesCache.size() == 1
            changesCache.get(shareId) == [preUpdateShare.getData(), preUpdateShare.getData(), updatedShare.getData()]
    }

    def "should change share volume in repository and add transaction to dispatcher queue"() {
        given:
            def shareId = new ShareId("test-share")
            def preUpdateShare = new Share(shareId, SharePrice.ZERO, ShareVolume.ZERO)
            def updatedVolume = ShareVolume.of(100)
            def updatedShare = new Share(shareId, SharePrice.ZERO, updatedVolume)

            def shareRepository = new InMemoryShareRepository()
            def dataRepository = new InMemoryShareholderDataRepository()
            def stock = new Stock(shareRepository, dataRepository, Mock(Supplier), interval, startingWallet)
            stock.onShareAdded(shareId, SharePrice.ZERO, ShareVolume.ZERO)
        when:
            stock.onShareVolumeChange(shareId, updatedVolume)
        then:
            def changesCache = stock.updateDispatcher.cachedShareChanges
            changesCache.size() == 1
            changesCache.get(shareId) == [preUpdateShare.getData(), preUpdateShare.getData(), updatedShare.getData()]
    }

    def "should remove share from repository and notify all observers owning any volume of given share"() {
        given:
            def shareId = new ShareId("test-share")
            def price = SharePrice.of(100);
            def ownedVolume = ShareVolume.of(100)
            def shareholderId = new ShareholderId("test-share-owner")
            def shareOwner = Mock(StockObserver)

            def differentShareholderId = new ShareholderId("test-different-shareholder")
            def differentObserver = Mock(StockObserver)

            def ownedVolumes = [(shareholderId): ownedVolume]
            def share = new Share(shareId, ownedVolumes, price, ShareVolume.ZERO)

            def shareRepository = new InMemoryShareRepository()
            def dataRepository = new InMemoryShareholderDataRepository()

            def stock = new Stock(shareRepository, dataRepository, Mock(Supplier), interval, startingWallet)

            shareRepository.save(shareId, share)
            stock.onShareholderAdded(shareholderId, shareOwner)
            stock.onShareholderAdded(differentShareholderId, differentObserver)
        when:
            stock.onShareRemoved(shareId)
        then:
            1 * shareOwner.onShareRemoved(price, ownedVolume)
            0 * differentObserver.onShareRemoved(price, _)
    }
}
