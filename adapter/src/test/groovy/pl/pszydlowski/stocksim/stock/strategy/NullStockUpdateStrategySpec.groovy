package pl.pszydlowski.stocksim.stock.strategy

import pl.pszydlowski.stocksim.share.value.ShareData
import pl.pszydlowski.stocksim.share.value.ShareId
import pl.pszydlowski.stocksim.share.value.SharePrice
import pl.pszydlowski.stocksim.share.value.ShareVolume
import spock.lang.Specification
import spock.lang.Subject

class NullStockUpdateStrategySpec extends Specification {

    @Subject
    def strategy = NullStockUpdateStrategy.instance

    def "should ensure only one instance of strategy"() {
        expect:
            strategy == NullStockUpdateStrategy.getInstance()
    }

    def "should always return given price"() {
        when:
            def share = new ShareId("test-share")
            def data = new ShareData(price, ShareVolume.ZERO)
            def computedPrice = strategy.computeNewPrice(share, data)
        then:
            computedPrice == price
        where:
            price << [
                    SharePrice.ZERO,
                    SharePrice.of(100),
                    new SharePrice(BigDecimal.ZERO),
                    new SharePrice(BigDecimal.valueOf(100))
            ]
    }
}
