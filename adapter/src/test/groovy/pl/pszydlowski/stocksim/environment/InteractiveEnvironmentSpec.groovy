package pl.pszydlowski.stocksim.environment

import pl.pszydlowski.stocksim.common.money.Wallet
import pl.pszydlowski.stocksim.share.repository.ShareRepository
import pl.pszydlowski.stocksim.share.value.ShareId
import pl.pszydlowski.stocksim.share.value.SharePrice
import pl.pszydlowski.stocksim.share.value.ShareVolume
import pl.pszydlowski.stocksim.shareholder.repository.ShareholderDataRepository
import pl.pszydlowski.stocksim.shareholder.value.ShareholderId
import pl.pszydlowski.stocksim.stock.StockObserver
import pl.pszydlowski.stocksim.stock.strategy.StockUpdateStrategy
import spock.lang.Specification
import spock.lang.Subject

class InteractiveEnvironmentSpec extends Specification {

    @Subject
    InteractiveEnvironment env

    def setup() {
        env = new InteractiveEnvironment(Mock(ShareRepository), Mock(ShareholderDataRepository))
    }

    def "should register observer to environment"() {
        given:
            def observer = Mock(EnvironmentObserver)
        when:
            env.register(observer)
        then:
            env.observers.contains(observer)
    }

    def "should unregister observer from environment"() {
        given:
            def observer = Mock(EnvironmentObserver)
            env.register(observer)
        when:
            env.unregister(observer)
        then:
            env.observers.isEmpty()
    }

    def "should register all observers passed as arguments to environment"() {
        given:
            def observer1 = Mock(EnvironmentObserver)
            def observer2 = Mock(EnvironmentObserver)
            def observer3 = Mock(EnvironmentObserver)
        when:
            def env = new InteractiveEnvironment(Mock(ShareRepository), Mock(ShareholderDataRepository), observer1, observer2, observer3)
        then:
            env.observers == [observer1, observer2, observer3]
    }

    def "should notify observers on environment start"() {
        given:
            def observer1 = Mock(EnvironmentObserver)
            def observer2 = Mock(EnvironmentObserver)
            env.register(observer1)
            env.register(observer2)
        when:
            env.start()
        then:
            1 * observer1.onEnvironmentStart()
            1 * observer2.onEnvironmentStart()
    }

    def "should notify observers on environment stop"() {
        given:
            def observer1 = Mock(EnvironmentObserver)
            def observer2 = Mock(EnvironmentObserver)
            env.register(observer1)
            env.register(observer2)
        when:
            env.stop()
        then:
            1 * observer1.onEnvironmentStop()
            1 * observer2.onEnvironmentStop()
    }

    def "should notify observers when changing stock update strategy"() {
        given:
            def observer1 = Mock(EnvironmentObserver)
            def observer2 = Mock(EnvironmentObserver)
            env.register(observer1)
            env.register(observer2)

            def strategy = Mock(StockUpdateStrategy)
        when:
            env.changeStockUpdateStrategy(strategy)
        then:
            1 * observer1.onStockUpdateStrategyChange(strategy)
            1 * observer2.onStockUpdateStrategyChange(strategy)
    }

    def "should notify observers when adding share to environment"() {
        given:
            def observer1 = Mock(EnvironmentObserver)
            def observer2 = Mock(EnvironmentObserver)
            env.register(observer1)
            env.register(observer2)

            def share = new ShareId("test-share")
            def price = SharePrice.ZERO
            def volume = ShareVolume.ZERO
        when:
            env.addShare(share, price, volume)
        then:
            1 * observer1.onShareAdded(share, price, volume)
            1 * observer2.onShareAdded(share, price, volume)
    }

    def "should notify observers when changing share price"() {
        given:
            def observer1 = Mock(EnvironmentObserver)
            def observer2 = Mock(EnvironmentObserver)
            env.register(observer1)
            env.register(observer2)

            def share = new ShareId("test-share")
            def price = SharePrice.ZERO
        when:
            env.changeSharePrice(share, price)
        then:
            1 * observer1.onSharePriceChange(share, price)
            1 * observer2.onSharePriceChange(share, price)
    }

    def "should notify observers when changing share volume"() {
        given:
            def observer1 = Mock(EnvironmentObserver)
            def observer2 = Mock(EnvironmentObserver)
            env.register(observer1)
            env.register(observer2)

            def share = new ShareId("test-share")
            def volume = ShareVolume.ZERO
        when:
            env.changeShareVolume(share, volume)
        then:
            1 * observer1.onShareVolumeChange(share, volume)
            1 * observer2.onShareVolumeChange(share, volume)
    }

    def "should notify observers when removing share from environment"() {
        given:
            def observer1 = Mock(EnvironmentObserver)
            def observer2 = Mock(EnvironmentObserver)
            env.register(observer1)
            env.register(observer2)

            def share = new ShareId("test-share")
        when:
            env.removeShare(share)
        then:
            1 * observer1.onShareRemoved(share)
            1 * observer2.onShareRemoved(share)
    }

    def "should notify observers when adding shareholder to environment"() {
        given:
            def observer1 = Mock(EnvironmentObserver)
            def observer2 = Mock(EnvironmentObserver)
            env.register(observer1)
            env.register(observer2)

            def shareholder = new ShareholderId("test-shareholder")
            def shareholderMock = Mock(StockObserver)
        when:
            env.addShareholder(shareholder, shareholderMock)
        then:
            1 * observer1.onShareholderAdded(shareholder, shareholderMock)
            1 * observer2.onShareholderAdded(shareholder, shareholderMock)
    }

    def "should notify observers when changing shareholder to environment"() {
        given:
            def observer1 = Mock(EnvironmentObserver)
            def observer2 = Mock(EnvironmentObserver)
            env.register(observer1)
            env.register(observer2)

            def shareholder = new ShareholderId("test-shareholder")
            def wallet = Wallet.EMPTY
        when:
            env.changeShareholderWallet(shareholder, wallet)
        then:
            1 * observer1.onShareholderWalletChange(shareholder, wallet)
            1 * observer2.onShareholderWalletChange(shareholder, wallet)
    }

    def "should notify observers when removing shareholder from environment"() {
        given:
            def observer1 = Mock(EnvironmentObserver)
            def observer2 = Mock(EnvironmentObserver)
            env.register(observer1)
            env.register(observer2)

            def shareholder = new ShareholderId("test-shareholder")
        when:
            env.removeShareholder(shareholder)
        then:
            1 * observer1.onShareholderRemoved(shareholder)
            1 * observer2.onShareholderRemoved(shareholder)
    }
}
