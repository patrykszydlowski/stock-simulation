package pl.pszydlowski.stocksim.shareholder

import pl.pszydlowski.stocksim.common.money.Wallet
import pl.pszydlowski.stocksim.share.value.*
import pl.pszydlowski.stocksim.shareholder.strategy.ShareholderAction
import pl.pszydlowski.stocksim.shareholder.strategy.ShareholderActionStrategy
import pl.pszydlowski.stocksim.shareholder.value.ShareholderId
import pl.pszydlowski.stocksim.stock.StockService
import pl.pszydlowski.stocksim.stock.value.StockResponse
import spock.lang.Specification
import spock.lang.Subject

import java.time.Instant

class StrategyBasedShareholderSpec extends Specification {

    def id = new ShareholderId("test-shareholder")

    @Subject
    StrategyBasedShareholder shareholder

    def setup() {
        shareholder = new StrategyBasedShareholder(id)
    }

    void "should pass on share change"() {
        given:
            def share = new ShareId("test-share")
            def shareData = new ShareData(SharePrice.ZERO, ShareVolume.ZERO)
            def shareChange = new ShareChange(share, shareData, shareData, Instant.now())

            def passingStrategy = Mock(ShareholderActionStrategy) {
                1 * computeAction(shareChange, _, _) >> ShareholderAction.pass(shareChange.id)
                0 * _
            }

            def service = Mock(StockService)

            shareholder.onStockServiceAssigned(service)
            shareholder.setStrategy(passingStrategy)
        when:
            shareholder.onShareChange(shareChange)
        then:
            0 * service._
    }

    void "should buy on share change"() {
        given:
            def share = new ShareId("test-share")
            def shareData = new ShareData(SharePrice.ZERO, ShareVolume.ZERO)
            def shareChange = new ShareChange(share, shareData, shareData, Instant.now())
            def buyVolume = ShareVolume.of(100)

            def responseWallet = Wallet.of(100)

            def buyingStrategy = Mock(ShareholderActionStrategy) {
                1 * computeAction(shareChange, _, _) >> ShareholderAction.buy(shareChange.id, buyVolume)
            }

            def service = Mock(StockService) {
                1 * requestBuy(id, share, buyVolume) >> StockResponse.success(responseWallet, ShareVolume.ZERO)
            }

            shareholder.onStockServiceAssigned(service)
            shareholder.setStrategy(buyingStrategy)
        when:
            shareholder.onShareChange(shareChange)
        then:
            shareholder.wallet == responseWallet
    }

    void "should sell on share change"() {
        given:
            def share = new ShareId("test-share")
            def shareData = new ShareData(SharePrice.ZERO, ShareVolume.ZERO)
            def shareChange = new ShareChange(share, shareData, shareData, Instant.now())
            def buyVolume = ShareVolume.of(100)

            def responseWallet = Wallet.of(100)

            def sellingStrategy = Mock(ShareholderActionStrategy) {
                1 * computeAction(shareChange, _, _) >> ShareholderAction.sell(shareChange.id, buyVolume)
            }

            def service = Mock(StockService) {
                1 * requestSell(id, share, buyVolume) >> StockResponse.success(responseWallet, ShareVolume.ZERO)
            }

            shareholder.onStockServiceAssigned(service)
            shareholder.setStrategy(sellingStrategy)
        when:
            shareholder.onShareChange(shareChange)
        then:
            shareholder.wallet == responseWallet
    }
}
