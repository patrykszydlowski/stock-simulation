package pl.pszydlowski.stocksim.shareholder

import pl.pszydlowski.stocksim.common.money.Wallet
import pl.pszydlowski.stocksim.share.value.ShareChange
import pl.pszydlowski.stocksim.share.value.ShareId
import pl.pszydlowski.stocksim.share.value.SharePrice
import pl.pszydlowski.stocksim.share.value.ShareVolume
import pl.pszydlowski.stocksim.shareholder.value.ShareholderId
import pl.pszydlowski.stocksim.stock.StockService
import pl.pszydlowski.stocksim.stock.value.StockResponse
import pl.pszydlowski.stocksim.stock.value.StockResponseCode
import spock.lang.Specification
import spock.lang.Subject

class AbstractShareholderSpec extends Specification {

    def id = new ShareholderId("test-shareholder")

    @Subject
    AbstractShareholder shareholder

    def setup() {
        shareholder = new AbstractShareholder(id) {
            @Override
            void onShareChange(ShareChange shareChange) {}

            @Override
            void onVolumeAssigned(ShareId shareId, ShareVolume ownedVolume) {}
        }
    }

    def "should refund money to shareholder when share with volume owned by shareholder was removed"() {
        given:
            def price = 100
            def volume = 10
        when:
            shareholder.onShareRemoved(SharePrice.of(price), ShareVolume.of(volume))
        then:
            shareholder.wallet == Wallet.of(price * volume)
    }

    def "should assign wallet to shareholder"() {
        given:
            def wallet = Wallet.of(1000)
        when:
            shareholder.onWalletAssigned(wallet)
        then:
            shareholder.wallet == wallet
    }

    def "should assign new wallet to shareholder after buy request"() {
        given:
            def share = new ShareId("test-share")
            def volume = ShareVolume.of(100)
            def responseVolume = ShareVolume.of(50)
            def responseWallet = Wallet.of(200)

            def service = Mock(StockService) {
                1 * requestBuy(id, share, volume) >> StockResponse.success(responseWallet, responseVolume)
            }

            shareholder.onStockServiceAssigned(service)
        when:
            def response = shareholder.requestBuy(share, volume)
        then:
            response.code == StockResponseCode.SUCCESS
            response.shareholderWallet == responseWallet
            response.ownedVolume == responseVolume
            shareholder.wallet == responseWallet
    }

    def "should assign new wallet to shareholder after sell request"() {
        given:
            def share = new ShareId("test-share")
            def volume = ShareVolume.of(100)
            def responseVolume = ShareVolume.of(50)
            def responseWallet = Wallet.of(1800)

            def service = Mock(StockService) {
                1 * requestSell(id, share, volume) >> StockResponse.success(responseWallet, ShareVolume.of(50))
            }

            shareholder.onStockServiceAssigned(service)
        when:
            def response = shareholder.requestSell(share, volume)
        then:
            response.code == StockResponseCode.SUCCESS
            response.shareholderWallet == responseWallet
            response.ownedVolume == responseVolume
            shareholder.wallet == responseWallet
    }
}
