package pl.pszydlowski.stocksim.shareholder.strategy

import pl.pszydlowski.stocksim.common.money.Wallet
import pl.pszydlowski.stocksim.share.value.*
import spock.lang.Specification
import spock.lang.Subject

import java.time.Instant

import static pl.pszydlowski.stocksim.shareholder.strategy.ShareholderActionCode.BUY
import static pl.pszydlowski.stocksim.shareholder.strategy.ShareholderActionCode.PASS

class BuyAllAvailableVolumeStrategySpec extends Specification {

    @Subject
    def strategy = BuyAllAvailableVolumeStrategy.instance

    def "should ensure only one instance of strategy"() {
        expect:
            strategy == BuyAllAvailableVolumeStrategy.getInstance()
    }

    def "should always buy maximum available amount of shares"() {
        given:
            def share = new ShareId("test-share")

        when:
            def previousData = new ShareData(SharePrice.of(previousPrice), ShareVolume.ZERO)
            def currentData = new ShareData(SharePrice.of(currentPrice), ShareVolume.of(volume))
            def shareChange = new ShareChange(share, previousData, currentData, Instant.now())

            def wallet = Wallet.of(ownedMoney)
            def action = strategy.computeAction(shareChange, wallet, ShareVolume.ZERO)
        then:
            action.code == expectedCode
            action.shareId == share
            action.volume == ShareVolume.of(expectedBuyVolume)
        where:
            previousPrice | currentPrice | volume | ownedMoney | expectedCode | expectedBuyVolume
            0             | 1            | 1      | 0          | PASS         | 0
            100           | 1            | 1      | 0          | PASS         | 0
            0             | 1            | 1      | 100        | BUY          | 1
            0             | 100          | 1      | 0          | PASS         | 0
            0             | 100          | 1      | 100        | BUY          | 1
            100           | 100          | 1      | 0          | PASS         | 0
            0             | 10           | 1000   | 1000       | BUY          | 100
            0             | 1000         | 100    | 999        | PASS         | 0
            0             | 10           | 100    | 5000       | BUY          | 100
    }
}
