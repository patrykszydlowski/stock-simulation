package pl.pszydlowski.stocksim.shareholder.repository.memory

import pl.pszydlowski.stocksim.common.money.Wallet
import pl.pszydlowski.stocksim.shareholder.value.ShareholderId
import spock.lang.Specification

class InMemoryWalletRepositorySpec extends Specification {

    def "should save wallets to delegate"() {
        given:
            def repo = new InMemoryWalletRepository()
            def shareholder = new ShareholderId("test-shareholder")
            def wallet = Wallet.EMPTY
        when:
            repo.save(shareholder, wallet)
        then:
            repo.delegate.containsKey(shareholder)
            repo.delegate.containsValue(wallet)
            repo.delegate.size() == 1
    }

    def "should get wallet from delegate"() {
        given:
            def repo = new InMemoryWalletRepository()
            def shareholder = new ShareholderId("test-shareholder")
            def wallet = Wallet.EMPTY
            repo.save(shareholder, wallet)
        when:
            def foundWallet = repo.findById(shareholder)
        then:
            foundWallet.isPresent()
            foundWallet.filter { it == wallet }.isPresent()
    }

    def "should return empty optional if wallet not found in delegate"() {
        given:
            def repo = new InMemoryWalletRepository()
            def shareholder = new ShareholderId("test-shareholder")
        when:
            def foundWallet = repo.findById(shareholder)
        then:
            !foundWallet.isPresent()
    }

    def "should remove wallet from delegate"() {
        given:
            def repo = new InMemoryWalletRepository()
            def shareholder = new ShareholderId("test-shareholder")
            def wallet = Wallet.EMPTY
            repo.save(shareholder, wallet)
        when:
            def foundWallet = repo.remove(shareholder)
        then:
            foundWallet.isPresent()
            foundWallet.filter { it == wallet }.isPresent()
            repo.delegate.isEmpty()
    }

    def "should return all wallets from delegate"() {
        given:
            def repo = new InMemoryWalletRepository()
            def shareholder1 = new ShareholderId("test-shareholder-1")
            def shareholder2 = new ShareholderId("test-shareholder-2")
            def wallet1 = Wallet.EMPTY
            def wallet2 = Wallet.EMPTY
            repo.save(shareholder1, wallet1)
            repo.save(shareholder2, wallet2)
        when:
            def foundWallets = repo.findAll()
        then:
            foundWallets.size() == 2
            foundWallets.containsAll([wallet1, wallet2])
    }
}
