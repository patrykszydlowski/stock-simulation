package pl.pszydlowski.stocksim.shareholder

import pl.pszydlowski.stocksim.common.money.Wallet
import pl.pszydlowski.stocksim.share.value.*
import pl.pszydlowski.stocksim.shareholder.value.ShareholderId
import pl.pszydlowski.stocksim.stock.StockService
import pl.pszydlowski.stocksim.stock.value.StockResponse
import pl.pszydlowski.stocksim.stock.value.StockResponseCode
import spock.lang.Specification
import spock.lang.Subject

import java.time.Instant
import java.util.function.BiConsumer
import java.util.function.Consumer

class InteractiveShareholderSpec extends Specification {

    def shareChangeConsumer = Mock(Consumer)
    def walletAssignConsumer = Mock(Consumer)
    def volumeAssignedConsumer = Mock(BiConsumer)

    def id = new ShareholderId("test-shareholder")

    @Subject
    InteractiveShareholder shareholder

    def setup() {
        shareholder = new InteractiveShareholder(id, shareChangeConsumer, walletAssignConsumer, volumeAssignedConsumer)
    }

    def "should call consumer on share change"() {
        given:
            def shareId = new ShareId("test-share")
            def shareData = new ShareData(SharePrice.ZERO, ShareVolume.ZERO)
            def shareChange = new ShareChange(shareId, shareData, shareData, Instant.now())
        when:
            shareholder.onShareChange(shareChange)
        then:
            1 * shareChangeConsumer.accept(shareChange)
    }

    def "should call biconsumer on volume assigned"() {
        given:
            def shareId = new ShareId("test-share")
            def volume = ShareVolume.of(100)
        when:
            shareholder.onVolumeAssigned(shareId, volume)
        then:
            1 * volumeAssignedConsumer.accept(shareId, volume)
    }

    def "should call underlying stock service for buy request"() {
        given:
            def share = new ShareId("test-share")
            def volume = ShareVolume.ZERO

            def responseWallet = Wallet.of(200)

            def service = Mock(StockService) {
                1 * requestBuy(id, share, volume) >> StockResponse.success(responseWallet, ShareVolume.ZERO)
            }

            shareholder.onStockServiceAssigned(service)
        when:
            def response = shareholder.buy(share, volume)
        then:
            response.code == StockResponseCode.SUCCESS
            response.shareholderWallet == responseWallet
    }

    def "should call underlying stock service for sell request"() {
        given:
            def share = new ShareId("test-share")
            def volume = ShareVolume.ZERO

            def responseWallet = Wallet.of(200)

            def service = Mock(StockService) {
                1 * requestSell(id, share, volume) >> StockResponse.success(responseWallet, ShareVolume.ZERO)
            }

            shareholder.onStockServiceAssigned(service)
        when:
            def response = shareholder.sell(share, volume)
        then:
            response.code == StockResponseCode.SUCCESS
            response.shareholderWallet == responseWallet
    }
}
