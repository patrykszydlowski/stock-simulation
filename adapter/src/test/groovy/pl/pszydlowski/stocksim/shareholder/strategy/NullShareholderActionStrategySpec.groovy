package pl.pszydlowski.stocksim.shareholder.strategy

import pl.pszydlowski.stocksim.common.money.Wallet
import pl.pszydlowski.stocksim.share.value.*
import spock.lang.Specification
import spock.lang.Subject

import java.time.Instant

class NullShareholderActionStrategySpec extends Specification {

    @Subject
    def strategy = NullShareholderActionStrategy.instance

    def "should ensure only one instance of strategy"() {
        expect:
            strategy == NullShareholderActionStrategy.getInstance()
    }

    def "should always pass"() {
        given:
            def share = new ShareId("test-share")
        when:
            def previousData = new ShareData(SharePrice.of(previousPrice), ShareVolume.ZERO)
            def currentData = new ShareData(SharePrice.of(currentPrice), ShareVolume.ZERO)
            def shareChange = new ShareChange(share, previousData, currentData, Instant.now())

            def wallet = Wallet.of(ownedMoney)
            def action = strategy.computeAction(shareChange, wallet, ShareVolume.ZERO)
        then:
            action.code == ShareholderActionCode.PASS
            action.shareId == share
            action.volume == ShareVolume.ZERO
        where:
            previousPrice | currentPrice | ownedMoney
            1000          | 100          | 10000
            0             | 12003        | 3213134
            321312.321    | 3213.321     | 10000
            0             | 0            | 0
            100           | 0            | 0
            0             | 0            | 100
            0             | 100          | 0
            0             | 100          | 100
            100           | 100          | 0
            100           | 0            | 100
    }
}
