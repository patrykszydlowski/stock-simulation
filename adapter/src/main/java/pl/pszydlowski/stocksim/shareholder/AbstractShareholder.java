package pl.pszydlowski.stocksim.shareholder;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NonNull;
import pl.pszydlowski.stocksim.common.money.Wallet;
import pl.pszydlowski.stocksim.share.Share;
import pl.pszydlowski.stocksim.share.value.ShareId;
import pl.pszydlowski.stocksim.share.value.SharePrice;
import pl.pszydlowski.stocksim.share.value.ShareVolume;
import pl.pszydlowski.stocksim.shareholder.value.ShareholderId;
import pl.pszydlowski.stocksim.stock.StockObserver;
import pl.pszydlowski.stocksim.stock.StockService;
import pl.pszydlowski.stocksim.stock.value.StockResponse;

import java.math.BigDecimal;

import static pl.pszydlowski.stocksim.stock.value.StockResponseCode.SUCCESS;


abstract class AbstractShareholder implements StockObserver {

    private final ShareholderId id;

    @Getter(AccessLevel.PROTECTED)
    private Wallet wallet;
    private StockService stockService;

    protected AbstractShareholder(@NonNull ShareholderId id) {
        this.id = id;
        this.wallet = Wallet.EMPTY;
    }

    @Override
    public final void onShareRemoved(SharePrice price, ShareVolume ownedVolume) {
        final BigDecimal ownedMoney = Share.getTotalPrice(price, ownedVolume);
        wallet = wallet.add(new Wallet(ownedMoney));
    }

    @Override
    public void onWalletAssigned(@NonNull Wallet wallet) {
        this.wallet = wallet;
    }

    @Override
    public final void onStockServiceAssigned(@NonNull StockService stockService) {
        this.stockService = stockService;
    }

    protected final StockResponse requestBuy(@NonNull ShareId shareId, @NonNull ShareVolume buyVolume) {
        final StockResponse stockResponse = stockService.requestBuy(id, shareId, buyVolume);
        notifyShareholderOnSuccess(shareId, stockResponse);
        return stockResponse;
    }

    protected final StockResponse requestSell(@NonNull ShareId shareId, @NonNull ShareVolume sellVolume) {
        final StockResponse stockResponse = stockService.requestSell(id, shareId, sellVolume);
        notifyShareholderOnSuccess(shareId, stockResponse);
        return stockResponse;
    }

    private void notifyShareholderOnSuccess(ShareId shareId, StockResponse response) {
        if (response.getCode() == SUCCESS) {
            onWalletAssigned(response.getShareholderWallet());
            onVolumeAssigned(shareId, response.getOwnedVolume());
        }
    }
}
