package pl.pszydlowski.stocksim.shareholder.strategy;

import pl.pszydlowski.stocksim.common.money.Wallet;
import pl.pszydlowski.stocksim.share.value.ShareChange;
import pl.pszydlowski.stocksim.share.value.SharePrice;

import java.math.BigInteger;
import java.math.RoundingMode;

final class Strategies {

    private Strategies() {
    }


    static BigInteger getMaxAffordableVolume(SharePrice price, Wallet shareholderWallet) {
        return shareholderWallet.getValue().divide(price.getValue(), RoundingMode.DOWN).toBigInteger();
    }

    static BigInteger getMaxBuyVolume(ShareChange shareChange, Wallet shareholderWallet) {
        final BigInteger maxAffordableVolume = getMaxAffordableVolume(shareChange.getCurrentData().getPrice(), shareholderWallet);
        return maxAffordableVolume.min(shareChange.getCurrentData().getVolume().getValue());
    }
}
