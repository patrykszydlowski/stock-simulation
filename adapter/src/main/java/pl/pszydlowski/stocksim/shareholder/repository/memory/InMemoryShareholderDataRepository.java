package pl.pszydlowski.stocksim.shareholder.repository.memory;

import pl.pszydlowski.stocksim.shareholder.repository.ShareholderDataRepository;
import pl.pszydlowski.stocksim.shareholder.value.ShareholderData;
import pl.pszydlowski.stocksim.shareholder.value.ShareholderId;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

import static java.util.Collections.unmodifiableList;
import static java.util.Optional.ofNullable;

public class InMemoryShareholderDataRepository implements ShareholderDataRepository {

    private final Map<ShareholderId, ShareholderData> delegate;

    public InMemoryShareholderDataRepository() {
        this.delegate = new ConcurrentHashMap<>();
    }

    @Override
    public Optional<ShareholderData> findById(ShareholderId shareholderId) {
        return ofNullable(delegate.get(shareholderId));
    }

    @Override
    public List<ShareholderData> findAll() {
        return unmodifiableList(new ArrayList<>(delegate.values()));
    }

    @Override
    public void save(ShareholderId shareholderId, ShareholderData shareholderData) {
        delegate.put(shareholderId, shareholderData);
    }

    @Override
    public Optional<ShareholderData> remove(ShareholderId shareholderId) {
        return ofNullable(delegate.remove(shareholderId));
    }

}
