package pl.pszydlowski.stocksim.shareholder.repository.json;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.NonNull;
import pl.pszydlowski.stocksim.shareholder.repository.ShareholderDataRepository;
import pl.pszydlowski.stocksim.shareholder.value.ShareholderData;
import pl.pszydlowski.stocksim.shareholder.value.ShareholderId;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static com.fasterxml.jackson.databind.SerializationFeature.INDENT_OUTPUT;

public class JsonShareholderDataRepository implements ShareholderDataRepository {

    private final ObjectMapper mapper;
    private final File databaseLocation;

    public JsonShareholderDataRepository(@NonNull File databaseLocation) {
        this.databaseLocation = databaseLocation;
        this.mapper = new ObjectMapper();
        configureMapper();
        createFileIfNotExists(databaseLocation);
    }

    public JsonShareholderDataRepository(String fileName) {
        this(new File(fileName + ".json"));
    }

    private void configureMapper() {
        mapper.registerModule(new ShareholderModule());
        mapper.configure(INDENT_OUTPUT, true);
    }

    private void createFileIfNotExists(File databaseLocation) {
        if (!databaseLocation.exists()) {
            try {
                Files.write(databaseLocation.toPath(), "{}".getBytes());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public Optional<ShareholderData> findById(ShareholderId shareholderId) {
        return Optional.ofNullable(loadJson().get(shareholderId));
    }

    @Override
    public List<ShareholderData> findAll() {
        return convertToList(loadJson());
    }

    @Override
    public void save(ShareholderId shareholderId, ShareholderData shareholderData) {
        final Map<ShareholderId, ShareholderData> dataMap = loadJson();
        dataMap.put(shareholderId, shareholderData);
        saveToJson(dataMap);
    }

    @Override
    public Optional<ShareholderData> remove(ShareholderId shareholderId) {
        final Map<ShareholderId, ShareholderData> dataMap = loadJson();
        final ShareholderData data = dataMap.remove(shareholderId);
        saveToJson(dataMap);
        return Optional.ofNullable(data);
    }

    private List<ShareholderData> convertToList(Map<ShareholderId, ShareholderData> data) {
        return new ArrayList<>(data.values());
    }


    private Map<ShareholderId, ShareholderData> loadJson() {
        try {
            return mapper.readValue(databaseLocation, new TypeReference<Map<ShareholderId, ShareholderData>>() {});
        } catch (IOException e) {
            e.printStackTrace();
            return new HashMap<>();
        }
    }

    private void saveToJson(Map<ShareholderId, ShareholderData> wallets) {
        try {
            mapper.writeValue(databaseLocation, wallets);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
