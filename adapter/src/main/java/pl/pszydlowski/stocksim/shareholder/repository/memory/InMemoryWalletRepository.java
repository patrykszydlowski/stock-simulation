package pl.pszydlowski.stocksim.shareholder.repository.memory;

import pl.pszydlowski.stocksim.common.money.Wallet;
import pl.pszydlowski.stocksim.shareholder.repository.WalletRepository;
import pl.pszydlowski.stocksim.shareholder.value.ShareholderId;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

import static java.util.Collections.unmodifiableList;
import static java.util.Optional.ofNullable;

public final class InMemoryWalletRepository implements WalletRepository {

    private final Map<ShareholderId, Wallet> delegate;

    public InMemoryWalletRepository() {
        this.delegate = new ConcurrentHashMap<>();
    }

    @Override
    public Optional<Wallet> findById(ShareholderId shareholderId) {
        return ofNullable(delegate.get(shareholderId));
    }

    @Override
    public List<Wallet> findAll() {
        return unmodifiableList(new ArrayList<>(delegate.values()));
    }

    @Override
    public void save(ShareholderId id, Wallet wallet) {
        delegate.put(id, wallet);
    }

    @Override
    public Optional<Wallet> remove(ShareholderId shareholderId) {
        return ofNullable(delegate.remove(shareholderId));
    }
}
