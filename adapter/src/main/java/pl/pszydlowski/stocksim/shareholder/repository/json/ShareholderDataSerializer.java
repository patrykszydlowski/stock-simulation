package pl.pszydlowski.stocksim.shareholder.repository.json;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import pl.pszydlowski.stocksim.shareholder.value.ShareholderData;

import java.io.IOException;

final class ShareholderDataSerializer extends StdSerializer<ShareholderData> {

    ShareholderDataSerializer(Class<ShareholderData> t) {
        super(t);
    }

    @Override
    public void serialize(ShareholderData data, JsonGenerator gen, SerializerProvider provider) throws IOException {
        gen.writeStartObject();
        gen.writeStringField("id", data.getId().getValue());
        gen.writeNumberField("wallet", data.getWallet().getValue());
        gen.writeEndObject();
    }
}
