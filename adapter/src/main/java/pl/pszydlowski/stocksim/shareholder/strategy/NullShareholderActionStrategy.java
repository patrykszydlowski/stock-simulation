package pl.pszydlowski.stocksim.shareholder.strategy;

import pl.pszydlowski.stocksim.common.money.Wallet;
import pl.pszydlowski.stocksim.share.value.ShareChange;
import pl.pszydlowski.stocksim.share.value.ShareVolume;

import static pl.pszydlowski.stocksim.shareholder.strategy.ShareholderAction.pass;

public final class NullShareholderActionStrategy implements ShareholderActionStrategy {

    private static final NullShareholderActionStrategy INSTANCE = new NullShareholderActionStrategy();

    private NullShareholderActionStrategy() {
    }

    public static NullShareholderActionStrategy getInstance() {
        return INSTANCE;
    }


    @Override
    public ShareholderAction computeAction(ShareChange shareChange, Wallet shareholderWallet, ShareVolume ownedVolume) {
        return pass(shareChange.getId());
    }
}
