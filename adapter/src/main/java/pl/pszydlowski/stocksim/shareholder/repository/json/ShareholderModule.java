package pl.pszydlowski.stocksim.shareholder.repository.json;

import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.Module;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.deser.Deserializers;
import com.fasterxml.jackson.databind.module.SimpleDeserializers;
import com.fasterxml.jackson.databind.module.SimpleSerializers;
import com.fasterxml.jackson.databind.ser.Serializers;
import pl.pszydlowski.stocksim.shareholder.value.ShareholderData;

final class ShareholderModule extends Module {

    private final ObjectMapper mapper;

    ShareholderModule() {
        mapper = new ObjectMapper();
        mapper.registerModule(new ShareholderValueObjectModule());
    }

    @Override
    public String getModuleName() {
        return "Shareholder-module";
    }

    @Override
    public Version version() {
        return Version.unknownVersion();
    }

    @Override
    public void setupModule(SetupContext context) {
        context.addSerializers(createSerializers());
        context.addDeserializers(createDeserializers());
    }

    private Serializers createSerializers() {
        SimpleSerializers serializers = new SimpleSerializers();
        serializers.addSerializer(dataSerializer());
        return serializers;
    }

    private Deserializers createDeserializers() {
        SimpleDeserializers deserializers = new SimpleDeserializers();
        deserializers.addDeserializer(ShareholderData.class, dataDeserializer());
        return deserializers;
    }

    private ShareholderDataSerializer dataSerializer() {
        return new ShareholderDataSerializer(ShareholderData.class);
    }

    private ShareholderDataDeserializer dataDeserializer() {
        return new ShareholderDataDeserializer(ShareholderData.class, mapper);
    }

}
