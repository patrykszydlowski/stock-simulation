package pl.pszydlowski.stocksim.shareholder.repository.json;

import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.Module;
import com.fasterxml.jackson.databind.deser.Deserializers;
import com.fasterxml.jackson.databind.module.SimpleDeserializers;
import com.fasterxml.jackson.databind.module.SimpleSerializers;
import com.fasterxml.jackson.databind.ser.Serializers;
import pl.pszydlowski.stocksim.common.json.ValueObjectDeserializer;
import pl.pszydlowski.stocksim.common.json.ValueObjectSerializer;
import pl.pszydlowski.stocksim.common.money.Wallet;
import pl.pszydlowski.stocksim.shareholder.value.ShareholderId;

import java.math.BigDecimal;

final class ShareholderValueObjectModule extends Module {

    @Override
    public String getModuleName() {
        return "Shareholder-value-object-module";
    }

    @Override
    public Version version() {
        return Version.unknownVersion();
    }

    @Override
    public void setupModule(SetupContext context) {
        context.addSerializers(createSerializers());
        context.addDeserializers(createDeserializers());
    }

    private Serializers createSerializers() {
        SimpleSerializers serializers = new SimpleSerializers();
        serializers.addSerializer(walletSerializer());
        serializers.addSerializer(shareholderIdSerializer());
        return serializers;
    }

    private Deserializers createDeserializers() {
        SimpleDeserializers deserializers = new SimpleDeserializers();
        deserializers.addDeserializer(Wallet.class, walletDeserializer());
        deserializers.addDeserializer(ShareholderId.class, shareholderIdDeserializer());
        return deserializers;
    }

    private ValueObjectSerializer<Wallet> walletSerializer() {
        return new ValueObjectSerializer<>(Wallet.class, wallet -> wallet.getValue().toString());
    }

    private ValueObjectDeserializer<Wallet> walletDeserializer() {
        return new ValueObjectDeserializer<>(Wallet.class, price -> new Wallet(new BigDecimal(price)));
    }

    private ValueObjectSerializer<ShareholderId> shareholderIdSerializer() {
        return new ValueObjectSerializer<>(ShareholderId.class, ShareholderId::getValue);
    }

    private ValueObjectDeserializer<ShareholderId> shareholderIdDeserializer() {
        return new ValueObjectDeserializer<>(ShareholderId.class, ShareholderId::new);
    }
}
