package pl.pszydlowski.stocksim.shareholder.repository.json;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import pl.pszydlowski.stocksim.common.money.Wallet;
import pl.pszydlowski.stocksim.shareholder.value.ShareholderData;
import pl.pszydlowski.stocksim.shareholder.value.ShareholderId;

import java.io.IOException;
import java.util.Optional;

import static java.lang.String.format;

final class ShareholderDataDeserializer extends StdDeserializer<ShareholderData> {

    private final ObjectMapper mapper;

    ShareholderDataDeserializer(Class<?> vc, ObjectMapper mapper) {
        super(vc);
        this.mapper = mapper;
    }

    @Override
    public ShareholderData deserialize(JsonParser parser, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        JsonNode rootNode = parser.getCodec().readTree(parser);

        ShareholderId shareholderId = getNode(rootNode, "id")
                .map(node -> mapper.convertValue(node, ShareholderId.class))
                .orElseThrow(() -> missingProperty(parser, "id"));

        Wallet wallet = getNode(rootNode, "wallet")
                .map(node -> mapper.convertValue(node, Wallet.class))
                .orElseThrow(() -> missingProperty(parser, "wallet"));

        return new ShareholderData(shareholderId, wallet);
    }

    private Optional<JsonNode> getNode(JsonNode node, String name) {
        return Optional.ofNullable(node.get(name));
    }

    private JsonMappingException missingProperty(JsonParser p, String propertyName) {
        return new JsonMappingException(p, format("Missing property: [%s]", propertyName));
    }
}
