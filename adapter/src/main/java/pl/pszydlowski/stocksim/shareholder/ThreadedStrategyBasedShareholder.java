package pl.pszydlowski.stocksim.shareholder;

import pl.pszydlowski.stocksim.share.value.ShareChange;
import pl.pszydlowski.stocksim.shareholder.strategy.ShareholderActionStrategy;
import pl.pszydlowski.stocksim.shareholder.value.ShareholderId;
import pl.pszydlowski.stocksim.stock.value.UpdateInterval;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ScheduledExecutorService;

public class ThreadedStrategyBasedShareholder extends StrategyBasedShareholder {

    private final List<ShareChange> currentSessionChanges;

    public ThreadedStrategyBasedShareholder(ShareholderId id, ShareholderActionStrategy strategy, ScheduledExecutorService executorService, UpdateInterval interval) {
        super(id, strategy);
        this.currentSessionChanges = new LinkedList<>();
        executorService.scheduleAtFixedRate(this::processSession, 0, interval.getInterval(), interval.getTimeUnit());
    }

    public ThreadedStrategyBasedShareholder(ShareholderId id, ScheduledExecutorService executorService, UpdateInterval interval) {
        super(id);
        this.currentSessionChanges = new LinkedList<>();
        executorService.scheduleAtFixedRate(this::processSession, 0, interval.getInterval(), interval.getTimeUnit());
    }

    @Override
    public final void onShareChange(ShareChange shareChange) {
        currentSessionChanges.add(shareChange);
    }

    private void processSession() {
        List<ShareChange> shareChangesCopy;
        synchronized (currentSessionChanges) {
            shareChangesCopy = new ArrayList<>(currentSessionChanges);
            currentSessionChanges.clear();
        }
        shareChangesCopy.forEach(this::processShareChange);
    }
}
