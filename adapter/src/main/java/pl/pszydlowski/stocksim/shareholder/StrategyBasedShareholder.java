package pl.pszydlowski.stocksim.shareholder;

import lombok.NonNull;
import pl.pszydlowski.stocksim.share.value.ShareChange;
import pl.pszydlowski.stocksim.share.value.ShareId;
import pl.pszydlowski.stocksim.share.value.ShareVolume;
import pl.pszydlowski.stocksim.shareholder.strategy.NullShareholderActionStrategy;
import pl.pszydlowski.stocksim.shareholder.strategy.ShareholderAction;
import pl.pszydlowski.stocksim.shareholder.strategy.ShareholderActionStrategy;
import pl.pszydlowski.stocksim.shareholder.value.ShareholderId;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class StrategyBasedShareholder extends AbstractShareholder {

    private final Map<ShareId, ShareVolume> ownedVolumes;
    private ShareholderActionStrategy strategy;

    public StrategyBasedShareholder(@NonNull ShareholderId id, @NonNull ShareholderActionStrategy strategy) {
        super(id);
        this.ownedVolumes = new ConcurrentHashMap<>();
        this.strategy = strategy;
    }

    public StrategyBasedShareholder(ShareholderId id) {
        this(id, NullShareholderActionStrategy.getInstance());
    }

    public void setStrategy(@NonNull ShareholderActionStrategy strategy) {
        this.strategy = strategy;
    }

    @Override
    public void onShareChange(ShareChange shareChange) {
        processShareChange(shareChange);
    }

    protected final void processShareChange(ShareChange shareChange) {
        final ShareholderAction action = strategy.computeAction(
                shareChange,
                getWallet(),
                ownedVolumes.getOrDefault(shareChange.getId(), ShareVolume.ZERO)
        );
        switch (action.getCode()) {
            case BUY:
                requestBuy(shareChange.getId(), action.getVolume());
                break;
            case SELL:
                requestSell(shareChange.getId(), action.getVolume());
                break;
        }
    }

    @Override
    public void onVolumeAssigned(ShareId shareId, ShareVolume ownedVolume) {
        ownedVolumes.put(shareId, ownedVolume);
    }
}
