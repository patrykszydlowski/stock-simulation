package pl.pszydlowski.stocksim.shareholder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.pszydlowski.stocksim.common.money.Wallet;
import pl.pszydlowski.stocksim.share.value.ShareChange;
import pl.pszydlowski.stocksim.share.value.ShareId;
import pl.pszydlowski.stocksim.share.value.SharePrice;
import pl.pszydlowski.stocksim.share.value.ShareVolume;
import pl.pszydlowski.stocksim.shareholder.value.ShareholderId;
import pl.pszydlowski.stocksim.stock.StockObserver;
import pl.pszydlowski.stocksim.stock.StockService;

public final class StockLogger implements StockObserver {

    public static final ShareholderId LOGGER_ID = new ShareholderId("STOCK-LOGGER");

    private static final Logger LOGGER = LoggerFactory.getLogger(StockLogger.class);

    @Override
    public void onShareChange(ShareChange shareChange) {
        LOGGER.info(
                "Share [{}] has price set to [{}] at [{}]",
                shareChange.getId(),
                shareChange.getCurrentData().getPrice(),
                shareChange.getTimestamp()
        );
        LOGGER.info(
                "Share [{}] has volume set to [{}] at [{}]",
                shareChange.getId(),
                shareChange.getCurrentData().getVolume(),
                shareChange.getTimestamp()
        );
    }

    @Override
    public void onShareRemoved(SharePrice price, ShareVolume ownedVolume) {
        //empty
    }

    @Override
    public void onWalletAssigned(Wallet wallet) {
        //empty
    }

    @Override
    public void onVolumeAssigned(ShareId shareId, ShareVolume ownedVolume) {
        //empty
    }

    @Override
    public void onStockServiceAssigned(StockService stockService) {
        //empty
    }
}
