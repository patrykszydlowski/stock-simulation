package pl.pszydlowski.stocksim.shareholder;

import lombok.NonNull;
import pl.pszydlowski.stocksim.common.money.Wallet;
import pl.pszydlowski.stocksim.share.value.ShareChange;
import pl.pszydlowski.stocksim.share.value.ShareId;
import pl.pszydlowski.stocksim.share.value.ShareVolume;
import pl.pszydlowski.stocksim.shareholder.value.ShareholderId;
import pl.pszydlowski.stocksim.stock.value.StockResponse;

import java.util.function.BiConsumer;
import java.util.function.Consumer;

public final class InteractiveShareholder extends AbstractShareholder {

    private final Consumer<ShareChange> shareChangeConsumer;
    private final Consumer<Wallet> walletAssignConsumer;
    private final BiConsumer<ShareId, ShareVolume> volumeAssignedConsumer;

    public InteractiveShareholder(@NonNull ShareholderId id, @NonNull Consumer<ShareChange> shareChangeConsumer,
                                  @NonNull Consumer<Wallet> walletAssignConsumer, @NonNull BiConsumer<ShareId, ShareVolume> volumeAssignedConsumer) {
        super(id);
        this.shareChangeConsumer = shareChangeConsumer;
        this.walletAssignConsumer = walletAssignConsumer;
        this.volumeAssignedConsumer = volumeAssignedConsumer;
    }

    @Override
    public void onShareChange(@NonNull ShareChange shareChange) {
        shareChangeConsumer.accept(shareChange);
    }

    @Override
    public void onVolumeAssigned(ShareId shareId, ShareVolume ownedVolume) {
        volumeAssignedConsumer.accept(shareId, ownedVolume);
    }

    @Override
    public void onWalletAssigned(Wallet wallet) {
        super.onWalletAssigned(wallet);
        walletAssignConsumer.accept(wallet);
    }

    public StockResponse buy(ShareId shareId, ShareVolume buyVolume) {
        return requestBuy(shareId, buyVolume);
    }

    public StockResponse sell(ShareId shareId, ShareVolume sellVolume) {
        return requestSell(shareId, sellVolume);
    }
}
