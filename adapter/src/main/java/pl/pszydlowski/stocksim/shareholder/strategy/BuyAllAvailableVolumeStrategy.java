package pl.pszydlowski.stocksim.shareholder.strategy;

import pl.pszydlowski.stocksim.common.money.Wallet;
import pl.pszydlowski.stocksim.share.value.ShareChange;
import pl.pszydlowski.stocksim.share.value.ShareId;
import pl.pszydlowski.stocksim.share.value.ShareVolume;

import java.math.BigInteger;

import static pl.pszydlowski.stocksim.shareholder.strategy.ShareholderAction.buy;
import static pl.pszydlowski.stocksim.shareholder.strategy.ShareholderAction.pass;
import static pl.pszydlowski.stocksim.shareholder.strategy.Strategies.getMaxBuyVolume;

public final class BuyAllAvailableVolumeStrategy implements ShareholderActionStrategy {

    private static final BuyAllAvailableVolumeStrategy INSTANCE = new BuyAllAvailableVolumeStrategy();

    private BuyAllAvailableVolumeStrategy() {
    }

    public static BuyAllAvailableVolumeStrategy getInstance() {
        return INSTANCE;
    }

    @Override
    public ShareholderAction computeAction(ShareChange shareChange, Wallet shareholderWallet, ShareVolume ownedVolume) {
        final ShareId shareId = shareChange.getId();
        final BigInteger maxBuyVolume = getMaxBuyVolume(shareChange, shareholderWallet);

        if (maxBuyVolume.compareTo(BigInteger.ZERO) > 0) {
            return buy(shareId, new ShareVolume(maxBuyVolume));
        } else {
            return pass(shareId);
        }
    }

}
