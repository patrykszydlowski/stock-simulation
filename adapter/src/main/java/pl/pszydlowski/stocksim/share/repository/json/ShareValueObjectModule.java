package pl.pszydlowski.stocksim.share.repository.json;

import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.Module;
import com.fasterxml.jackson.databind.deser.Deserializers;
import com.fasterxml.jackson.databind.module.SimpleDeserializers;
import com.fasterxml.jackson.databind.module.SimpleSerializers;
import com.fasterxml.jackson.databind.ser.Serializers;
import pl.pszydlowski.stocksim.common.json.ValueObjectDeserializer;
import pl.pszydlowski.stocksim.common.json.ValueObjectSerializer;
import pl.pszydlowski.stocksim.share.value.ShareId;
import pl.pszydlowski.stocksim.share.value.SharePrice;
import pl.pszydlowski.stocksim.share.value.ShareVolume;
import pl.pszydlowski.stocksim.shareholder.value.ShareholderId;

import java.math.BigDecimal;
import java.math.BigInteger;

final class ShareValueObjectModule extends Module {

    @Override
    public String getModuleName() {
        return "Share-value-object-module";
    }

    @Override
    public Version version() {
        return Version.unknownVersion();
    }

    @Override
    public void setupModule(SetupContext context) {
        context.addSerializers(createSerializers());
        context.addDeserializers(createDeserializers());
    }

    private Serializers createSerializers() {
        SimpleSerializers serializers = new SimpleSerializers();
        serializers.addSerializer(shareIdSerializer());
        serializers.addSerializer(shareholderIdSerializer());
        serializers.addSerializer(sharePriceSerializer());
        serializers.addSerializer(shareVolumeSerializer());
        return serializers;
    }

    private Deserializers createDeserializers() {
        SimpleDeserializers deserializers = new SimpleDeserializers();
        deserializers.addDeserializer(ShareId.class, shareIdDeserializer());
        deserializers.addDeserializer(ShareholderId.class, shareholderIdDeserializer());
        deserializers.addDeserializer(SharePrice.class, sharePriceDeserializer());
        deserializers.addDeserializer(ShareVolume.class, shareVolumeDeserializer());
        return deserializers;
    }

    private ValueObjectSerializer<ShareId> shareIdSerializer() {
        return new ValueObjectSerializer<>(ShareId.class, id -> id.getValue().toUpperCase());
    }

    private ValueObjectDeserializer<ShareId> shareIdDeserializer() {
        return new ValueObjectDeserializer<>(ShareId.class, ShareId::new);
    }

    private ValueObjectSerializer<ShareholderId> shareholderIdSerializer() {
        return new ValueObjectSerializer<>(ShareholderId.class, ShareholderId::getValue);
    }

    private ValueObjectDeserializer<ShareholderId> shareholderIdDeserializer() {
        return new ValueObjectDeserializer<>(ShareholderId.class, ShareholderId::new);
    }

    private ValueObjectSerializer<SharePrice> sharePriceSerializer() {
        return new ValueObjectSerializer<>(SharePrice.class, price -> price.getValue().toPlainString());
    }

    private ValueObjectDeserializer<SharePrice> sharePriceDeserializer() {
        return new ValueObjectDeserializer<>(SharePrice.class, value -> new SharePrice(new BigDecimal(value)));
    }

    private ValueObjectSerializer<ShareVolume> shareVolumeSerializer() {
        return new ValueObjectSerializer<>(ShareVolume.class, price -> price.getValue().toString());
    }

    private ValueObjectDeserializer<ShareVolume> shareVolumeDeserializer() {
        return new ValueObjectDeserializer<>(ShareVolume.class, value -> new ShareVolume(new BigInteger(value)));
    }
}
