package pl.pszydlowski.stocksim.share.repository.memory;

import pl.pszydlowski.stocksim.share.Share;
import pl.pszydlowski.stocksim.share.repository.ShareRepository;
import pl.pszydlowski.stocksim.share.value.ShareId;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

import static java.util.Collections.unmodifiableList;
import static java.util.Optional.ofNullable;

public final class InMemoryShareRepository implements ShareRepository {

    private final Map<ShareId, Share> delegate;

    public InMemoryShareRepository() {
        this.delegate = new ConcurrentHashMap<>();
    }

    @Override
    public Optional<Share> findById(ShareId shareId) {
        return ofNullable(delegate.get(shareId));
    }

    @Override
    public List<Share> findAll() {
        return unmodifiableList(new ArrayList<>(delegate.values()));
    }

    @Override
    public void save(ShareId shareId, Share share) {
        delegate.put(shareId, share);
    }

    @Override
    public Optional<Share> remove(ShareId shareId) {
        return ofNullable(delegate.remove(shareId));
    }
}
