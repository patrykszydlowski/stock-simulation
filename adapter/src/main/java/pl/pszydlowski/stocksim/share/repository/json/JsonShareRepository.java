package pl.pszydlowski.stocksim.share.repository.json;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import pl.pszydlowski.stocksim.share.Share;
import pl.pszydlowski.stocksim.share.repository.ShareRepository;
import pl.pszydlowski.stocksim.share.value.ShareId;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static com.fasterxml.jackson.databind.SerializationFeature.INDENT_OUTPUT;
import static java.util.Optional.ofNullable;

public final class JsonShareRepository implements ShareRepository {

    private final ObjectMapper mapper;
    private final File databaseLocation;

    public JsonShareRepository(File databaseLocation) {
        this.databaseLocation = databaseLocation;
        this.mapper = new ObjectMapper();
        configureMapper();
        createFileIfNotExists(databaseLocation);
    }

    public JsonShareRepository(String fileName) {
        this(new File(fileName + ".json"));
    }

    private void configureMapper() {
        mapper.registerModule(new ShareValueObjectModule());
        mapper.registerModule(new ShareModule());
        mapper.configure(INDENT_OUTPUT, true);
    }

    private void createFileIfNotExists(File databaseLocation) {
        if (!databaseLocation.exists()) {
            try {
                Files.write(databaseLocation.toPath(), "{}".getBytes());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public Optional<Share> findById(ShareId shareId) {
        final Map<ShareId, Share> shares = loadJson();
        return ofNullable(shares.get(shareId));
    }

    @Override
    public List<Share> findAll() {
        return convertToList(loadJson());
    }

    @Override
    public void save(ShareId shareId, Share share) {
        final Map<ShareId, Share> shares = loadJson();
        shares.put(shareId, share);
        saveToJson(shares);
    }

    @Override
    public Optional<Share> remove(ShareId shareId) {
        final Map<ShareId, Share> shares = loadJson();
        final Share share = shares.remove(shareId);
        saveToJson(shares);
        return ofNullable(share);
    }

    private List<Share> convertToList(Map<ShareId, Share> shares) {
        return new ArrayList<>(shares.values());
    }

    private Map<ShareId, Share> loadJson() {
        try {
            return mapper.readValue(databaseLocation, new TypeReference<Map<ShareId, Share>>() {});
        } catch (IOException e) {
            e.printStackTrace();
            return new HashMap<>();
        }
    }

    private void saveToJson(Map<ShareId, Share> shares) {
        try {
            mapper.writeValue(databaseLocation, shares);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
