package pl.pszydlowski.stocksim.share.repository.json;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import pl.pszydlowski.stocksim.share.Share;

import java.io.IOException;
import java.math.BigDecimal;

final class ShareSerializer extends StdSerializer<Share> {

    ShareSerializer(Class<Share> t) {
        super(t);
    }

    @Override
    public void serialize(Share share, JsonGenerator gen, SerializerProvider provider) throws IOException {
        gen.writeStartObject();
        gen.writeStringField("id", share.getId().getValue());
        gen.writeNumberField("price", share.getCurrentPrice().getValue());
        gen.writeNumberField("available-volume", new BigDecimal(share.getAvailableVolume().getValue()));
        gen.writeObjectField("owned-volume", share.getOwnedVolumes());
        gen.writeEndObject();
    }
}
