package pl.pszydlowski.stocksim.share.repository.json;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import pl.pszydlowski.stocksim.share.Share;
import pl.pszydlowski.stocksim.share.value.ShareId;
import pl.pszydlowski.stocksim.share.value.SharePrice;
import pl.pszydlowski.stocksim.share.value.ShareVolume;
import pl.pszydlowski.stocksim.shareholder.value.ShareholderId;

import java.io.IOException;
import java.util.Map;
import java.util.Optional;

import static java.lang.String.format;

final class ShareDeserializer extends StdDeserializer<Share> {

    private final ObjectMapper mapper;

    ShareDeserializer(Class<?> vc, ObjectMapper mapper) {
        super(vc);
        this.mapper = mapper;
    }

    @Override
    public Share deserialize(JsonParser parser, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        JsonNode rootNode = parser.getCodec().readTree(parser);

        ShareId shareId = getNode(rootNode, "id")
                .map(node -> mapper.convertValue(node, ShareId.class))
                .orElseThrow(() -> missingProperty(parser, "id"));

        SharePrice sharePrice = getNode(rootNode, "price")
                .map(node -> mapper.convertValue(node, SharePrice.class))
                .orElseThrow(() -> missingProperty(parser, "current price"));

        ShareVolume shareVolume = getNode(rootNode, "available-volume")
                .map(node -> mapper.convertValue(node, ShareVolume.class))
                .orElseThrow(() -> missingProperty(parser, "available volume"));

        Map<ShareholderId, ShareVolume> ownedVolume = getNode(rootNode, "owned-volume")
                .map(node -> mapper.<Map<ShareholderId, ShareVolume>>convertValue(node, new TypeReference<Map<ShareholderId, ShareVolume>>() {
                }))
                .orElseThrow(() -> missingProperty(parser, "owned volume"));

        return new Share(shareId, ownedVolume, sharePrice, shareVolume);
    }

    private Optional<JsonNode> getNode(JsonNode node, String name) {
        return Optional.ofNullable(node.get(name));
    }

    private JsonMappingException missingProperty(JsonParser p, String propertyName) {
        return new JsonMappingException(p, format("Missing property: [%s]", propertyName));
    }
}
