package pl.pszydlowski.stocksim.share.repository.json;

import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.Module;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.deser.Deserializers;
import com.fasterxml.jackson.databind.module.SimpleDeserializers;
import com.fasterxml.jackson.databind.module.SimpleSerializers;
import com.fasterxml.jackson.databind.ser.Serializers;
import pl.pszydlowski.stocksim.share.Share;

final class ShareModule extends Module {

    private final ObjectMapper mapper;

    ShareModule() {
        mapper = new ObjectMapper();
        mapper.registerModule(new ShareValueObjectModule());
    }

    @Override
    public String getModuleName() {
        return "Share-module";
    }

    @Override
    public Version version() {
        return Version.unknownVersion();
    }

    @Override
    public void setupModule(SetupContext context) {
        context.addSerializers(createSerializers());
        context.addDeserializers(createDeserializers());
    }

    private Serializers createSerializers() {
        SimpleSerializers serializers = new SimpleSerializers();
        serializers.addSerializer(shareSerializer());
        return serializers;
    }

    private Deserializers createDeserializers() {
        SimpleDeserializers deserializers = new SimpleDeserializers();
        deserializers.addDeserializer(Share.class, shareDeserializer());
        return deserializers;
    }

    private ShareSerializer shareSerializer() {
        return new ShareSerializer(Share.class);
    }

    private ShareDeserializer shareDeserializer() {
        return new ShareDeserializer(Share.class, mapper);
    }
}
