package pl.pszydlowski.stocksim.common.repository;

import lombok.NonNull;

import java.util.List;
import java.util.Optional;

public final class Repositories {

    private Repositories() {
    }

    public static <T, ID> Repository<T, ID> unmodifiable(Repository<T, ID> repository) {
        return new UnmodifiableRepository<>(repository);
    }

    private static final class UnmodifiableRepository<T, ID> implements Repository<T, ID> {

        private final Repository<T, ID> delegate;

        private UnmodifiableRepository(@NonNull Repository<T, ID> delegate) {
            this.delegate = delegate;
        }

        @Override
        public Optional<T> findById(@NonNull ID id) {
            return delegate.findById(id);
        }

        @Override
        public List<T> findAll() {
            return delegate.findAll();
        }

        @Override
        public void save(ID id, T t) {
            throw new UnsupportedOperationException();
        }

        @Override
        public Optional<T> remove(ID id) {
            throw new UnsupportedOperationException();
        }
    }
}
