package pl.pszydlowski.stocksim.common.json;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import java.io.IOException;
import java.util.function.Function;

public final class ValueObjectDeserializer<T> extends StdDeserializer<T> {

    private final Function<String, T> constructor;

    public ValueObjectDeserializer(Class vc, Function<String, T> constructor) {
        super(vc);
        this.constructor = constructor;
    }

    @Override
    public T deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        JsonNode node = p.getCodec().readTree(p);
        String value = node.asText();
        return constructor.apply(value);
    }
}
