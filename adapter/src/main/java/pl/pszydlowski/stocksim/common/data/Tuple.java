package pl.pszydlowski.stocksim.common.data;

import lombok.NonNull;
import lombok.Value;

@Value
public final class Tuple<A, B> {
    @NonNull
    A left;
    @NonNull
    B right;
}
