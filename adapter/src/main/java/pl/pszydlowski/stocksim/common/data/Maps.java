package pl.pszydlowski.stocksim.common.data;

import java.util.AbstractMap;
import java.util.Map;

import static java.util.Arrays.stream;
import static java.util.Collections.unmodifiableMap;
import static java.util.stream.Collectors.toMap;

public final class Maps {

    private Maps() {

    }

    @SafeVarargs
    public static <K, V> Map<K, V> map(Map.Entry<K, V>... entries) {
        return unmodifiableMap(stream(entries).collect(toMap(Map.Entry::getKey, Map.Entry::getValue)));
    }

    public static <K, V> Map.Entry<K, V> entry(K key, V value) {
        return new AbstractMap.SimpleEntry<>(key, value);
    }
}
