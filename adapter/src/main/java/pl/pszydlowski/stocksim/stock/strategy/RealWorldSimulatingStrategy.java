package pl.pszydlowski.stocksim.stock.strategy;

import pl.pszydlowski.stocksim.share.value.ShareData;
import pl.pszydlowski.stocksim.share.value.ShareId;
import pl.pszydlowski.stocksim.share.value.SharePrice;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RealWorldSimulatingStrategy implements StockUpdateStrategy {

    private final Map<ShareId, List<ShareData>> shareChanges;

    public RealWorldSimulatingStrategy() {
        this.shareChanges = new HashMap<>();
    }

    @Override
    public SharePrice computeNewPrice(ShareId shareId, ShareData currentData) {
        final List<ShareData> history = shareChanges.computeIfAbsent(shareId, key -> new ArrayList<>());
        history.add(currentData);

        return null;
    }
}
