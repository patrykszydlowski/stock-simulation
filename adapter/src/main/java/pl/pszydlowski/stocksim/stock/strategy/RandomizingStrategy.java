package pl.pszydlowski.stocksim.stock.strategy;

import pl.pszydlowski.stocksim.share.value.ShareData;
import pl.pszydlowski.stocksim.share.value.ShareId;
import pl.pszydlowski.stocksim.share.value.SharePrice;

import java.math.BigDecimal;
import java.util.Random;

import static java.math.BigDecimal.valueOf;

public final class RandomizingStrategy implements StockUpdateStrategy {

    private final double randomizationRatio;
    private final Random random;

    public RandomizingStrategy(double randomizationRatio) {
        this.randomizationRatio = randomizationRatio;
        this.random = new Random();
    }

    @Override
    public SharePrice computeNewPrice(ShareId shareId, ShareData currentData) {
        final BigDecimal randomMultiplier = randomMultiplier();
        final BigDecimal nextPrice = currentData.getPrice().getValue().multiply(randomMultiplier);
        return new SharePrice(nextPrice);
    }

    private BigDecimal randomMultiplier() {
        return valueOf(random.nextDouble()).subtract(valueOf(0.5)).multiply(valueOf(randomizationRatio)).add(valueOf(1));
    }
}
