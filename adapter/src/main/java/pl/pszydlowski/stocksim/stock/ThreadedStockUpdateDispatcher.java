package pl.pszydlowski.stocksim.stock;

import lombok.NonNull;
import pl.pszydlowski.stocksim.share.value.ShareChange;
import pl.pszydlowski.stocksim.share.value.ShareData;
import pl.pszydlowski.stocksim.share.value.ShareId;
import pl.pszydlowski.stocksim.stock.value.UpdateInterval;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.function.Supplier;

final class ThreadedStockUpdateDispatcher implements StockUpdateDispatcher {

    private final Supplier<ScheduledExecutorService> serviceSupplier;

    private final Collection<StockObserver> observers;
    private final Map<ShareId, List<ShareData>> cachedShareChanges;
    private final StockUpdateApplier updateApplier;

    private final UpdateInterval updateInterval;

    private volatile ScheduledExecutorService currentService;
    private volatile boolean running;
    private volatile ScheduledFuture<?> dispatchingFunction;

    ThreadedStockUpdateDispatcher(@NonNull Supplier<ScheduledExecutorService> serviceSupplier, @NonNull Collection<StockObserver> observers,
                                  @NonNull StockUpdateApplier updateApplier, @NonNull UpdateInterval updateInterval) {
        this.serviceSupplier = serviceSupplier;
        this.observers = observers;
        this.updateApplier = updateApplier;
        this.updateInterval = updateInterval;
        this.running = false;
        this.cachedShareChanges = new ConcurrentHashMap<>();
    }

    @Override
    public void start() {
        if (!running) {
            currentService = serviceSupplier.get();
            dispatchingFunction = currentService.scheduleAtFixedRate(this::publishChanges, 0, updateInterval.getInterval(), updateInterval.getTimeUnit());
            running = true;
        }
    }

    @Override
    public void addShareTransaction(ShareId shareId, ShareData shareData) {
        cachedShareChanges.computeIfAbsent(shareId, key -> new ArrayList<>()).add(shareData);
    }

    @Override
    public void stop() {
        if (running) {
            boolean cancelled = dispatchingFunction.cancel(false);
            running = false;
            currentService.shutdown();
            if (cancelled) {
                publishChanges();
            }
        }
    }

    private void publishChanges() {
        for (Map.Entry<ShareId, List<ShareData>> shareHistory : cachedShareChanges.entrySet()) {
            Optional<ShareChange> shareChange = updateApplier.updateShare(shareHistory.getKey(), shareHistory.getValue());
            shareChange.ifPresent(change -> observers.forEach(observer -> observer.onShareChange(change)));
        }
        cachedShareChanges.clear();
    }
}
