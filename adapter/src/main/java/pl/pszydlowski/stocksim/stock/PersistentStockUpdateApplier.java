package pl.pszydlowski.stocksim.stock;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import pl.pszydlowski.stocksim.share.Share;
import pl.pszydlowski.stocksim.share.repository.ShareRepository;
import pl.pszydlowski.stocksim.share.value.ShareChange;
import pl.pszydlowski.stocksim.share.value.ShareData;
import pl.pszydlowski.stocksim.share.value.ShareId;
import pl.pszydlowski.stocksim.share.value.SharePrice;
import pl.pszydlowski.stocksim.stock.strategy.StockUpdateStrategy;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
final class PersistentStockUpdateApplier implements StockUpdateApplier {

    @NonNull
    private final ShareRepository repository;

    @NonNull
    @Getter
    @Setter
    private StockUpdateStrategy stockUpdateStrategy;

    @Override
    public Optional<ShareChange> updateShare(ShareId shareId, List<ShareData> shareDataList) {
        return repository.findById(shareId).map(share -> {

            final ShareData previousData = shareDataList.get(0);

            final int lastDataIndex = shareDataList.size() - 1;

            final ShareData mostRecentChangeData = shareDataList.get(lastDataIndex);

            updateSharePrice(shareId, share, mostRecentChangeData);

            final ShareData currentData = share.getData();

            return new ShareChange(shareId, previousData, currentData, Instant.now());
        });
    }

    private void updateSharePrice(ShareId shareId, Share share, ShareData mostRecentChangeData) {
        final SharePrice updatedPrice = stockUpdateStrategy.computeNewPrice(shareId, mostRecentChangeData);
        share.setCurrentPrice(updatedPrice);
        repository.save(shareId, share);
    }
}
