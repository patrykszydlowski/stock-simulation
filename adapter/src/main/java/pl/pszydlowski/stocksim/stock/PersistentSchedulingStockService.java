package pl.pszydlowski.stocksim.stock;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import pl.pszydlowski.stocksim.common.money.Wallet;
import pl.pszydlowski.stocksim.share.Share;
import pl.pszydlowski.stocksim.share.repository.ShareRepository;
import pl.pszydlowski.stocksim.share.value.ShareId;
import pl.pszydlowski.stocksim.share.value.ShareVolume;
import pl.pszydlowski.stocksim.shareholder.repository.ShareholderDataRepository;
import pl.pszydlowski.stocksim.shareholder.value.ShareholderData;
import pl.pszydlowski.stocksim.shareholder.value.ShareholderId;
import pl.pszydlowski.stocksim.stock.value.StockResponse;

import java.math.BigDecimal;

import static pl.pszydlowski.stocksim.stock.value.StockResponse.failure;
import static pl.pszydlowski.stocksim.stock.value.StockResponse.success;
import static pl.pszydlowski.stocksim.stock.value.StockResponseCode.INSUFFICIENT_AVAILABLE_VOLUME;
import static pl.pszydlowski.stocksim.stock.value.StockResponseCode.INSUFFICIENT_OWNED_MONEY;
import static pl.pszydlowski.stocksim.stock.value.StockResponseCode.INSUFFICIENT_OWNED_VOLUME;
import static pl.pszydlowski.stocksim.stock.value.StockResponseCode.INVALID_SHARE_ID;

@RequiredArgsConstructor
final class PersistentSchedulingStockService implements StockService {

    @NonNull
    private final ShareRepository shareRepository;
    @NonNull
    private final ShareholderDataRepository dataRepository;
    @NonNull
    private final StockUpdateDispatcher dispatcher;

    @Override
    public StockResponse requestBuy(ShareholderId shareholderId, ShareId shareId, ShareVolume buyVolume) {

        final Wallet buyerWallet = dataRepository.findById(shareholderId).map(ShareholderData::getWallet).orElse(Wallet.EMPTY);

        return shareRepository.findById(shareId).map(share -> {

            if (!share.hasEnoughAvailableVolume(buyVolume)) {
                return failure(INSUFFICIENT_AVAILABLE_VOLUME, buyerWallet);
            }

            final BigDecimal totalPrice = Share.getTotalPrice(share.getCurrentPrice(), buyVolume);

            if (!buyerWallet.canAfford(totalPrice)) {
                return failure(INSUFFICIENT_OWNED_MONEY, buyerWallet);
            }

            final Wallet responseWallet = buyerWallet.subtract(new Wallet(totalPrice));

            final ShareVolume ownedVolume = share.getShareholderOwnedVolume(shareholderId);
            final ShareVolume newVolume = ownedVolume.add(buyVolume);

            share.setShareholderOwnedVolume(shareholderId, newVolume);

            shareRepository.save(shareId, share);
            dataRepository.save(shareholderId, new ShareholderData(shareholderId, responseWallet));

            dispatcher.addShareTransaction(shareId, share.getData());

            return success(responseWallet, newVolume);

        }).orElse(failure(INVALID_SHARE_ID, buyerWallet));
    }

    @Override
    public StockResponse requestSell(ShareholderId shareholderId, ShareId shareId, ShareVolume sellVolume) {

        Wallet sellerWallet = dataRepository.findById(shareholderId).map(ShareholderData::getWallet).orElse(Wallet.EMPTY);

        return shareRepository.findById(shareId).map(share -> {

            if (!share.hasEnoughOwnedVolume(shareholderId, sellVolume)) {
                return failure(INSUFFICIENT_OWNED_VOLUME, sellerWallet);
            }

            final BigDecimal totalPrice = Share.getTotalPrice(share.getCurrentPrice(), sellVolume);

            final Wallet responseWallet = sellerWallet.add(new Wallet(totalPrice));

            final ShareVolume ownedVolume = share.getShareholderOwnedVolume(shareholderId);
            final ShareVolume newVolume = ownedVolume.subtract(sellVolume);

            share.setShareholderOwnedVolume(shareholderId, newVolume);

            shareRepository.save(shareId, share);
            dataRepository.save(shareholderId, new ShareholderData(shareholderId, responseWallet));

            dispatcher.addShareTransaction(shareId, share.getData());

            return success(responseWallet, newVolume);

        }).orElse(failure(INVALID_SHARE_ID, sellerWallet));
    }
}
