package pl.pszydlowski.stocksim.stock;

import lombok.NonNull;
import pl.pszydlowski.stocksim.common.money.Wallet;
import pl.pszydlowski.stocksim.environment.EnvironmentObserver;
import pl.pszydlowski.stocksim.share.Share;
import pl.pszydlowski.stocksim.share.repository.ShareRepository;
import pl.pszydlowski.stocksim.share.value.ShareId;
import pl.pszydlowski.stocksim.share.value.SharePrice;
import pl.pszydlowski.stocksim.share.value.ShareVolume;
import pl.pszydlowski.stocksim.shareholder.repository.ShareholderDataRepository;
import pl.pszydlowski.stocksim.shareholder.value.ShareholderData;
import pl.pszydlowski.stocksim.shareholder.value.ShareholderId;
import pl.pszydlowski.stocksim.stock.strategy.NullStockUpdateStrategy;
import pl.pszydlowski.stocksim.stock.strategy.StockUpdateStrategy;
import pl.pszydlowski.stocksim.stock.value.UpdateInterval;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.concurrent.ScheduledExecutorService;
import java.util.function.Supplier;

import static java.util.Optional.ofNullable;
import static pl.pszydlowski.stocksim.common.data.Maps.entry;

public final class Stock implements StockObservable, EnvironmentObserver {

    private final Collection<StockObserver> observers;
    private final Map<ShareholderId, StockObserver> observerCache;

    private final ShareRepository shareRepository;

    private final ShareholderDataRepository dataRepository;

    private final StockUpdateApplier updateApplier;
    private final StockUpdateDispatcher updateDispatcher;
    private final StockService stockService;

    private final Wallet startingWallet;

    public Stock(@NonNull ShareRepository shareRepository, @NonNull ShareholderDataRepository dataRepository,
                 @NonNull StockUpdateStrategy strategy, @NonNull Supplier<ScheduledExecutorService> serviceSupplier,
                 @NonNull UpdateInterval updateInterval, @NonNull Wallet startingWallet) {
        this.dataRepository = dataRepository;
        this.startingWallet = startingWallet;
        this.observers = new HashSet<>();
        this.observerCache = new HashMap<>();
        this.shareRepository = shareRepository;
        this.updateApplier = new PersistentStockUpdateApplier(shareRepository, strategy);
        this.updateDispatcher = new ThreadedStockUpdateDispatcher(serviceSupplier, observers, updateApplier, updateInterval);
        this.stockService = new PersistentSchedulingStockService(shareRepository, dataRepository, updateDispatcher);

        shareRepository.findAll().forEach(share -> updateDispatcher.addShareTransaction(share.getId(), share.getData()));
    }

    public Stock(@NonNull ShareRepository shareRepository, @NonNull ShareholderDataRepository dataRepository,
                 @NonNull Supplier<ScheduledExecutorService> serviceSupplier, @NonNull UpdateInterval updateInterval,
                 @NonNull Wallet startingWallet) {
        this(shareRepository, dataRepository, NullStockUpdateStrategy.getInstance(), serviceSupplier, updateInterval, startingWallet);
    }

    @Override
    public synchronized void register(StockObserver observer) {
        observers.add(observer);
    }

    @Override
    public synchronized void unregister(StockObserver observer) {
        observers.remove(observer);
    }

    @Override
    public synchronized void onEnvironmentStart() {
        updateDispatcher.start();
    }

    @Override
    public synchronized void onEnvironmentStop() {
        updateDispatcher.stop();
    }

    @Override
    public synchronized void onStockUpdateStrategyChange(@NonNull StockUpdateStrategy stockUpdateStrategy) {
        updateApplier.setStockUpdateStrategy(stockUpdateStrategy);
    }

    @Override
    public synchronized void onShareholderAdded(@NonNull ShareholderId shareholderId, @NonNull StockObserver shareholder) {
        final Wallet wallet = dataRepository.findById(shareholderId).map(ShareholderData::getWallet).orElse(startingWallet);
        dataRepository.save(shareholderId, new ShareholderData(shareholderId, wallet));
        observerCache.put(shareholderId, shareholder);
        register(shareholder);
        shareholder.onWalletAssigned(wallet);
        shareholder.onStockServiceAssigned(stockService);
        assignOwnedVolume(shareholderId, shareholder);
    }

    @Override
    public synchronized void onShareholderWalletChange(@NonNull ShareholderId shareholderId, @NonNull Wallet wallet) {
        dataRepository.save(shareholderId, new ShareholderData(shareholderId, wallet));
        ofNullable(observerCache.get(shareholderId)).ifPresent(observer -> observer.onWalletAssigned(wallet));
    }

    @Override
    public synchronized void onShareholderRemoved(@NonNull ShareholderId shareholderId) {
        final StockObserver observer = observerCache.remove(shareholderId);
        unregister(observer);
        dataRepository.remove(shareholderId);
        shareRepository.findAll().stream()
                       .filter(share -> share.shareholderOwnsAnyVolume(shareholderId))
                       .forEach(share -> {
                           share.setShareholderOwnedVolume(shareholderId, ShareVolume.ZERO);
                           onShareVolumeChange(share.getId(), share.getAvailableVolume());
                       });
    }

    @Override
    public synchronized void onShareAdded(@NonNull ShareId shareId, @NonNull SharePrice price, @NonNull ShareVolume volume) {
        if (!shareRepository.findById(shareId).isPresent()) {
            final Share share = new Share(shareId, price, volume);
            shareRepository.save(shareId, share);
            updateDispatcher.addShareTransaction(shareId, share.getData());
        }
    }

    @Override
    public synchronized void onSharePriceChange(@NonNull ShareId shareId, @NonNull SharePrice updatedPrice) {
        shareRepository.findById(shareId).ifPresent(share -> {
            updateDispatcher.addShareTransaction(shareId, share.getData());
            share.setCurrentPrice(updatedPrice);
            updateDispatcher.addShareTransaction(shareId, share.getData());
            shareRepository.save(shareId, share);
        });
    }

    @Override
    public synchronized void onShareVolumeChange(@NonNull ShareId shareId, @NonNull ShareVolume updatedVolume) {
        shareRepository.findById(shareId).ifPresent(share -> {
            updateDispatcher.addShareTransaction(shareId, share.getData());
            share.setAvailableVolume(updatedVolume);
            updateDispatcher.addShareTransaction(shareId, share.getData());
            shareRepository.save(shareId, share);
        });
    }

    @Override
    public synchronized void onShareRemoved(@NonNull ShareId shareId) {
        shareRepository.remove(shareId).ifPresent(share -> {
            final Map<ShareholderId, ShareVolume> ownedVolumes = share.getOwnedVolumes();
            ownedVolumes.forEach(((shareholderId, ownedVolume) -> {
                final StockObserver observer = observerCache.get(shareholderId);
                observer.onShareRemoved(share.getCurrentPrice(), ownedVolume);
            }));
        });
    }

    private void assignOwnedVolume(ShareholderId shareholderId, StockObserver observer) {
        shareRepository
                .findAll()
                .stream()
                .map(share -> entry(share.getId(), share.getShareholderOwnedVolume(shareholderId)))
                .filter(entry -> entry.getValue().compareTo(ShareVolume.ZERO) > 0)
                .forEach(entry -> observer.onVolumeAssigned(entry.getKey(), entry.getValue()));
    }
}
