package pl.pszydlowski.stocksim.stock.strategy;

import pl.pszydlowski.stocksim.share.value.ShareData;
import pl.pszydlowski.stocksim.share.value.ShareId;
import pl.pszydlowski.stocksim.share.value.SharePrice;

public final class NullStockUpdateStrategy implements StockUpdateStrategy {

    private static final NullStockUpdateStrategy INSTANCE = new NullStockUpdateStrategy();

    private NullStockUpdateStrategy() {
    }

    public static NullStockUpdateStrategy getInstance() {
        return INSTANCE;
    }

    @Override
    public SharePrice computeNewPrice(ShareId shareId, ShareData currentData) {
        return currentData.getPrice();
    }
}
