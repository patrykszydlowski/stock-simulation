package pl.pszydlowski.stocksim.stock.strategy;

import lombok.RequiredArgsConstructor;
import pl.pszydlowski.stocksim.share.value.ShareData;
import pl.pszydlowski.stocksim.share.value.ShareId;
import pl.pszydlowski.stocksim.share.value.SharePrice;

import java.math.BigDecimal;

@RequiredArgsConstructor
public final class ConstantValueChangeStrategy implements StockUpdateStrategy {

    private final double changeRatio;

    @Override
    public SharePrice computeNewPrice(ShareId shareId, ShareData currentData) {
        return new SharePrice(currentData.getPrice().getValue().multiply(BigDecimal.valueOf(changeRatio)));
    }
}
