package pl.pszydlowski.stocksim.environment;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.pszydlowski.stocksim.common.money.Wallet;
import pl.pszydlowski.stocksim.share.value.ShareId;
import pl.pszydlowski.stocksim.share.value.SharePrice;
import pl.pszydlowski.stocksim.share.value.ShareVolume;
import pl.pszydlowski.stocksim.shareholder.value.ShareholderId;
import pl.pszydlowski.stocksim.stock.StockObserver;
import pl.pszydlowski.stocksim.stock.strategy.StockUpdateStrategy;

public class EnvironmentLogger implements EnvironmentObserver {

    private static final Logger LOGGER = LoggerFactory.getLogger(EnvironmentLogger.class);

    @Override
    public void onEnvironmentStart() {
        LOGGER.info("Environment has started");
    }

    @Override
    public void onEnvironmentStop() {
        LOGGER.info("Environment has stopped");
    }

    @Override
    public void onStockUpdateStrategyChange(StockUpdateStrategy stockUpdateStrategy) {
        LOGGER.info("Stock update strategy has been changed to [{}]", stockUpdateStrategy.getClass().getSimpleName());
    }

    @Override
    public void onShareholderAdded(ShareholderId shareholderId, StockObserver shareholder) {
        LOGGER.info("Shareholder [{}] has been added to environment", shareholderId);
    }

    @Override
    public void onShareholderWalletChange(ShareholderId shareholderId, Wallet wallet) {
        LOGGER.info("Shareholder [{}] has been granted new wallet of [{}]", shareholderId, wallet.getValue());
    }

    @Override
    public void onShareholderRemoved(ShareholderId shareholderId) {
        LOGGER.info("Shareholder [{}] has been removed from environment", shareholderId);
    }

    @Override
    public void onShareAdded(ShareId shareId, SharePrice price, ShareVolume volume) {
        LOGGER.info("Share [{}] has been added to environment with price [{}] and volume [{}]", shareId, price, volume);
    }

    @Override
    public void onSharePriceChange(ShareId shareId, SharePrice updatedPrice) {
        LOGGER.info("Share [{}] has changed it's price to [{}]", shareId, updatedPrice);
    }

    @Override
    public void onShareVolumeChange(ShareId shareId, ShareVolume updatedVolume) {
        LOGGER.info("Share [{}] has changed it's volume to [{}]", shareId, updatedVolume);
    }

    @Override
    public void onShareRemoved(ShareId shareId) {
        LOGGER.info("Share [{}] has been removed from environment", shareId);
    }
}
