package pl.pszydlowski.stocksim.environment;

import lombok.NonNull;
import pl.pszydlowski.stocksim.common.money.Wallet;
import pl.pszydlowski.stocksim.share.repository.ShareRepository;
import pl.pszydlowski.stocksim.shareholder.repository.ShareholderDataRepository;
import pl.pszydlowski.stocksim.stock.Stock;
import pl.pszydlowski.stocksim.stock.StockObserver;
import pl.pszydlowski.stocksim.stock.value.UpdateInterval;

import java.util.Collection;
import java.util.HashSet;
import java.util.concurrent.ScheduledExecutorService;
import java.util.function.Supplier;

import static java.util.Arrays.stream;
import static java.util.stream.Collectors.toList;

public final class InteractiveEnvironmentBuilder {

    private final Collection<StockObserver> additionalStockObservers;

    private ShareRepository shareRepository;
    private ShareholderDataRepository dataRepository;
    private Supplier<ScheduledExecutorService> serviceSupplier;
    private UpdateInterval updateInterval;
    private Wallet defaultWallet;

    public InteractiveEnvironmentBuilder() {
        this.additionalStockObservers = new HashSet<>();
    }

    public InteractiveEnvironmentBuilder withShareRepository(@NonNull ShareRepository shareRepository) {
        this.shareRepository = shareRepository;
        return this;
    }

    public InteractiveEnvironmentBuilder withDataRepository(@NonNull ShareholderDataRepository dataRepository) {
        this.dataRepository = dataRepository;
        return this;
    }

    public InteractiveEnvironmentBuilder withServiceSupplier(@NonNull Supplier<ScheduledExecutorService> serviceSupplier) {
        this.serviceSupplier = serviceSupplier;
        return this;
    }

    public InteractiveEnvironmentBuilder withUpdateInterval(@NonNull UpdateInterval updateInterval) {
        this.updateInterval = updateInterval;
        return this;
    }

    public InteractiveEnvironmentBuilder withDefaultWallet(@NonNull Wallet defaultWallet) {
        this.defaultWallet = defaultWallet;
        return this;
    }

    public InteractiveEnvironmentBuilder withAdditionalStockObserver(@NonNull StockObserver observer) {
        additionalStockObservers.add(observer);
        return this;
    }

    public InteractiveEnvironmentBuilder withAdditionalStockObservers(@NonNull Collection<StockObserver> observers) {
        additionalStockObservers.addAll(observers);
        return this;
    }

    public InteractiveEnvironmentBuilder withAdditionalStockObservers(@NonNull StockObserver... observers) {
        return withAdditionalStockObservers(stream(observers).collect(toList()));
    }

    public InteractiveEnvironment build(@NonNull EnvironmentObserver... additionalObservers) {
        final Stock stock = new Stock(shareRepository, dataRepository, serviceSupplier, updateInterval, defaultWallet);
        final InteractiveEnvironment environment = new InteractiveEnvironment(shareRepository, dataRepository, additionalObservers);

        additionalStockObservers.forEach(stock::register);
        environment.register(stock);
        return environment;
    }
}
