package pl.pszydlowski.stocksim.environment;

import lombok.NonNull;
import pl.pszydlowski.stocksim.common.money.Wallet;
import pl.pszydlowski.stocksim.common.repository.Repository;
import pl.pszydlowski.stocksim.share.Share;
import pl.pszydlowski.stocksim.share.repository.ShareRepository;
import pl.pszydlowski.stocksim.share.value.ShareId;
import pl.pszydlowski.stocksim.share.value.SharePrice;
import pl.pszydlowski.stocksim.share.value.ShareVolume;
import pl.pszydlowski.stocksim.shareholder.repository.ShareholderDataRepository;
import pl.pszydlowski.stocksim.shareholder.value.ShareholderData;
import pl.pszydlowski.stocksim.shareholder.value.ShareholderId;
import pl.pszydlowski.stocksim.stock.StockObserver;
import pl.pszydlowski.stocksim.stock.strategy.StockUpdateStrategy;

import java.util.ArrayList;
import java.util.Collection;

import static pl.pszydlowski.stocksim.common.repository.Repositories.unmodifiable;

public final class InteractiveEnvironment implements EnvironmentObservable {

    private final Collection<EnvironmentObserver> observers;
    private final ShareRepository shareRepository;
    private final ShareholderDataRepository dataRepository;

    InteractiveEnvironment(ShareRepository shareRepository, ShareholderDataRepository dataRepository, @NonNull EnvironmentObserver... observers) {
        this.shareRepository = shareRepository;
        this.dataRepository = dataRepository;
        this.observers = new ArrayList<>();
        for (EnvironmentObserver observer : observers) {
            register(observer);
        }
    }

    @Override
    public void register(@NonNull EnvironmentObserver observer) {
        observers.add(observer);
    }

    @Override
    public void unregister(@NonNull EnvironmentObserver observer) {
        observers.remove(observer);
    }

    public void start() {
        observers.forEach(EnvironmentObserver::onEnvironmentStart);
    }

    public void stop() {
        observers.forEach(EnvironmentObserver::onEnvironmentStop);
    }

    public Repository<Share, ShareId> getShareRepository() {
        return unmodifiable(shareRepository);
    }

    public Repository<ShareholderData, ShareholderId> getDataRepository() {
        return unmodifiable(dataRepository);
    }

    public void changeStockUpdateStrategy(@NonNull StockUpdateStrategy stockUpdateStrategy) {
        observers.forEach(observer -> observer.onStockUpdateStrategyChange(stockUpdateStrategy));
    }

    public void addShareholder(@NonNull ShareholderId shareholderId, @NonNull StockObserver shareholder) {
        observers.forEach(observer -> observer.onShareholderAdded(shareholderId, shareholder));
    }

    public void changeShareholderWallet(@NonNull ShareholderId shareholderId, @NonNull Wallet wallet) {
        observers.forEach(observer -> observer.onShareholderWalletChange(shareholderId, wallet));
    }

    public void removeShareholder(@NonNull ShareholderId shareholderId) {
        observers.forEach(observer -> observer.onShareholderRemoved(shareholderId));
    }

    public void addShare(@NonNull ShareId shareId, @NonNull SharePrice price, @NonNull ShareVolume volume) {
        observers.forEach(observer -> observer.onShareAdded(shareId, price, volume));
    }

    public void changeSharePrice(@NonNull ShareId shareId, @NonNull SharePrice updatedPrice) {
        observers.forEach(observer -> observer.onSharePriceChange(shareId, updatedPrice));
    }

    public void changeShareVolume(@NonNull ShareId shareId, @NonNull ShareVolume updatedVolume) {
        observers.forEach(observer -> observer.onShareVolumeChange(shareId, updatedVolume));
    }

    public void removeShare(@NonNull ShareId shareId) {
        observers.forEach(observer -> observer.onShareRemoved(shareId));
    }
}
